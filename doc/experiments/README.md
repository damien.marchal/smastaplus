# Empirical evaluation

The instructions to install and run SMASTA+ are [here](../../README.md)

The following features are evaluated:
- [Job Release](#job-release)
- [Running consumer](#running-consumer)
- [Consumer](#consumer)
  - [The reallocation strategy improves the flowtime](#the-reallocation-strategy-improves-the-flowtime)
  - [The reallocation strategy does not penalize the consumption](#the-reallocation-strategy-does-not-penalize-the-consumption)
  - [The reallocation strategy is robust to execution hazards](#the-reallocation-strategy-is-robust-to-execution-hazards)
- [Balancer](#balancer)
  - [Classical heuristic and acceptability criterion](#classical-heuristic-and-acceptability-criterion)
  - [Mono vs. multi-thread](#mono-vs-multi-thread)
  - [N-ary delegation](#n-ary-delegation)
  - [Comparison with Sequential Single-Issue (SSI) and Parallel Single-Issue (PSI) auctions](#comparison-with-sequential-single-issue-ssi-and-parallel-single-issue-psi-auctions)
  - [Comparison with Distributed Constraint Optimization Problem (DCOP) solver](#comparison-with-distributed-constraint-optimization-problem-dcop-solver)
  - [Swap](#swap)

We consider the simulated cost $c^S(t,n)$ as the effective cost of performing the task t
by the node n. :
- with a perfect knowledge of the computing environment, $c^S(t,n) = cost(t,n)$;
- with half of the nodes slowing down, $c^S(t,n) = 2 * c(t,n)$ if $n mod 2=1$ and $c^S(t,n) = cost(t,n)$ otherwise.
  This is the reason why e distinguish the two following metrics:
- the simulated flowtime $C^S(A)$ for an allocation A according to the simulated costs;
- the real flowtime $C^(At)$ for an allocation A according to the task completion times which are measured.

The performance improvement rate is defined:

$γ = (C^R(A0) - C^R(Ae))/C^R(A0)$

where $A_e$ is the allocation where tasks are executed and $A_0$ is the initial allocation.

During the consumption of the initial allocation, we consider the following metrics :
- the flowtime of the current allocation according to the completion times of tasks
  already performed which are measured and the simulated cost of forthcoming tasks;
- the number of performed tasks;
- the number of delegated tasks if the reallocation strategy is adopted.

In order compare our reallocation strategy executed concurrently with this consumption process, we consider :
- an allocation method based on sequential single-item (SSI) auctions [Lagoudakis et al., 2006] where bids correspond to the flowtime, 
  which precedes the allocation process. Every agent bids on each unallocated task.
  It bids the increase in the smallest mean flowtime that results from winning the
  task that it bids on. The agent with the overall smallest bid is allocated the
  corresponding task. Then, each agent re-bids on each unallocated task, and the cycle
  repeats until all targets are assigned. When new jobs are released, additional tasks are
  are allocated in the same way;
- a reallocation method based on parallel single-item (PSI) auctions [Lagoudakis et al., 2006] executed concurrently 
  with the consumption process where each node announces the pending jobs in its job pool. The agent that currently owns a task determines and
  informs the winning agent for the target, which is the agent with the smallest bid on the target.
  When new jobs are new jobs are released, additional tasks are reallocated in the same way.

[Lagoudakis et al., 2006] Lagoudakis, S. K. C. T. M., Markakis, V., Kempe, D., Keskinocak, P.,
Kleywegt, A., Meyerson, A., & Jain, S. (2006). The Power of Sequential Single-Item Auctions
for Agent Coordination. AAAI. 1625--1629.

It is worth noticing that we do not implement single-round combinatorial auction since
agents cannot bid on all possible bundles of tasks because the number of possible bundles
is exponential in the number of tasks and the computation by the agents of their bids
is NP-hard.

The experimental protocol consists in randomly generating 25 initial allocations for distinct STAP problems.
We empirically set:
- κ = 2 as a realistic value to capture the overhead induced by the recovery of non-local resources;
- m = 8 nodes;
- l = 4 jobs;
- n ∈ [40;320] tasks with 10 resources per task;
- Each resource ρi is replicated 3 times and |ρi| ∈ [0;500].

## Job Release

Our experiment aims at validating that the reallocation strategy adapts to the release of jobs.

The experimental protocol consists in randomly generating 10 initial allocations for distinct STAP problems.
We empirically set n = 128 tasks with 10 resources per task. One job is released 10 seconds after the 
others and after the start of consumption.

We assume that initial allocations are random  and that the agents have perfect knowledge of the execution environment.
execution environment ($c^{S_E}$). This figure  shows the evolution of the medians and standard deviations of our metrics during
consumption (according to a logarithmic time scale) of 128 tasks divided into 4 jobs, one of which is released 10 seconds after the others and
after consumption has begun. In contrast to the SSI-based allocation method and the PSI-based reallocation method, our strategy 
reallocates the tasks of a job as soon as it is released in order to
improve the achieved flowtime and reduce the consumption time.

![](../../experiments/jobRelease/figures/monitor.svg)

To run the experiment, execute 10 times:

    sbt "run org.smastaplus.utils.MASTAPlusJobRelease"
    cp *.csv experiments/jobRelease/data

To generate the figures in 'experiments/jobRelease/figures/' :

    cd experiments/jobRelease
    gawk -f merge.awk data/noNegotiation*.csv > data/quartilesNoNegotiationLog.csv
    gawk -f merge.awk data/globalFlowtimeMultiProposalStrategySingleCounterProposalStrategyLog*.csv > data/quartilesGlobalFlowtimeMultiProposalStrategySingleCounterProposalStrategyLog.csv
    cat monitor.plot| gnuplot


## Running consumer

Our experiment aims at explaining how the reallocation strategy improves the flowtime 
when executed continuously during the consumption process.

This figure shows the evolution of the medians and standard deviations
standard deviations of our metrics over the course of the consumption (on a logarithmic
time scale) of 128 tasks. It confirms that the best flowtime
achieved is that obtained by our strategy when it is run concurrently with the
executed concurrently with the consumption process. The responsiveness of our strategy is explained by 
the reallocation of tasks bundles that represent almost half of them thanks to multiple
concurrent bilateral negotiations that begin even before the  consumption process (after 0.1 second).

![](../../experiments/runningAuction/figures/monitor.svg)

To run the experiment, execute 10 times:

    sbt "run org.smastaplus.utils.MASTAPlusConsumer"
    cp *.csv experiments/runningAuction/data

To generate the figures in 'experiments/runningAuction/figures/' :

    cd experiments/runningAuction
    gawk -f merge.awk data/noNegotiation*.csv > data/quartilesNoNegotiationLog.csv
    gawk -f merge.awk data/negotiation*.csv > data/quartilesNegotiationLog.csv
    gawk -f merge.awk data/ssi*.csv > data/quartilesSsiLog.csv
    gawk -f merge.awk data/psi*.csv > data/quartilesPsiLog.csv
    cat monitor.plot| gnuplot

## Consumer

Our experiments aim at validating that, when executed continuously during the consumption process, 
the reallocation strategy: 
1. improves the flowtime; 
2. does not penalize the consumption;
3. is robust to execution hazards (i.e. node slowdown).

### The reallocation strategy improves the flowtime

This figure  shows the medians and standard deviations of our
of our metrics as a function of the number of tasks. We observe that the
flowtime achieved by the SSI auction-based allocation method employed prior to the
used before the consumption process is worse than the
flowtime of the initial random allocation. Even if the simulated flowtime of 
the SSI-based allocation is very good, this method
method delays the consumption process in order to first obtain a balanced
allocation and therefore penalises the achieved flowtime. Conversely, the flowtime achieved 
by the reallocation methods applied during the consumption process,
whether this is our strategy or whether it is based on PSI auctions, is better than the achieved
flowtime realised from random initial allocation. Furthermore, the
flowtime of the reallocation methods is bounded by the simulated flowtime of the reallocation (if an oracle calculates the
reallocation in constant time). These methods improve the flowtime
by reallocating, during the consumption process, non-local tasks whose delegation reduces the cost.
As the number of delegations in strategy is greater than the number of delegations in the PSI-based
reallocation method, the performance improvement rate of our strategy is better (between 13% and 23%) 
than the performance  the performance improvement rate of the reference method (between 0% and
15%).

![](../../experiments/consumer/figures/consumption4jobs8nodes5staps5allocations040-320tasks.svg)

To run the experiment:

    sbt "run org.smastaplus.experiment."org.smastaplus.experiment.consumer.WithVsWithoutNegotiationCampaign"
    sbt "run org.smastaplus.utils.MergeRuns"

To generate the figures in 'experiments/consumer/figures/' :

    cd experiments/consumer/
    cat consumption.plot| gnuplot

#### The reallocation strategy does not penalize the consumption

We assume here that the initial allocations are stable, i.e. for which there is no socially rational bilateral reallocation.
In this figure the real flowtime of the reallocation methods is similar to the real flowtime of the initial allocation. 
The additional cost of negotiation, be it auctions or bilateral bargaining, is negligible because the negotiation process 
is concurrent with the consumption process and, in the case of our strategy, no negotiation is triggered when the agents 
believe that the allocation is stable.

![](../../experiments/consumer/figures/consumption4jobs8nodes5staps5allocationsFromStable040-320tasks.svg)

To run the experiment:

    sbt "run org.smastaplus.experiment."org.smastaplus.experiment.consumer.WithVsWithoutNegotiationFromStableCampaign"
    sbt "run org.smastaplus.utils.MergeRuns"

To generate the figures in 'experiments/consumer/figures/' :

    cd experiments/consumer/
    cat consumption.plot| gnuplot

### The reallocation strategy is robust to execution hazards

We consider here the effective cost of the tasks which simulates the slowdown of half the nodes.
In this figure we see that the mean flowtimes have doubled because of the execution hazards. 
The real flowtime and the simulated flowtime of the SSI-based allocation method, which does not take into account the slowdown 
of half the nodes, are higher than the same configurations without slowdown. Unlike the PSI-based reallocation method, 
the real flowtime of our strategy remains better than the real flowtime of the random initial allocation, despite imperfect 
knowledge of the agents' execution environment. Taking into account the actual execution times of the tasks already performed
allows the performance improvement rate to be between 17% and 29%.

![](../../experiments/consumer/figures/consumption4jobs8nodes5staps5allocations040-320tasks.svg)


To run the experiment:

    sbt "run org.smastaplus.experiment."org.smastaplus.experiment.consumer.WithVsWithoutNegotiationSlowDownHalfNodeCampaign"
    sbt "run org.smastaplus.utils.MergeRuns"

To generate the figures in 'experiments/consumer/figures' :

    cd experiments/consumer/
    cat consumptionSlowDownHalfNode.plot| gnuplot


## Balancer

The hypothesis we want to test are: (1) the flowtime reached by our strategy is close to 
the one reached by the classical approach and (2) the decentralization significantly reduces 
the scheduling time. Moreover, unlike our [previous work](../publications/beauprez21icaart.pdf) 
where we used the local flowtime  and the makespan in our acceptability criterion, here, we 
only consider the global flowtime which is sufficient to ensure the negotiation process convergence.
We also want to verify that the acceptability the quality of the outcome.

We consider problem instance with:
- m ∈ [2; 12] nodes;
- l = 4 jobs;
- n = 3 × l × m tasks;
- 10 resources per task. Each resource ρi is replicated 3 times and |ρi| ∈ [0;100].
  We generate 10 problem instances, and for each we randomly generate 10 initial allocations.
  We assess the medians and the standard deviations of three metrics: (1) the mean flowtime,
  (2) the local availability ratio (Eq. 10), and (3) the rescheduling time.

### Classical heuristic and acceptability criterion

![](../../experiments/balancer/figures/localFlowtimeVsGlobalFlowtime/meanFlowtime.svg)
![](../../experiments/balancer/figures/localFlowtimeVsGlobalFlowtime/schedulingTime.svg)

These figures respectively compare the flowtime and rescheduling time of our unary delegation 
strategy with the strategy presented in our [previous work](../publications/beauprez21icaart.pdf)  
and a hill climbing algorithm. These three algorithms start with the same random initial allocation. 
At each step, the hill climbing algorithm selects among all the possible delegations, the one 
which minimizes the flowtime.

![](../../experiments/balancer/figures/localFlowtimeVsGlobalFlowtime/localityRatio.svg)

This figure compares the local availability ratio of the initial allocation,
the allocations reached by our strategy, by the strategy proposed in 
our [previous work](../publications/beauprez21icaart.pdf) and 
by the hill climbing algorithm.

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.LocalVsGlobalCampaign"

To generate the figures in 'experiments/balancer/figures/localFlowtimeVsGlobalFlowtime' :

    cd experiments/balancer/
    cat localFlowtimeVsGlobalFlowtime.plot| gnuplot

### Mono vs. multi-thread

![](../../experiments/balancer/figures/monoVsMultiThread/schedulingTime.svg)

This figure compares the rescheduling time of our strategy with one or more threads and 
the rescheduling time of the hill climbing algorithm. 

N.B.: Since SMASTA+ 1.2, the internal agents within the node agents are multithreaded.

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.MonoVsMultiThread"

To generate the figures in 'experiments/balancer/figures/monoVsMultiThread' :

    cd experiments/balancer/
    cat monoVsMultiThread.plot| gnuplot


### N-ary delegation

![](../../experiments/balancer/figures/singleVsMultiDelegationFlowtime/meanFlowtime.svg)
![](../../experiments/balancer/figures/singleVsMultiDelegationFlowtime/schedulingTime.svg)

These figures respectively compare the flowtime and rescheduling time for 
our unary strategy with our n-ary strategy. 

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.singleVsMultiDelegationFlowtime" 

To generate the figures in 'experiments/balancer/figures/centralizedVsDecentralized' :

    cd experiments/balancer/
    cat singleVsMultiDelegationFlowtime.plot| gnuplot


![](../../experiments/balancer/figures/singleVsMultiDelegationFlowtime/workloadMultiProposalByDelegation.svg)
![](../../experiments/balancer/figures/singleVsMultiDelegationFlowtime/workloadSingleProposalByDelegation.svg)
![](../../experiments/balancer/figures/singleVsMultiDelegationFlowtime/workloadMultiProposalByTime.svg)
![](../../experiments/balancer/figures/singleVsMultiDelegationFlowtime/workloadSingleProposalByTime.svg)

These figures show the evolution of the flowtime for both offer strategies for a particular 
reallocation problem.

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.CompareMultiProposalStrategies"

To generate the figures in 'experiments/balancer/figures/singleVsMultiDelegationFlowtime' :

    mv *.csv experiments/balancer/data
    cd experiments/balancer/
    cat workloadProposalStrategies.plot| gnuplot

### Comparison with Sequential Single-Issue (SSI) and Parallel Single-Issue (PSI) auctions

![](../../experiments/balancer/figures/negotiation/meanFlowtime.svg)
![](../../experiments/balancer/figures/negotiation/schedulingTime.svg)
![](../../experiments/balancer/figures/negotiation/localityRatio.svg)

These figures respectively compare the flowtime, the scheduling time and the locality ratio
for our strategy with :
- a hill climbing algorithm which starts with a random initial allocation and, at each step, 
 selects among all the possible delegations, the one which minimizes the flowtime.
- an allocation method based on a Sequential Single-Issue (SSI) auctions, 
  where the bids correspond to the flowtime ;
- a reallocation method which starts with a random initial allocation and, 
  based on single-item parallel auctions auctions (PSI), 
  where each node announces the pending jobs in its bundle.

[Lagoudakis et al., 2006] Lagoudakis, S. K. C. T. M., Markakis, V., Kempe, D., Keskinocak, P., 
Kleywegt, A., Meyerson, A., & Jain, S. (2006). The Power of Sequential Single-Item Auctions 
for Agent Coordination. AAAI? 1625--1629

It is worth noticing that we do not implement single-round combinatorial auction since 
agents cannot bid on all possible bundles of tasks because the number of possible bundles 
is exponential in the number of tasks and the computation by the agents of their bids 
is NP-hard. 

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.PSSIVsNegotiationCampaign"" 

To generate the figures in 'experiments/balancer/figures/ssiVsNegotiation' :

    cd experiments/balancer/
    cat negotiation.plot | gnuplot

### Comparison with Distributed Constraint Optimization Problem (DCOP) solver

We consider here the MGM2 algorithm – Maximum Gain Message – as the most suitable DCOP resolution method, 
since it is a distributed local search algorithm which is approximate and asynchronous. 
We used the [pyDCOP libray](https://pydcop.readthedocs.io). 
We consider 100 problems with m = 2 nodes, l = 4 jobs and n = 3 x l × m = 24 tasks.


![](../../experiments/balancer/figures/dcopVsNego/dcopVsNego.svg)

This figure compares the flowtimes reached by our strategy in 0.4 second (mean value) 
and by the MGM2 algorithm with a timeout of 2, 5 and 10 seconds, respectively.

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.CompetitionBalancer" 

To generate the figures in 'experiments/balancer/figures/singleVsMultiDelegationFlowtime' :

    cd experiments/balancer/
    cat dcopVsNego.plot| gnuplot

### Swap

![](../../experiments/balancer/figures/delegationVsSwapFlowtime/meanFlowtime.svg)
![](../../experiments/balancer/figures/delegationVsSwapFlowtime/schedulingTime.svg)

Theses figures compare the flowtime and the rescheduling time, respectively, of our delegation strategy, 
our swap strategy as well and a hill-climbing algorithm with the same randomly generated initial allocation.

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.DelegationVsSwapCampaign" 

To generate the figures in 'experiments/balancer/figures/delegationVsSwapFlowtime/' :

    cd experiments/balancer/
    cat delegationVsSwapFlowtime.plot| gnuplot

![](../../experiments/balancer/figures/delegationVsSwapFlowtime/meanFlowtimeByTime.svg)

This figure shows the evolution of the mean flowtime for our swap strategies for a particular
reallocation problem.

To run the experiment:

    sbt "run org.smastaplus.experiment.balancer.SwapCounterProposalStrategy" 

To generate the figures in 'experiments/balancer/figures/delegationVsSwapFlowtime/' :

    mv *.csv experiments/balancer/data
    cd experiments/balancer/
    cat workloadCounterProposalStrategies.plot | gnuplot
