# Supervisor Behaviour Specification 
In this document, we specify
the [supervisor behaviour](../../src/main/scala/org/smastaplus/balancer/deal/mas/supervisor/Supervisor.scala).

The supervisor behaviour is specified by a deterministic finite
 state automaton with the following states:

- `InitialState` where the supervisor can receive the initial allocation from
the balancer and provide to the node agents their initial bundle;

- `RunnningFirstStage` where the supervisor is waiting for the agents to end
during the first negotiation stage;

- `EndFirstStage` where the agents no longer have transactions to initiate in
the first stage and the supervisor can, after collecting the metrics, initiate
the second stage or close the negotiation process;

- `FirstToSecond` where the supervisor waits for the agents to be ready to start
the next second stage;

- `RunnningSecondStage` where the supervisor is waiting for the agents to end
during the second negotiation stage if defined;

- `EndSecondStage` where the agents no longer have transactions to initiate in
the second stage and the supervisor collects the metrics, then can initiate the
first stage again or close the negotiation process;

- `SecondToFirst` where the supervisor waits for the agents to be ready to
start the next first stage;

- `TransitionalEnd` where the supervisor is waiting for the agents to receive
their Kill message to end the process;

- `Terminal` where the negotiation process is finished and the supervisor
has sent the outcome to the balancer;


The state transitions are such as:
```
 Event if conditions => actions.
```

While `stash` enables the supervisor to temporarily stash away 
messages, `unstashAll` prepends all messages in the stash to the mailbox.

The supervisor has several variables it keeps up-to-date, 
which are useful to know what to do, depending on the situation :

- `allocation` is the current allocation;

- `isTwoStageProcess` is a boolean which is true 
if the negotiation process includes two stages, false otherwise;

- `sumReady` is the number of nodes which are ready to receive 
their initial bundle;

- `desperatedNodes` is the set of agents which have no no more potential
bilateral reallocation;

- `sumReadyForNext` is the number of agents which are ready to start
the next stage;

- `answerNodes` is the set of agents which had given their metrics to the supervisor;

- `sumKilled` is the number of agents which have received a kill message;

- `lastQueryId` is the id of the last query for collecting the metrics of agents;

- `metrics` measures the negotiation process such as the current allocation, the
  number of proposals/counter-proposals sent, the number of delegations/swaps
  which are confirmed, the number of timeout, the number of delegated tasks, and
  the number of deals in the current stage.

![](figures/supervisorBehaviour.svg)


In the state `InitialState`, the supervisor can receive the 
following messages:

- An init message <tt>Init(A)</tt> from the balancer giving it the 
initial allocation. In response, the supervisor wakes the agents;

- A message <tt>Ready</tt> from an agent,

    - if not all the agents are ready, the supervisor
    increments the <tt>sumReady</tt> variable;

    - if all the agents are ready, the supervisor increments the
    <tt>sumReady</tt> variable and gives them their initial bundle
    (<tt>Give</tt>(B<sub>i</sub>)). Then it unstashes the messages it had
    previously stashed and moves to the state `RunnningFirstStage`;

- An <tt>End</tt> message from an agent which is stashed.


In the state `RunningFirstStage`, the supervisor can receive 
the following messages:

- An <tt>End</tt> message from an agent which updates the current allocation and,

    - if not all the agents are ended, the supervisor 
    adds the agent in the <tt>desperateNodes</tt> set;

    - if all the agents are ended, the supervisor 
    adds the agent in the <tt>desperateNodes</tt> set and, sends 
    itself a <tt>Close</tt> message and goes to the `EndFirstStage` state ;

- A <tt>Restart</tt> message from an agent which leads the 
supervisor to remove it from the <tt>desperateNodes</tt> set;

- An <tt>Answer</tt> from an agent or a <tt>Close</tt> message
 from itself which are ignored.



In the state `EndFirstStage`, the supervisor can receive the 
following messages :

- A <tt>Close</tt> message sent by itself which leads it to
 send a <tt>Query</tt> to all the agents, asking them to
  communicate their new metrics. The supervisor keeps in mind
  the id of the query in `lastQueryId`;

- A <tt>Restart</tt> message from an agent which leads the 
supervisor to remove the agents from the 
<tt>desperateNodes</tt>, to reset the <tt>answerNodes</tt>
set and <tt>lastQuery</tt>, and to go back to the 
`RunningFirstStage` state;

- An <tt>Answer</tt> from an agent,
  - if the <tt>replyId</tt> of the answer is different from 
  <tt>lastQueryId</tt>, the supervisor ignores it;

  - if the <tt>replyId</tt> of the answer is equal to
   <tt>lastQueryId</tt> and not all the answers had been
    collected, the supervisor stays in the current state, it 
    updates the metrics and adds the agent to the set 
    <tt>answerNodes</tt>;

  - if the <tt>replyId</tt> of the answer is equal to 
  <tt>lastQueryId</tt> and all the answers had been collected, 
  the supervisor updates the metrics and adds the agent to the
  set <tt>answerNodes</tt>,

    - if a second stage is defined, the supervisor sends an 
   <tt>PreTrigger</tt>(secondStage) to all the agents to 
   initiate the second stage, sets the
    <tt>isTransactionDuringSecondStage</tt> variable to 
    <tt>False</tt> and resets <tt>sumReady</tt>, 
    <tt>sumReadyForNext</tt> and 
    <tt>desperateNodes</tt> before moving to the 
    `FirstToSecond` state;
  
    - if there is no defined second stage, the supervisor 
     sends a <tt>Kill</tt> to the agents and moves to the `TransitionalEnd` state;


In the state `FirstToSecond`, the supervisor can 
receive the following messages:

- a <tt>ReadyForNextStage</tt> from an agent,
  
  - if all the agents are ready to start the next second stage, 
  the supervisor sends a <tt>Trigger</tt>(secondStage) message 
  to them and resets its `sumReadyForNext` variable before 
  moving to the `RunningFirstStage` state;

  - if not all the agents are ready, the supervisor increases 
  its `sumReadyForNext` variable and stays in the current state;

- an <tt>Answer</tt>/<tt>Restart</tt> from an agent or a <tt>Close</tt> message 
from itself which are ignored;


In the state `RunningSecondStage`, the supervisor can receive
 the following messages:

- An <tt>End</tt> message from an agent which updates the current allocation and,

    - if not all the agents are ended, the supervisor 
    adds the agent in the <tt>desperateNodes</tt> set;

    - if all the agents are ended, the supervisor 
    adds the agent in the <tt>desperateNodes</tt> set, sends 
    itself a <tt>Close</tt> message and goes to the
     `EndSecondStage` state;

- A <tt>Restart</tt> message from an agent which leads the 
supervisor to remove it from the <tt>desperateNodes</tt> set;

- An <tt>Answer</tt>/<tt>Restart</tt> from an agent or a <tt>Close</tt> message
 from itself which are ignored.

In the state `EndSecondStage`, the supervisor can receive the 
following messages:

- A <tt>Close</tt> message sent by itself which leads it to 
send a <tt>Query</tt> to all the agents, asking them to 
communicate their new metrics;

- A <tt>Restart</tt> message from an agent which leads the 
supervisor to remove the agents from the 
<tt>desperateNodes</tt> set, to reset the 
<tt>answerNodes</tt> set and <tt>lastQuery</tt>, and to go back 
to the `RunningSecondStage` state;

- An <tt>Answer</tt> from an agent,

   - if the <tt>replyId</tt> of the answer is different from 
   <tt>lastQueryId</tt>, the message is ignored,

   - if the <tt>replyId</tt> of the answer is equal to
   <tt>lastQueryId</tt> and not all the answers had been
    collected, the supervisor stays in the current state, it 
    updates the metrics and adds the agent to the set 
    <tt>answerNodes</tt>;

  - if the <tt>replyId</tt> of the answer is equal to 
  <tt>lastQueryId</tt> and all the answers had been collected, 
  the supervisor updates the metrics and adds the agent to the
  set <tt>answerNodes</tt>, 

    - if at least one transaction occurred in the current stage,
     the supervisor sends a <tt>PreTrigger</tt>(firstStage) to
      all the agents to initiate the first stage again and 
      resets <tt>desperateNodes</tt> before moving to the 
      `SecondToFirst` state;

    - if no transaction occurred in the current stage, the 
    supervisor sends a <tt>Kill</tt> message to the agents and moves to the `TransitionalEnd` state.


In the state `SecondToFirst`, the supervisor can 
receive the following messages:

- A <tt>ReadyForNextStage</tt> from an agent,

  - if all the agents are ready to start the next first stage, 
  the supervisor sends a <tt>Trigger</tt>(firstStage) message 
  to  them and resets its `sumReadyForNext` variable before 
  moving to the `RunningSecondStage` state;

  - if not all the agents are ready, the supervisor increases
   its `sumReadyForNext` variable and stays in the current
   state;

- An <tt>Answer</tt> from an agent or a <tt>Close</tt> message
 from itself which are ignored.


In the state `TransitionalEnd`, the supervisor can receive
the following messages:

- A <tt>Finished</tt> message from an agent,

  - if all the other agents already said they have finished,
  the supervisor sends the <tt>Outcome</tt> to the balancer 
  and moves to the `TerminalState`,

  - if not all the agents have finished, the supervisor 
  increases its `sumKilled` variable and stays in the current 
  state.

- An <tt>Answer</tt> message which is ignored.
