# Overview

SMASTA+ is a Scala implementation of the Extended Multi-agents Situated Task Allocation.
Its installation and usage are explained [here](../../../README.md).

To allocate and re-allocate tasks among themselves, the agent bargain task delegations and task swaps.
Research in automated negotiation has been primarily concerned with issues such as strategic behavior, 
incentive compatibility and privacy. Such issues do not arise in cooperative multiagent systems 
where agents share the same goal.

In this document, we give an overview of our framework.

## Big Picture

In order to execute concurrently the consumption process and the reallocation process,
we consider two types of agents:
- the node agents, each of them representing a computational node by managing its task bundle;
- the supervisor which synchronises the phases of the negotiation process.

The [supervisor behaviour](./supervisor.md) is described here.

![](bigPicture.jpg)

The consumption process consists of the concurrent or sequential execution of different tasks
by the computational nodes under the supervision of their agent. The reallocation process
consists of multiple local reallocations that are the outcomes of bilateral negotiations
between node agents, performed sequentially or concurrently. These processes are complementary.
While the consumptions take place continuously, the agents negotiate their task bundle until
they reach a stable allocation. The consumption of a task can make an allocation unstable and thus
trigger new negotiations. The consumption process ends when all tasks are executed. The final allocation,
which is empty, ends the process.

## Agent architecture

In order to reduce the design complexity of a node agent, we adopt a modular architecture that allows 
for competition of negotiations and consumption.

A node agent is a composite agent consisting of three component agents, each with a limited role:
- the worker executes (consumes) tasks;
- the negotiator maintains a belief base to negotiate tasks with its peers;
- the manager manages the task bundle of the computing node to schedule their execution by 
the worker by adding or removing tasks according to the bilateral reallocations of the negotiator.

![](compositeAgent.svg)

The [behaviour of the negotiator](./negotiator.md) is described here.
The [behaviour of the manager](./manager.md) is described here.
The [behaviour of the worker](./worker.md) is described here.


## Negotiation Protocol Specification

To achieve task reallocations, agents are involved in multiple bilateral negotiations.
Each negotiation is based on an alternating offer protocol, including four decision steps:
1. the proposer's offer strategy, which selects a delegation based on a delegation (a list of tasks in its bundle) and a responder
2. the counter-offer strategy that allows the responder to determine whether to decline the delegation, accepts it, or even makes a counter-offer
3. the possible confirmation or withdrawal of the endowment by the proposer depending on its consumption that has taken place concurrently
4the possible confirmation or withdrawal of the counterpart by the responder depending on its consumption


![](protocol.svg)


