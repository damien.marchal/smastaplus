// Copyright (C) Maxime MORGE, 2021, 2022
package org.smastaplus.utils.thread

import org.scalatest._

import scala.concurrent.{ExecutionContext, Future, TimeoutException}
import scala.concurrent.duration._
import scala.language.postfixOps

class TestFutureUtil extends org.scalatest.flatspec.AsyncFlatSpec with OptionValues with Inside with Inspectors {

  implicit override def executionContext: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  "futureWithTimeout" should "complete the users future when it returns before the timeout" in {
    def myFuture = Future[Int] {
      //println(s"starting user future ${System.currentTimeMillis()}")
      Thread.sleep((1 seconds).toMillis)
      //println(s"user future done ${System.currentTimeMillis()}")
      100
    }
    FutureUtil.futureWithTimeout(myFuture, 2 seconds).map {
      result => if(result == 100) succeed else fail()
    }
  }

  it should "not complete the future when it returns after the timeout" in {
    lazy val myFuture = Future[Int] {
      //println(s"user future waiting 4 seconds ${System.currentTimeMillis()}")
      Thread.sleep((4 seconds).toMillis)
      //println(s"user future done at ${System.currentTimeMillis()}")
      100
    }
    recoverToSucceededIf[TimeoutException] {
      FutureUtil.futureWithTimeout(myFuture, 1 seconds)
    }
  }
}