// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.strategy.deal.proposal

import org.smastaplus.core.{GlobalFlowtime, LocalFlowtime, LocalFlowtimeAndGlobalFlowtime}
import org.smastaplus.process.{Delegation, SingleDelegation}
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.deal.proposal.single.ConcreteConservativeProposalStrategy

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the single counter-proposal strategy with the first stap example
  */
class SingleProposalStrategyEx1Spec extends AnyFlatSpec {

  import org.smastaplus.example.stap.ex1._
  import org.smastaplus.example.allocation.stap1.ex1lcjf._

  val heuristic: ConcreteConservativeProposalStrategy = ConcreteConservativeProposalStrategy()

  behavior of "Example 1 with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst"

  "The delegation strategy by the donor #1 " should "selects the recipient #3 and the task t7 " in {
    val delegation : Delegation= new SingleDelegation(stap, cn1, cn3, t7)
    val mindRecipient = MindWithPeerModelling(stap, delegation.recipient, a)
    assert( delegation.isSociallyRational(a,LocalFlowtime) )
    assert( delegation.isSociallyRational(a,GlobalFlowtime) )
    val delegationStrategy = new ConcreteConservativeProposalStrategy
    assert( delegationStrategy.acceptabilityCriteria(delegation, mindRecipient, LocalFlowtime) )
    assert( delegationStrategy.acceptabilityCriteria(delegation, mindRecipient, GlobalFlowtime) )
    assert( delegationStrategy.acceptabilityRule(delegation, mindRecipient, LocalFlowtimeAndGlobalFlowtime) )
  }
}
