// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.strategy.consumption

import org.scalatest.flatspec.AnyFlatSpec

import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the consumption strategy with the first stap example
  */
class ConsumptionStrategyEx1Spec extends AnyFlatSpec {

  behavior of "Example 1"

  import org.smastaplus.example.stap.ex1._
  import org.smastaplus.example.allocation.stap1.ex1Allocation._

  "The LocallyCheapJobFirstLocallyCheapTaskFirst strategy" should
    "consume first the cheap tasks of the locally cheap jobs" in {
    val a2  = a.sort()
    assert( a2.bundle(cn1)  == List(t7, t4, t1))
    assert( a2.bundle(cn2) == List(t8, t5, t2))
    assert( a2.bundle(cn3) == List(t3, t6, t9))
    assert( a2.meanGlobalFlowtime ~= 9.0 )
  }
}
