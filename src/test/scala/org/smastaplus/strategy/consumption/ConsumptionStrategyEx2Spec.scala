// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.strategy.consumption

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the consumption strategy for the second stap example
  */
class ConsumptionStrategyEx2Spec extends AnyFlatSpec {

  behavior of "Example 2"

  import org.smastaplus.example.stap.ex2._
  import org.smastaplus.example.allocation.stap2.ex2Allocation._

  "The LocallyCheapJobFirstLocallyCheapTaskFirst strategy" should
    "consume first the cheap tasks of the locally cheap jobs" in {
    val a2 = a.sort()
    assert( a2.bundle(cn1) == List(t1, t4, t7))
    assert( a2.bundle(cn2) == List(t5, t8, t2))
    assert( a2.bundle(cn3) == List(t3, t9, t6))
    assert( a2.meanGlobalFlowtime ~= 25.00/3 )
  }
}
