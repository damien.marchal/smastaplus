// Copyright (C) Maxime MORGE 2020, 2021
package org.smastaplus.balancer.deal.mas

import org.smastaplus.core._
import akka.actor.ActorSystem
import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.strategy.deal.proposal.single.ConcreteConservativeProposalStrategy
import org.smastaplus.strategy.deal.counterproposal.single.NoneSingleCounterProposalStrategy

/**
  * Unit test for the decentralized balancer with the second stap example
  */
class AgentBasedBalancerEx2Spec extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap2.ex2lcjf._
  import org.smastaplus.example.stap.ex2._

  behavior of "Example 2 with the centralized balancer with " +
    "the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst"

  "LocalFlowtime: he delegation strategy by the donor #3 " should
    "selects the recipient #3 and the task t3 " in {
    val system : ActorSystem = ActorSystem("DecentralizedBalancer" + AgentBasedBalancer.id)
    val balancer = new AgentBasedBalancer(stap, LocalFlowtimeAndMakespan, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy,
      system, dispatcherId = "akka.actor.default-dispatcher" , monitor = false)
    balancer.trace = false
    val outcome = balancer.reallocate(a)
    system.terminate()
    assert(
      (outcome.bundle(cn1) == List(t1, t4, t7) &&
        outcome.bundle(cn2) == List(t5, t8, t2) &&
        outcome.bundle(cn3) == List(t3, t9, t6)
        )
        ||
        (outcome.bundle(cn1) == List(t4, t1, t7) &&
          outcome.bundle(cn2) == List(t5, t8, t3, t2) &&
          outcome.bundle(cn3) == List(t9, t6)
          )
    )
  }

  "GlobalFlowtime: the delegation strategy by the donor #3 " should
    "selects the recipient #3 and the task t3 " in {
    val system : ActorSystem = ActorSystem("DecentralizedBalancer" + AgentBasedBalancer.id)
    val balancer = new AgentBasedBalancer(stap, GlobalFlowtime, new ConcreteConservativeProposalStrategy, None, new NoneSingleCounterProposalStrategy,
      system, dispatcherId = "akka.actor.default-dispatcher" , monitor = false)
    balancer.trace = false
    val outcome = balancer.reallocate(a)
    system.terminate()
    assert(outcome.bundle(cn1) == List(t1, t4, t7))
    assert(outcome.bundle(cn2) == List(t5, t8, t2))
    assert(outcome.bundle(cn3) == List(t3, t9, t6))
  }
}