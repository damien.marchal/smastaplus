// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.balancer.dual

import org.smastaplus.core._
import org.smastaplus.example.stap.ex2._
import org.smastaplus.utils.MathUtils._
import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the deputation balancer for the second stap example without the heuristic
  */
class DeputationEx2Spec extends AnyFlatSpec {
  val debug =  false
  val trace = false
  val fullTrace = false

  behavior of "Example 2"

  val a = new ExecutedAllocation(stap)
  a.bundle += ( cn1 -> List(t4, t7) )
  a.bundle += ( cn2 -> List(t8, t5, t2, t1) )
  a.bundle += ( cn3 -> List(t9, t3, t6) )

  if (trace) {
    println(s"Initial allocation\n$a")
    println(s"Initial globalFlowtime: ${a.globalFlowtime}")
  }

  val balancer = new DeputationBalancer(stap, GlobalFlowtime, heuristic = false)
  if (fullTrace) balancer.debug = true
  val a2 : ExecutedAllocation = balancer.reallocate(a)

  if (trace){
    println(s"Nb of reassignments ${balancer.nbReassignments}")
    println(s"Final allocation\n$a2")
    println(s"Final globalFlowtime: ${a2.globalFlowtime}")
  }

  "The globalFlowtime" should " be 21.0" in {
    assert(a2.globalFlowtime ~= 21.0)
  }

  "The bundle of the node #1" should "be [T7, T1, T4]" in {
    if (debug) println("node #1: "+ a2.bundle(cn1).mkString("[", ", ", "]"))
    assert( a2.bundle(cn1) == List(t7, t1, t4) )
  }

  "The bundle of the node #2" should "be [T8, T5, T2]" in {
    if (debug) println("node #2: "+ a2.bundle(cn2).mkString("[", ", ", "]"))
    assert( a2.bundle(cn2) == List(t8, t5, t2) )
  }

  "The bundle of the node #3" should "be [T9, T3, T6]" in {
    if (debug) println("node #3: "+ a2.bundle(cn3).mkString("[", ", ", "]"))
    assert( a2.bundle(cn3) == List(t9, t3, t6) )
  }
}
