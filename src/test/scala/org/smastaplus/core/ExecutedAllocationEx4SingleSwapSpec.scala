// Copyright (C) Maxime MORGE, Luc BIGAND 2021, 2022
package org.smastaplus.core

import org.scalatest.flatspec.AnyFlatSpec
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the fourth example after task exchange
  */
class ExecutedAllocationEx4SingleSwapSpec extends AnyFlatSpec {

  import org.smastaplus.example.allocation.stap4.jobRelease.ex4Swap._
  import org.smastaplus.example.stap.ex4._

  behavior of "Example 3"

  "The workload" should " be the sum of the costs of the task in the bundles" in {
    assert( a.workload(cn1) ~= 8.0 )
    assert( a.workload(cn2) ~= 9.0 )
    assert( a.workload(cn3) ~= 11.0 )
    assert( a.makespan ~= 11.0 )
  }

  "The completion times of the tasks" should " include the delay" in {
    assert( a.completionTime(t1) ~= 5.0 )
    assert( a.completionTime(t2) ~= 8.0 )
    assert( a.completionTime(t3) ~= 5.0 )
    assert( a.completionTime(t4) ~= 4.0 )
    assert( a.completionTime(t5) ~= 2.0 )
    assert( a.completionTime(t6) ~= 9.0 )
    assert( a.completionTime(t7) ~= 7.0 )
    assert( a.completionTime(t8) ~= 4.0 )
    assert( a.completionTime(t9) ~= 11.0 )
  }

  "The completion times of the jobs" should " include the delay" in {
    assert( a.completionTime(j1) ~= 8.0 )
    assert( a.completionTime(j2) ~= 9.0 )
    assert( a.completionTime(j3) ~= 11.0 )
    assert( a.meanGlobalFlowtime ~= 28.0/3.0 )
  }
}