// Copyright (C) Maxime MORGE 2020, 2021, 2022
package org.smastaplus.process

import org.scalatest.flatspec.AnyFlatSpec

import org.smastaplus.core._
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.MathUtils._

/**
  * Unit test for the consumption with the first stap example
  */
class DelegationEx1Spec extends AnyFlatSpec {

  behavior of "Example 1"

  import org.smastaplus.example.stap.ex1._

  "The delegation by the donor #1 to the recipient #3 of the task t7" should
    "modify the allocation with the strategy LocallyCheapestJobFirstLocallyCheapestTaskFirst" in {
    import org.smastaplus.example.allocation.stap1.ex1lcjf._
    val delegation = new SingleDelegation(stap, cn1, cn3, t7)

    var mindDonor = MindWithPeerModelling(a.stap, delegation.donor, a)
    mindDonor = mindDonor.removeTask(t7)
    var mindRecipient = MindWithPeerModelling(a.stap, delegation.recipient, a)
    mindRecipient = mindRecipient.addTask(t7)

    assert( mindDonor.sortedTasks == List(t4, t1))
    assert( mindRecipient.sortedTasks == List(t3, t6, t7, t9))
    val a2 = delegation.execute(a)

    assert( a2.bundle(cn1) == List(t4, t1))
    assert( a2.bundle(cn2) == List(t8, t5, t2))
    assert( a2.bundle(cn3) == List(t3, t6, t7, t9))

    assert( delegation.isSociallyRational(a, LocalFlowtime) )
    assert( mindDonor.beliefWorkload(cn3) + mindDonor.stap.cost(t7,cn3) < mindDonor.workload() )
    assert( delegation.isSociallyRational(a, Makespan) )
    assert( a.meanGlobalFlowtime ~= 9.00 )
    assert( a2.meanGlobalFlowtime ~= 26.00/3 )
    assert( delegation.isSociallyRational(a, GlobalFlowtime))
  }

}
