// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.consumer.deal.mas

/**
  * Metrics for the negotiation process
  * @param sumOfProposals is the number of proposals sent, initially 0
  * @param sumOfCounterProposals is the number of counter-proposals sent, initially 0
  * @param sumOfDelegationConfirmations is the number of delegations which are confirmed, initially 0
  * @param sumOfSwapConfirmations is the number of swaps which are confirmed, initially 0
  * @param sumOfTimeouts is the number of timeout, initially 0
  * @param sumOfDelegatedTasks  is the number of delegated tasks, initially 0
  * @param nbReallocationsCurrentStage is the number of deals in the current stage, initially 0
  */
class Metrics(val sumOfProposals: Int = 0,
              val sumOfCounterProposals: Int = 0,
              val sumOfDelegationConfirmations: Int = 0,
              val sumOfSwapConfirmations: Int = 0,
              val sumOfTimeouts: Int = 0,
              val sumOfDelegatedTasks: Int = 0,
              val nbReallocationsCurrentStage: Int = 0
             ) {

  override def toString: String = s"Metrics($sumOfProposals,$sumOfCounterProposals,$sumOfDelegationConfirmations," +
    s"$sumOfSwapConfirmations,$sumOfTimeouts,$sumOfDelegatedTasks,$nbReallocationsCurrentStage)"

  /**
    * Returns the updated metrics
    */
  def update(metrics: Metrics) : Metrics = {
    new Metrics(sumOfProposals = sumOfProposals + metrics.sumOfProposals,
      sumOfCounterProposals = sumOfCounterProposals + metrics.sumOfCounterProposals,
      sumOfDelegationConfirmations = sumOfDelegationConfirmations + metrics.sumOfDelegationConfirmations,
      sumOfSwapConfirmations = sumOfSwapConfirmations + metrics.sumOfSwapConfirmations,
      sumOfTimeouts = sumOfTimeouts + metrics.sumOfTimeouts,
      sumOfDelegatedTasks = sumOfDelegatedTasks + metrics.sumOfDelegatedTasks,
      nbReallocationsCurrentStage = nbReallocationsCurrentStage + metrics.nbReallocationsCurrentStage)
  }

  /**
    * Returns the metrics where the number of proposals is incremented
    */
  def incrementProposals() : Metrics = new Metrics(sumOfProposals = sumOfProposals +1 ,
    sumOfCounterProposals = sumOfCounterProposals,
    sumOfDelegationConfirmations = sumOfDelegationConfirmations,
    sumOfSwapConfirmations = sumOfSwapConfirmations,
    sumOfTimeouts = sumOfTimeouts,
    sumOfDelegatedTasks = sumOfDelegatedTasks,
    nbReallocationsCurrentStage = nbReallocationsCurrentStage)


  /**
    * Returns the metrics where the number of counter-proposals is incremented
    */
  def incrementCounterProposals() : Metrics = new Metrics(sumOfProposals = sumOfProposals,
    sumOfCounterProposals = sumOfCounterProposals + 1,
    sumOfDelegationConfirmations = sumOfDelegationConfirmations,
    sumOfSwapConfirmations = sumOfSwapConfirmations,
    sumOfTimeouts = sumOfTimeouts,
    sumOfDelegatedTasks = sumOfDelegatedTasks,
    nbReallocationsCurrentStage = nbReallocationsCurrentStage)

  /**
    * Returns the metrics where the number of delegations is incremented
    */
  def incrementDelegations() : Metrics = new Metrics(sumOfProposals = sumOfProposals,
    sumOfCounterProposals = sumOfCounterProposals,
    sumOfDelegationConfirmations = sumOfDelegationConfirmations + 1,
    sumOfSwapConfirmations = sumOfSwapConfirmations,
    sumOfTimeouts = sumOfTimeouts,
    sumOfDelegatedTasks = sumOfDelegatedTasks,
    nbReallocationsCurrentStage = nbReallocationsCurrentStage + 1)

  /**
    * Returns the metrics where the number of swaps is incremented
    */
  def incrementSwaps() : Metrics = new Metrics(sumOfProposals = sumOfProposals,
    sumOfCounterProposals = sumOfCounterProposals,
    sumOfDelegationConfirmations = sumOfDelegationConfirmations,
    sumOfSwapConfirmations = sumOfSwapConfirmations + 1,
    sumOfTimeouts = sumOfTimeouts,
    sumOfDelegatedTasks = sumOfDelegatedTasks,
    nbReallocationsCurrentStage = nbReallocationsCurrentStage + 1)

  /**
    * Returns the metrics where the number of timeouts is incremented
    */
  def incrementTimeout() : Metrics = new Metrics(sumOfProposals = sumOfProposals,
    sumOfCounterProposals = sumOfCounterProposals,
    sumOfDelegationConfirmations = sumOfDelegationConfirmations,
    sumOfSwapConfirmations = sumOfSwapConfirmations,
    sumOfTimeouts = sumOfTimeouts + 1,
    sumOfDelegatedTasks = sumOfDelegatedTasks,
    nbReallocationsCurrentStage = nbReallocationsCurrentStage)

  /**
    * Returns the metrics where the number of delegated tasks is incremented
    */
  def addDelegatedTasks(nbDelegatedTasks : Int) : Metrics = new Metrics(sumOfProposals = sumOfProposals,
    sumOfCounterProposals = sumOfCounterProposals,
    sumOfDelegationConfirmations = sumOfDelegationConfirmations,
    sumOfSwapConfirmations = sumOfSwapConfirmations,
    sumOfTimeouts = sumOfTimeouts,
    sumOfDelegatedTasks = sumOfDelegatedTasks + nbDelegatedTasks,
    nbReallocationsCurrentStage = nbReallocationsCurrentStage)


  /**
    * Returns the metrics where the number of deals of current stage is reset
    */
  def resetNbReallocationCurrentStage() : Metrics = new Metrics(sumOfProposals = sumOfProposals,
    sumOfCounterProposals = sumOfCounterProposals,
    sumOfDelegationConfirmations = sumOfDelegationConfirmations,
    sumOfSwapConfirmations = sumOfSwapConfirmations,
    sumOfTimeouts = sumOfTimeouts,
    sumOfDelegatedTasks = sumOfDelegatedTasks,
    nbReallocationsCurrentStage = 0)

  /**
    * Returns the reset metrics
    */
  def reset() : Metrics = new Metrics(sumOfProposals = 0,
    sumOfCounterProposals = 0,
    sumOfDelegationConfirmations = 0,
    sumOfSwapConfirmations = 0,
    sumOfTimeouts = 0,
    sumOfDelegatedTasks = 0,
    nbReallocationsCurrentStage = 0)

}
