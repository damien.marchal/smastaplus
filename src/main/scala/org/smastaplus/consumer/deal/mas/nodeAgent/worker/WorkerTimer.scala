// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022, 2023
package org.smastaplus.consumer.deal.mas.nodeAgent.worker

import org.smastaplus.consumer.deal.mas._

import akka.actor.{Actor, Timers}
import scala.concurrent.duration._

/** Timer actor which makes a countdown.
  *
  * @param duration of the timer in milliseconds
  * @param tickInterval is the number of milliseconds between ticks for the task progression
  */
class WorkerTimer(duration: Int, tickInterval: Int) extends Actor with Timers{
  import org.smastaplus.utils.Timer._
  private case object TimerKey
  private case object Tick
  var progression : Int = 0

  /**
   * Message handling
   */
  def receive: Receive = this.active orElse this.handleUnexpected

  /**
   * Start method
   */
  override def preStart(): Unit = {
    timers.startTimerWithFixedDelay(TimerKey, Tick, tickInterval.millis)
    super.preStart()
  }

  /**
   * Active state of the worker timer
   * */
  def active: Receive = {

    case Start if sender() == context.parent =>
      timers.startTimerWithFixedDelay(TimerKey, Tick, tickInterval.millis)

    case Tick if progression < duration-tickInterval =>
      progression += tickInterval

    case Tick if progression >= duration-tickInterval =>
      context.parent ! Timeout
      timers.cancel(TimerKey)
      context stop self

    case QueryRemainingWork =>
      sender() ! TaskProgression(progression)

  }

  /** Handle unexpected messages */
  def handleUnexpected: Receive = {
    case msg@_ =>
      throw new RuntimeException(s"ERROR WorkerTimer was not expected message $msg")
  }
}
