// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.consumer.deal.mas.nodeAgent.worker

import org.smastaplus.core.{ComputingNode, STAP, Task}

import scala.annotation.unused

/**
  *  Worker's state of mind
  * @param me is the node I work for
  * @param stap instance
  */
final class WorkerMind(me: ComputingNode, stap: STAP){

  val debug = false
  val debugUpdate = false

  private var inProgressTask : Option[Task] = None
  var remainingWork : Option[Double] = None

  /**
    * Auxiliary  constructor
    */
  def this(me: ComputingNode,
            stap: STAP,
            inProgressTask : Option[Task],
            remainingWork: Option[Double]) = {
    this(me, stap)
    this.inProgressTask = inProgressTask
    this.remainingWork = remainingWork
 }

  override def toString: String = super.toString + s" and the current task is $inProgressTask"

  @unused
  def updateTaskInProgress(task: Task, remainingCost: Double) : WorkerMind = {
    new WorkerMind(me, stap, Some(task), Some(remainingCost))
  }
}
