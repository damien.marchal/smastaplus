// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2022
package org.smastaplus.consumer.deal.mas.nodeAgent.negotiator

import org.smastaplus.consumer.deal.mas.State

/**
  * Trait representing as state in a FSM
  * for the negotiation behaviour
  */
trait NegotiationState extends State
case object Initial extends NegotiationState
case object Waiting extends NegotiationState
case object Responder extends NegotiationState
case object Proposer extends NegotiationState
case object Swapper extends NegotiationState
case object Contractor extends NegotiationState
case object Pause extends NegotiationState
case object EndStage extends NegotiationState
case object TransitionState extends NegotiationState