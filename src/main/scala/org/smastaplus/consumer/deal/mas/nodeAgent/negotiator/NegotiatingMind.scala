// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2020, 2021, 2022
package org.smastaplus.consumer.deal.mas.nodeAgent.negotiator

import org.smastaplus.core.{ComputingNode, STAP, Task}
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.consumption._
import org.smastaplus.provisioning.ProvisioningUsage

import org.joda.time.LocalDateTime
import scala.annotation.unused

/**
  *  Negotiator's state of mind
  * @param me is the node I represent
  * @param peers are nodes managed by the other agents
  * @param usage are the the provisioning usages of the computing nodes
  * @param stap instance
  * @param taskSet tasks assigned to the agent
  * @param beliefBase about the other agents
  * @param informers is the set of peers which have sent Inform messages
  * @param lastOffer is the delegation (hashCode) suggested in the last proposal
  * @param isProactive is true if the proactive behaviour of the agent as a proposer is already activated
  * @param isDesperate  is true if the triggering of the proactive behaviour fails since a new offer cannot be proposed
  * @param isFirstStage  is true if the agent is in the first stage, false otherwise.
  */
final class NegotiatingMind(me: ComputingNode,
                            peers: Set[ComputingNode],
                            usage: Map[ComputingNode, ProvisioningUsage],
                            stap: STAP, taskSet: Set[Task],
                            beliefBase: BeliefBase = new BeliefBase(),
                            val informers: Set[ComputingNode] = Set.empty,
                            val lastOffer: Option[Int] = None,
                            val isProactive: Boolean,
                            val isDesperate: Boolean,
                            val isFirstStage: Boolean)
  extends MindWithPeerModelling(me, peers, usage, stap, taskSet, beliefBase) {

  debug = false
  val debugUpdate = false

  bundle = new Bundle(stap, me).setBundle(taskSet)

  //the timestamp corresponding to the date of the last update of the bundle
  private var timestamp: LocalDateTime = LocalDateTime.now()

  /**
    * Auxiliary  constructor
    */
  def this(me: ComputingNode,
           peers: Set[ComputingNode],
           usage: Map[ComputingNode, ProvisioningUsage],
           stap: STAP,
           bundle: Bundle,
           beliefBase: BeliefBase,
           informers: Set[ComputingNode],
           lastProposalId: Option[Int],
           isReactivated: Boolean,
           isDesperate: Boolean,
           isFirstStage: Boolean,
           timestamp: LocalDateTime) = {
    this(me, peers, usage, stap, Set.empty[Task], beliefBase, informers, lastProposalId, isReactivated, isDesperate, isFirstStage)
    this.bundle = bundle
    this.timestamp = timestamp
  }

  override def toString: String = super.toString + s" and the informers $informers, the lastProposalId $lastOffer, isProactive is $isProactive and isDesperate is $isDesperate"

  /**
    * Returns a copy
    */
  override def copy(): NegotiatingMind = {
    val copyOfBeliefBase: BeliefBase = beliefBase.copy() //beliefBase
    val copyOfBundle = this.bundle.copy()
    new NegotiatingMind(me, peers, usage, stap, copyOfBundle, copyOfBeliefBase, informers, lastOffer, isProactive, isDesperate, isFirstStage, timestamp)
  }

  /**
    * Updates the timestamp with the value given in parameter
    */
  def updateTimestamp(newTimestamp: LocalDateTime) : Unit = {
    timestamp = newTimestamp
  }

  /**
    * Returns the timestamp of the bundle
    */
  def getTimestamp : LocalDateTime = timestamp

  /**
    * Updates the belief base with the insight of a node
    */
  override def updateBeliefBase(node: ComputingNode, insight: Insight): NegotiatingMind = {
    val updatedBeliefBase = beliefBase.update(node,insight)
    if (debug) println(s"NegotiatingMind>$me 's belief base: $updatedBeliefBase")
    new NegotiatingMind(me, peers, usage, stap, bundle, updatedBeliefBase, informers, lastOffer, isProactive, isDesperate, isFirstStage, timestamp)
  }

  /**
    * Updates the belief base with the task in progress and its remaining work
    */
  def updateBeliefBaseWithWorkInProgress(taskInProgress: Option[Task], remainingWork: Option[Double]): NegotiatingMind = {
    val updatedBeliefBase = beliefBase.updateWorkInProgress(taskInProgress, remainingWork)
    if (debug) println(s"NegotiatingMind>$me 's belief base: $updatedBeliefBase")
    new NegotiatingMind(me, peers, usage, stap, bundle, updatedBeliefBase, informers, lastOffer, isProactive, isDesperate, isFirstStage, timestamp)
  }

  /**
    * Sets and sorts the bundle of the mind
    */
  override def setTaskSet(updatedBundle: Set[Task]) : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, this.bundle.setBundle(updatedBundle), beliefBase, informers, lastOffer, isProactive, isDesperate, isFirstStage, timestamp)

  /**
   * Sets the bundle of the mind
   */
  override def setBundle(updatedBundle: Bundle) : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, updatedBundle, beliefBase, informers, lastOffer, isProactive, isDesperate, isFirstStage, timestamp)

  /**
   * Return the bundle
   */
  def getBundle : Bundle = {
    bundle
  }
  


  /**
    * Returns true if all the initial information have
    * been received
    */
  @unused
  def isFullyInformed: Boolean = informers.size == peers.size

  /**
    * Returns if and how the negotiation mind is updated
    * with the insight of a peer and
    */
  def isUpdateInsight(peer: ComputingNode, insight: Insight): (Boolean, NegotiatingMind) = {
    if (beliefBase.belief.contains(peer) && beliefBase.belief(peer).equals(insight)) {
      if (debugUpdate) println(s"NegotiatingMind>$me with $bundle does no need to update its belief base with insight from $peer")
      return (false,this)
    }
    if (debugUpdate) println(s"NegotiatingMind>$me updates insight for $peer: $insight")
    // updates belief base
    (true, updateBeliefBase(peer, insight)) // since we assume LocallyCheapestJobFirst
    }

  /**
    * Returns the negotiation mind which is updated
    * with the insight of a peer
    */
  def updateInsight(peer: ComputingNode, insight: Insight): NegotiatingMind = {
    isUpdateInsight(peer: ComputingNode, insight: Insight)._2
  }

  /**
    * Updates the negotiation mind with a new informer
    */
  def updateInformers(peer: ComputingNode): NegotiatingMind = {
    if (debug) println(s"NegotiatingMind>$me updates its informers with $informers")
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers = informers + peer, lastOffer, isProactive, isDesperate, isFirstStage, timestamp)
  }

  /**
    * Reset informers
    */
  def resetInformers : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, Set.empty[ComputingNode], lastOffer, isProactive, isDesperate, isFirstStage, timestamp)

  /**
    * Initializes the negotiation mind with a new bundle
    */
  def initBundle(initialBundle: List[Task]): NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, initialBundle.toSet, beliefBase, informers, lastOffer, isProactive, isDesperate, isFirstStage)

  /**
    * Sets the last offer
    */
  def setLastProposalId(msgId : Int) =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, Some(msgId), isProactive, isDesperate, isFirstStage, timestamp)

  /**
    * Reset the last offer
    */
  def resetLastOffer : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, None, isProactive, isDesperate, isFirstStage, timestamp)

  /**
    * Reactivate the proactive behaviour
    */
  def reactivate : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, lastOffer, true, isDesperate, isFirstStage, timestamp)

  /**
    * Deactivate the proactive behaviour
    */
  def deactivate : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, lastOffer, false, isDesperate, isFirstStage, timestamp)

  /**
    * Agent has no more proposal
    */
  def desperate : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, lastOffer, isProactive, true, isFirstStage, timestamp)

  /**
    * Reset proactive and desperate booleans
    */
  def resetAttitude : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, lastOffer, false, false, isFirstStage, timestamp)

  /**
    * Go to the first stage
    */
  @unused
  def firstStage : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, lastOffer, isProactive, isDesperate, true, timestamp)

  /**
    * Go th the second stage
    */
  @unused
  def secondStage : NegotiatingMind =
    new NegotiatingMind(me, peers, usage, stap, bundle, beliefBase, informers, lastOffer, isProactive, isDesperate, false, timestamp)
}