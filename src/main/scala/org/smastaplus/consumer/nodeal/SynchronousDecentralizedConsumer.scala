// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2021, 2023
package org.smastaplus.consumer.nodeal

import org.smastaplus.core._
import org.smastaplus.consumer.SynchronousConsumer
import org.smastaplus.consumer.deal.mas.{Init, Outcome}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout

/**
  * Synchronous and decentralized execution of the tasks
  * @param stap instance
  * @param system of actors
  * @param name of the consumer
  * @param dispatcherId is the name of the dispatcher "akka.actor.default-dispatcher" or "single-thread-dispatcher"
  * @param monitor is true if the completion times are monitored during the solving
  * @param simulated cost with  with a perfect information of the running environment by default
  * @param ssi true if sequential single-item auction is applied
  * @param psi true if parallel single-item auction is applied
  */
class SynchronousDecentralizedConsumer(stap: STAP,
                                       val system: ActorSystem, name: String,
                                       val dispatcherId: String,
                                       monitor: Boolean = false,
                                       simulated: SimulatedCost = new SimulatedCostE(),
                                       val ssi: Boolean,
                                       val psi: Boolean)
  extends SynchronousConsumer(stap, name, monitor, simulated){

  val TIMEOUT_VALUE : FiniteDuration = 6000 minutes // Default timeout of a run
  implicit val timeout : Timeout = Timeout(TIMEOUT_VALUE)

  // Launch a new distributor
  private val distributor : ActorRef = system.actorOf(Props(classOf[Distributor], stap, simulated, dispatcherId, monitor, ssi, psi)
    withDispatcher dispatcherId, name = "Distributor")

  /**
    * Run the consumption process and returns the empty allocation
    */
  def consume(allocation: ExecutedAllocation) : ExecutedAllocation = {
    allocation.simulatedCost = simulated
    stap.resetReleaseTime()
    init(allocation)
    val future = distributor ? Init(allocation)
    val result = Await.result(future, timeout.duration).asInstanceOf[Outcome]
    if (debug) println(s"DecentralizedConsumption ends")
    nbDeals = result.metrics.sumOfDelegationConfirmations
    nbDelegations = result.metrics.sumOfDelegationConfirmations
    nbSwaps = result.metrics.sumOfSwapConfirmations
    nbFirstStages = result.nbFirstStages
    nbSecondStages = result.nbSecondStages
    nbDelegatedTasks = result.metrics.sumOfDelegatedTasks
    nbTimeouts = result.metrics.sumOfTimeouts
    avgEndowmentSize = result.metrics.sumOfDelegatedTasks.toDouble / (result.metrics.sumOfDelegationConfirmations + result.metrics.sumOfSwapConfirmations).toDouble
    if (avgEndowmentSize.isNaN) avgEndowmentSize = 0.0
    localRatio = result.allocation.localAvailabilityRatio
    result.allocation
  }

  /**
    * Closes the consumer
    */
  def close() : Unit = {
      system.terminate()
  }
}