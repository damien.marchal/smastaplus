// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2023
package org.smastaplus.consumer.nodeal

import org.smastaplus.core._
import org.smastaplus.provisioning.{Active, ProvisioningUsage}
import org.smastaplus.utils.serialization.core.{AllocationWriter, BundleWriter}
import org.smastaplus.consumer.deal.mas._
import org.smastaplus.consumer.deal.mas.nodeAgent.manager.ManagerMind
import org.smastaplus.consumer.deal.mas.nodeAgent.worker.WorkerBehaviour
import org.smastaplus.consumer.monitor.Monitor

import java.io._
import akka.actor.{Actor, ActorRef, FSM, Props, Stash}
import com.typesafe.config.{Config, ConfigFactory}
import org.smastaplus.balancer.SSIBalancer

/**
  * Distributor which consume an allocation
  * @param stap instance
  * @param simulated cost
  * @param dispatcherId is the name of the dispatcher "akka.actor.default-dispatcher" or "single-thread-dispatcher"
  * @param isMonitored is true if the ongoing consumption is monitored
  * @param ssi true if sequential single-item auction is applied
  * @param psi true if parallel single-item auction is applied
  **/
class Distributor(stap: STAP, simulated: SimulatedCost,
                  val dispatcherId: String,
                  isMonitored: Boolean,
                  val ssi: Boolean,
                  val psi: Boolean)
  extends Actor with Stash
    with FSM[DistributorState, DistributorStatus] {

  var debug = false
  var trace = false
  val debugMetrics = false
  val debugState = false
  private val debugBundle = false

  private var metrics: Metrics = new Metrics(
    sumOfProposals = 0,
    sumOfCounterProposals = 0,
    sumOfDelegationConfirmations = 0,
    sumOfSwapConfirmations = 0,
    sumOfTimeouts = 0,
    sumOfDelegatedTasks = 0,
    nbReallocationsCurrentStage = 0
  )

  // Lgo file
  def folder : String = "./"
  def fileName : String = folder + "log.txt"
  var outputStream: OutputStream = OutputStream.nullOutputStream
  var outputStreamWriter = new OutputStreamWriter(outputStream)
  var writer = new PrintWriter(outputStreamWriter, true) // rather than System.out

  // Monitor file
  val config: Config = ConfigFactory.load()
  var pathName: String = config.getString("path.smastaplus") + "/" +
    config.getString("path.monitoringDirectory") + "/trace/"

  // The acquaintance of the distributor
  private var distributedConsumptionSystem: ActorRef = context.parent // The parent
  var directory = new Directory() // White page for the agents

  val scheduler: Option[ActorRef] = if (psi)
    Some(context.actorOf(Props(classOf[Scheduler], stap, GlobalFlowtime)
      .withDispatcher(dispatcherId), "Scheduler"))
  else None

  // Eventually build the monitor
  private val logName = if (ssi) "ssi" else if (psi) "psi" else "noNegotiation"
  val monitor: Option[ActorRef] = if (isMonitored)
    Some(context.actorOf(Props(classOf[Monitor], stap, logName, None, None)
      .withDispatcher(dispatcherId), "Monitor"))
  else None

  /**
    * Initially the allocation and the minds are empty
    */
  startWith(
    Running,
    new DistributorStatus(allocation = new ExecutedAllocation(stap), minds = Map[ComputingNode, ManagerMind]())
  )

  /**
    * Method invoked after starting the actor
    */
  override def preStart(): Unit = {
    stap.ds.computingNodes.foreach{ node: ComputingNode => // For each node
      val actor = context.actorOf(// build the worker
        Props(classOf[WorkerBehaviour], stap, node, simulated, None, true)// ticksActivated is true as AgentBasedConsumer
          .withDispatcher(dispatcherId), node.name)
      directory.add(node, actor)// add it to the directory
    }
  }

  /**
    * In the running state the distributor gives tasks to the workers
    * until all the tasks are consumed
    */
  when(Running) {
    /**
     *     When the allocation is initialized
     */
    case Event(Init(allocation), status) =>
      distributedConsumptionSystem = sender()
      // Set up the initial allocation
      var updatedStatus = if (ssi) {
        val balancer = new SSIBalancer(stap, GlobalFlowtime)
        val balancedAllocation = balancer.balance(allocation)
        // Metrics
        metrics = new Metrics(
          sumOfProposals = allocation.tasks.size * stap.ds.computingNodes.size,
          sumOfCounterProposals = 0,
          sumOfDelegationConfirmations = balancer.nbDeals,
          sumOfSwapConfirmations = 0,
          sumOfTimeouts = 0,
          sumOfDelegatedTasks = balancer.nbDelegatedTasks,
          nbReallocationsCurrentStage = balancer.nbDeals
        )
        new DistributorStatus(balancedAllocation, status.minds)
      }else new DistributorStatus(allocation, status.minds)

      // Eventually set up the initial allocation of the monitor
      if (isMonitored) {
        val config: Config = ConfigFactory.load()
        val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
          config.getString("path.monitoringDirectory")
        val writer = new AllocationWriter(monitoringDirectoryName + "/allocation.json", updatedStatus.allocation, "monitoring")
        writer.write()
        monitor.get ! Init(allocation)
      }

      // Start the consumption
      directory.allActors().foreach { actor: ActorRef => // For each worker
        val node = directory.nodeOf(actor)
        val taskList : List[Task] = updatedStatus.allocation.bundle(node)
        // Setup the corresponding ManagerMind
        val mind = new ManagerMind(node,
          peers = stap.ds.computingNodes.toSet - node,
          usage = Map[ComputingNode, ProvisioningUsage]() ++ stap.ds.computingNodes.view.map(i => i -> Active),
          stap,
          taskSet = taskList.toSet,
          isFirstStage = true)
        // Setup the first task to perform if any
        val nextTask : Option[Task] =if (!mind.getBundle.isEmpty)
          Some(mind.getBundle.nextTaskToExecute)
        else None
        // Update mind
        if (nextTask.isDefined) { // Setup the first task to perform if any
          updatedStatus = updatedStatus.updateMind(node, mind.setTaskInProgress(nextTask))
          if (debug) println(s"DistributorStatus: next task of $node : ${updatedStatus.minds(node).taskInProgress}")
        } else updatedStatus = updatedStatus.updateMind(node, mind)
        if (debug || debugBundle) println(s"DistributorStatus: bundle of $node in mind: ${updatedStatus.minds(node).getBundle}")

        // Update the allocation
        updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateTaskInProgress(node,nextTask))
        if (debug || debugBundle) println(s"DistributorStatus: bundle of $node in allocation: ${updatedStatus.allocation.bundle(node)}")

        // Activate the worker
        if (nextTask.isDefined) actor ! Perform(nextTask.get)

        // Monitor the mind
        if (isMonitored) {
          new BundleWriter(pathName + node.name + ".json", updatedStatus.allocation.getTheBundle(node), "monitoring").write()
          monitor.get ! Notify(node, updatedStatus.minds(node).getCurrentTasks, updatedStatus.minds(node).insight())
        }
      }
      // Monitor the allocation
      if (isMonitored) {
        monitor.get ! Init(allocation)
        if (debugMetrics) println(s"${updatedStatus.allocation.realGlobalMeanFlowtime}")
      }
      if (debug) println(s"Initial allocation: ${updatedStatus.allocation}")
      if (debug) println(s"Initial tasks in progress: ${updatedStatus.allocation.tasksInProgress}")

      // Activate the scheduler
      if (psi) scheduler.get ! Reschedule(stap.ds.computingNodes,updatedStatus.allocation,updatedStatus.allocation.pendingTasks())
      stay() using updatedStatus

    /**
     * When the scheduler suggest a delegation
     */
    case Event(Suggest(delegation), status) =>
      var updatedStatus = status
      if (debug) {
        println(s"Distributor: current allocation=\n ${updatedStatus.allocation}")
        println(s"Distributor: scheduler suggestion= $delegation")
        println(s"Distributor donor mind= ${updatedStatus.minds(delegation.donor)}")
      }
      metrics =  metrics.incrementProposals()
      // If the suggestions is up to date, i.e. none task in the endowment is a task in progress or a completed task
      if (delegation.endowment.forall(updatedStatus.allocation.bundle(delegation.donor).contains)){ // Update the allocation and the minds
        updatedStatus = updatedStatus.updateAllocation(delegation.execute(updatedStatus.allocation))
        updatedStatus = updatedStatus.updateMind(delegation.donor, updatedStatus.minds(delegation.donor).removeTasks(delegation.endowment))
        updatedStatus = updatedStatus.updateMind(delegation.recipient, updatedStatus.minds(delegation.recipient).addTasks(delegation.endowment))
        metrics = metrics.incrementDelegations()
        if (isMonitored) { //  Monitor the minds
          new BundleWriter(pathName + delegation.donor + ".json", updatedStatus.allocation.getTheBundle(delegation.donor), "monitoring").write()
          monitor.get ! Notify(delegation.donor, updatedStatus.minds(delegation.donor).getCurrentTasks, updatedStatus.minds(delegation.donor).insight())
          new BundleWriter(pathName + delegation.recipient + ".json", updatedStatus.allocation.getTheBundle(delegation.recipient), "monitoring").write()
          monitor.get ! Notify(delegation.recipient, updatedStatus.minds(delegation.recipient).getCurrentTasks, updatedStatus.minds(delegation.recipient).insight())
          monitor.get ! Suggest(delegation)
        }
      }

      // Re-schedule the other pending tasks
      scheduler.get ! Reschedule(stap.ds.computingNodes,updatedStatus.allocation, updatedStatus.allocation.pendingTasks().diff(delegation.endowment))
      stay() using updatedStatus

    /**
      * When the allocation is updated
      */
    case Event(Update(additionalAllocation), status) =>
      // Update the allocation
      var updatedStatus = if (ssi) { // If the distributor considers Sequential single-issue auctions
        val balancer = new SSIBalancer(stap, GlobalFlowtime)
        val fullAllocation = status.allocation.update(additionalAllocation)
        val reallocation = balancer.balance(fullAllocation)
        // Metrics
        metrics = new Metrics(
          sumOfProposals = metrics.sumOfProposals + fullAllocation.tasks.size * stap.ds.computingNodes.size,
          sumOfCounterProposals = 0,
          sumOfDelegationConfirmations = metrics.sumOfDelegationConfirmations + balancer.nbDeals,
          sumOfSwapConfirmations = 0,
          sumOfTimeouts = 0,
          sumOfDelegatedTasks = metrics.sumOfDelegatedTasks + balancer.nbDelegatedTasks,
          nbReallocationsCurrentStage = metrics.nbReallocationsCurrentStage + balancer.nbDeals
        )
        new DistributorStatus(reallocation, status.minds)
      } else new DistributorStatus(status.allocation.update(additionalAllocation), status.minds)

      // Monitor the allocation
      if (isMonitored) {
        val config: Config = ConfigFactory.load()
        val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
          config.getString("path.monitoringDirectory")
        val writer = new AllocationWriter(monitoringDirectoryName + "/allocation.json", updatedStatus.allocation, "monitoring")
        writer.write()
        monitor.get ! Init(updatedStatus.allocation)
      }

      // Update the minds
      directory.allActors().foreach { actor: ActorRef => // For each worker
        val node = directory.nodeOf(actor)
        // Setup the corresponding ManagerMind
        val mind = new ManagerMind(me = node,
          peers = stap.ds.computingNodes.toSet - node,
          usage = Map[ComputingNode, ProvisioningUsage]() ++ stap.ds.computingNodes.view.map(i => i -> Active),
          stap,
          updatedStatus.allocation.getTheBundle(node),
          isFirstStage = true,
          status.minds(node).taskInProgress,
          status.minds(node).remainingWork,
          status.minds(node).completedTasks
        )
        if (mind.taskInProgress.isEmpty && mind.getBundle.nonEmpty) { // If a new task can be consumed
          val nextTask = Some(mind.getBundle.nextTaskToExecute)
          // Update the mind
          updatedStatus = updatedStatus.updateMind(node, mind.setTaskInProgress(nextTask))
          // Update the allocation
          updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateTaskInProgress(node, nextTask))
          // Activate the worker
          actor ! Perform(nextTask.get)
          if (debug) println(s"DistributorStatus: next task of $node : ${updatedStatus.minds(node).taskInProgress}")
        }
        else updatedStatus = updatedStatus.updateMind(node, mind)

        // Monitor the mind
        if (isMonitored) {
          new BundleWriter(pathName + node.name + ".json", updatedStatus.allocation.getTheBundle(node), "monitoring").write()
          monitor.get ! Notify(node, updatedStatus.minds(node).getCurrentTasks, updatedStatus.minds(node).insight())
        }
      }

      // Monitor the allocation
      if (isMonitored) {
        monitor.get ! Init(updatedStatus.allocation)
        if (debugMetrics) println(s"${updatedStatus.allocation.realGlobalMeanFlowtime}")
      }
      if (debug) println(s"Initial tasks in progress: ${updatedStatus.allocation.tasksInProgress}")

      // Activate the scheduler
      if (psi) scheduler.get ! Reschedule(stap.ds.computingNodes, updatedStatus.allocation, updatedStatus.allocation.pendingTasks())
      stay() using updatedStatus

    /**
     *  When there is still a task to give to the worker
     */
    case Event(Done(completionDate), status)  if status.allocation.bundle(directory.nodeOf(sender())).nonEmpty  =>
      if (debug) println(s"Case 1: there is still a task to give to the worker")
      var updatedStatus = new DistributorStatus(status.allocation, status.minds)
      val node = directory.nodeOf(sender())
      val mind = status.minds(node)
      val completedTask = mind.taskInProgress.get
      if (debug) println(s"Complete task: $completedTask")

      // The next task to perform
      val nextTask: Option[Task] = Some(mind.getBundle.nextTaskToExecute)

      // Update mind
      updatedStatus = updatedStatus.updateMind(node,updatedStatus.minds(node).updateCompletedTasks(completedTask, completionDate))
      updatedStatus = updatedStatus.updateMind(node,updatedStatus.minds(node).setTaskInProgress(nextTask))
      if (debug){
        println(s"Distributor: bundle of $node in mind: ${updatedStatus.minds(node).getBundle}")
        println(s"Distributor: next task of $node : $nextTask")
      }

      // Update the allocation
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.removeTask(node, completedTask))
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateCompletedTask(node,completedTask,completionDate))
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateTaskInProgress(node, nextTask))
      if (debug) println(s"DistributorStatus: bundle of $node in allocation: ${updatedStatus.allocation.bundle(node)}")

      // Activate the worker
      sender() ! Perform(nextTask.get)

      // Monitor the mind and the consumption
      if (isMonitored) {
        new BundleWriter(pathName + node.name + ".json", updatedStatus.allocation.getTheBundle(node), "monitoring").write()
        monitor.get ! Notify(node, updatedStatus.minds(node).getCurrentTasks, updatedStatus.minds(node).insight())
        monitor.get ! NotifyDone(node, completedTask, completionDate)
        if (debugMetrics) println(s"${updatedStatus.allocation.realGlobalMeanFlowtime}")
      }
      if (debug) println(s"Tasks in progress: ${updatedStatus.allocation.tasksInProgress}")

      // Reschedule the pending tasks with the other nodes
      if (psi) scheduler.get ! Reschedule(stap.ds.computingNodes.diff(Set(node)), updatedStatus.allocation, updatedStatus.allocation.pendingTasks())
      stay() using updatedStatus

    /**
     *  When there is no more task for this worker, but there are still tasks for others
      */
    case Event(Done(completionDate), status) if status.allocation.bundle(directory.nodeOf(sender())).isEmpty&&
      status.allocation.tasksInProgress.values.collect { case Some(task) => task }.size != 1 =>
      if (debug) println(s"Case 2: there is no more task for this worker, but there are still tasks for others")
      var updatedStatus = new DistributorStatus(status.allocation, status.minds)
      val node = directory.nodeOf(sender())
      val mind = status.minds(node)
      val bundle = status.allocation.bundle(node)
      val completedTask = mind.taskInProgress.get
      if (debug) {
        println(s"Complete task: $completedTask")
        println(s"Distributor: bundle of $node : $bundle")
      }

      // There is no more task to perform
      val nextTask: Option[Task] = None

      // Update mind
      updatedStatus = updatedStatus.updateMind(node, updatedStatus.minds(node).updateCompletedTasks(completedTask, completionDate))
      updatedStatus = updatedStatus.updateMind(node, updatedStatus.minds(node).setTaskInProgress(nextTask))
      if (debug) {
        println(s"Distributor: bundle of $node in mind: ${updatedStatus.minds(node).getBundle}")
        println(s"Distributor: next task of $node : $nextTask")
      }

      // Update the allocation
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.removeTask(node, completedTask))
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateCompletedTask(node,completedTask,completionDate))
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateTaskInProgress(node, nextTask))
      if (debug) println(s"DistributorStatus: bundle of $node in allocation: ${updatedStatus.allocation.bundle(node)}")

      // Monitor the mind and the consumption
      if (isMonitored) {
        new BundleWriter(pathName + node.name + ".json", updatedStatus.allocation.getTheBundle(node), "monitoring").write()
        monitor.get ! Notify(node, updatedStatus.minds(node).getCurrentTasks, updatedStatus.minds(node).insight())
        monitor.get ! NotifyDone(node, completedTask, completionDate)
        if (debugMetrics) println(s"${updatedStatus.allocation.realGlobalMeanFlowtime}")
      }
      if (debug) println(s"Tasks in progress: ${updatedStatus.allocation.tasksInProgress}")

      // Reschedule the pending tasks with the other nodes
      if (psi) scheduler.get ! Reschedule(stap.ds.computingNodes.diff(Set(node)), updatedStatus.allocation, updatedStatus.allocation.pendingTasks())
      stay() using updatedStatus

    /**
     * When there is no more task for this worker, but there no more tasks for others
     */
    case Event(Done(completionDate), status) if status.allocation.bundle(directory.nodeOf(sender())).isEmpty &&
      status.allocation.tasksInProgress.values.collect { case Some(task) => task }.size == 1 =>
      if (debug) println(s"Case 3: there is no more task for this worker, but there no more tasks for others")
      var updatedStatus = new DistributorStatus(status.allocation, status.minds)
      val node = directory.nodeOf(sender())
      val mind = status.minds(node)
      val bundle = status.allocation.bundle(node)
      val completedTask = mind.taskInProgress.get
      if (debug) {
        println(s"Complete task: $completedTask")
        println(s"Distributor: bundle of $node : $bundle")
      }

      // There is no more task to perform
      val nextTask: Option[Task] = None

      // Update mind
      updatedStatus = updatedStatus.updateMind(node, updatedStatus.minds(node).updateCompletedTasks(completedTask, completionDate))
      updatedStatus = updatedStatus.updateMind(node, updatedStatus.minds(node).setTaskInProgress(nextTask))
      if (debug){
        println(s"Distributor: bundle of $node in mind: ${updatedStatus.minds(node).getBundle}")
        println(s"Distributor: next task of $node : $nextTask")
      }

      // Update the allocation
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.removeTask(node, completedTask))
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateCompletedTask(node,completedTask,completionDate))
      updatedStatus = updatedStatus.updateAllocation(updatedStatus.allocation.updateTaskInProgress(node, nextTask))
      if (debug) println(s"DistributorStatus: bundle of $node in allocation: ${updatedStatus.allocation.bundle(node)}")

      // Monitor the mind and the consumption
      if (isMonitored) {
        new BundleWriter(pathName + node.name + ".json", updatedStatus.allocation.getTheBundle(node), "monitoring").write()
        monitor.get ! Notify(node, updatedStatus.minds(node).getCurrentTasks, updatedStatus.minds(node).insight())
        monitor.get ! NotifyDone(node, completedTask, completionDate)
        if (debugMetrics) println(s"${updatedStatus.allocation.realGlobalMeanFlowtime}")
      }
      // Close the distributor
      self ! Close
      if (debug) println(s"Tasks in progress: ${updatedStatus.allocation.tasksInProgress}")
      goto(Terminal) using updatedStatus
  }

  /**
   * End the distributor
   */
  when(Terminal) {
    /**
     * When the consumer closes
      */
    case Event(Close, status) =>
      distributedConsumptionSystem ! Outcome(status.allocation, metrics, nbFirstStages=0, nbSecondStages=0)
      if (debug) println("Distributor ends")
      stay() using status
    /**
     * When the scheduler suggest a delegation
     */
    case Event(Suggest(_), status) =>
      stay() using status
  }

  /**
    * Whatever the state is
    **/
  whenUnhandled {
    // The other messages are not expected
    case Event(msg @ _, status) =>
      throw new RuntimeException(s"Distributor>ERROR: Distributor in state $stateName receives the message $msg from which was not expected")
      stay() using status
  }

  /**
    * Associates actions with a transition instead of with a state and even, e.g. debugging
    */
  onTransition {
    case s1 -> s2 =>
      if (debugState) println(s"Distributor>Distributor moves from the state $s1 to the state $s2")
  }

  // Finally triggering it up using initialize,
  // which performs the transition into the initial state and sets up timers (if required).
  initialize()
}
