// Copyright (C) Maxime MORGE, Luc BIGAND 2021, 2023
package org.smastaplus.strategy.deal.counterproposal.single

import org.smastaplus.consumer.deal.mas.nodeAgent.negotiator.NegotiatingMind
import org.smastaplus.core.SocialRule
import org.smastaplus.process.{Delegation, SingleSwap, Swap}
import org.smastaplus.strategy.deal.MindWithPeerModelling

/**
  * SingleSwap strategy which never suggest any SingleSwap
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
class NoneSingleCounterProposalStrategy(override val minimalImprovement: Double = 0.0)
  extends SingleCounterProposalStrategy(minimalImprovement){

  override def toString: String = "NoneCounterProposalStrategy"

  /**
    * Returns a potential swap eventually none after an offer
    */
  override def selectCounterOffer(offer: Delegation, mind: NegotiatingMind, rule: SocialRule): Option[SingleSwap] = {
    previousCounterOffer = None
    None
  }

  /**
    * Returns false since none swap is triggerable by the mind according to the rule
    */
  override def triggerable(swap: Swap, mind: MindWithPeerModelling, rule: SocialRule): Boolean = false
}
