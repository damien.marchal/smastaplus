// Copyright (C) Maxime MORGE, Luc BIGAND 2021, 2023
package org.smastaplus.strategy.deal.counterproposal

import org.smastaplus.consumer.deal.mas.nodeAgent.negotiator.NegotiatingMind
import org.smastaplus.core._
import org.smastaplus.process.{Delegation, Swap}
import org.smastaplus.strategy.deal.OfferStrategy

/**
  * Abstract swap strategy
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class CounterProposalStrategy(override val minimalImprovement: Double)
  extends OfferStrategy[Swap](minimalImprovement){

  /**
    * Returns the counter-offer which has been previously selected by selectCounterOffer,
    * eventually none
    */
  var previousCounterOffer : Option[Swap] = None

  /**
    * Returns a potential swap eventually none after an offer
    */
  def selectCounterOffer(offer: Delegation, mind: NegotiatingMind, rule: SocialRule): Option[Swap]

}