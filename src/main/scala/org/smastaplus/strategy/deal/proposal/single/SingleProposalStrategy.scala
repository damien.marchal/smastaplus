// Copyright (C) Maxime MORGE 2020, 2021, 2022, 2023
package org.smastaplus.strategy.deal.proposal.single

import org.smastaplus.core._
import org.smastaplus.process.Delegation
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.strategy.deal.proposal.ProposalStrategy
import org.smastaplus.utils.MathUtils._

import scala.annotation.switch

/**
  * Abstract strategy for proposing a single delegation
  * @param minimalImprovement is the minimal required reduction for acceptability
  */
abstract class SingleProposalStrategy(override val minimalImprovement: Double)
  extends ProposalStrategy(minimalImprovement) {

  override def toString: String = "SingleProposalStrategy"

  /**
    * Returns true if the acceptability criteria of the delegation is validated by any mind
    */
  @throws(classOf[RuntimeException])
  override def acceptabilityCriteria(delegation: Delegation, mind: MindWithPeerModelling, rule: SocialRule): Boolean = {
    val donor = delegation.donor
    val recipient = delegation.recipient
    if (delegation.endowment.size != 1) {
      throw new RuntimeException(s"SingleProposalStrategy>${mind.me} cannot handle $delegation")
    }
    if (!provisioningCompliance(delegation, mind)) {
      if (debugAcceptability) println(s"SingleProposalStrategy>${mind.me} considers the delegation $delegation as not provisioning-compliant")
      return false
    }
    if (debugAcceptability) println(s"SingleProposalStrategy>${mind.me} with ${mind.tasks} calculating Acceptability Criteria for delegation $delegation")
    val task = delegation.endowment.head
    var (before, after) = (0.0, 0.0)
    rule match {
      case Makespan => // The makespan rule
        before = mind.beliefWorkload(donor)
        after = mind.beliefWorkload(recipient) + mind.stap.cost(task, recipient)
      case LocalFlowtime => // The localFlowtime rule
        before = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          val donorBefore = mind.beliefCompletionTime(job, donor)
          val recipientBefore = mind.beliefCompletionTime(job, recipient)
          val tmp = sum + Math.max(
            donorBefore,
            recipientBefore
          )
          if (debugAcceptability)
            println(s"SingleProposalStrategy>${mind.me} with ${mind.tasks}: " +
              s"$job for $delegation -> AccSum=$sum  ${donor}Before=$donorBefore  ${recipient}Before=$recipientBefore")
          tmp
        }
        after = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          val donorAfter = mind.beliefCompletionTimeOfDonor(job, donor, recipient, task)
          val recipientAfter = mind.beliefCompletionTimeOfRecipient(job, donor, recipient, task)
          val tmp = sum + Math.max(
            donorAfter,
            recipientAfter
          )
          if (debugAcceptability)
            println(s"SingleProposalStrategy>${mind.me} with ${mind.tasks}: " +
              s"$job , AccSum=$sum  ${donor}After=$donorAfter  ${recipient}After=$recipientAfter")
          tmp
        }
        if (debugAcceptability)
          println(s"SingleProposalStrategy>${mind.me} acceptability of $delegation =" +
            s" ${after < before} since after=$after and before=$before with ${mind.orderedJobs}")
      case GlobalFlowtime => // The globalFlowtime rule
        before = mind.beliefGlobalFlowtime
        after = mind.stap.jobs.foldLeft(0.0) { (sum, job) =>
          sum + mind.stap.ds.computingNodes.map { node =>
            (node: @switch) match {
              case n if n == donor =>
                mind.beliefCompletionTimeOfDonor(job, donor, recipient, task)
              case n if n == recipient =>
                mind.beliefCompletionTimeOfRecipient(job, donor, recipient, task)
              case _ =>
                mind.beliefCompletionTime(job, node)
            }
          }.max
        }
        if (debugAcceptability)
          println(s"SingleProposalStrategy>${mind.me} acceptability of $delegation =" +
            s" ${after < before} since after=$after and before=$before")
      case rule =>
        throw new RuntimeException(s"SingleProposalStrategy>${mind.me}  is not able to manage $rule ")
    }
    if (debug) println(s"SingleProposalStrategy>${mind.me} acceptability of $delegation = " +
      s"${after < before} since after=$after and before=$before")
    minimalImprovement ~< (before - after)
  }
}