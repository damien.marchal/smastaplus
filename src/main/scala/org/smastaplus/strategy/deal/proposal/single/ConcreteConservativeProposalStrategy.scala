// Copyright (C) Maxime MORGE 2020, 2023
package org.smastaplus.strategy.deal.proposal.single

import org.smastaplus.core._
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.MathUtils._
import org.smastaplus.utils.RandomUtils

/**
  * Concrete conservative strategy for proposing a single delegation
 * @param minimalImprovement is the minimal required reduction for acceptability, 0.0 by default
  */
case class ConcreteConservativeProposalStrategy(override val minimalImprovement : Double = 0.0)
  extends AbstractConservativeProposalStrategy(minimalImprovement) {

  /**
    * Selects among the jobs for which the donor is considered as a bottleneck,
    * the highest priority job according to the donor's  mind
    */
  override def select(mind: MindWithPeerModelling, jobs: Set[Job]): Option[Job] = {
    try {
      val bottleneckJobsList = mind.bottleneckJobs().filter(jobs.contains)
      Some(bottleneckJobsList.head)
    } catch {
      case _: NoSuchElementException => None
    }
  }

  /**
    * Selects one of the peer for whom the sum of the differences between
    * the completion time for the allocation and the completion time for the peer
    * is the largest for the jobs after the job in the peer bundle according to the donor
    */
  override def select(mind: MindWithPeerModelling, nodes: Set[ComputingNode], job: Job): Option[ComputingNode] = {
    var maxDistance = Double.MinValue
    var maxNodes: Option[Set[ComputingNode]] = None
    nodes.foreach { peer =>
      val peerJobsList: List[Job] = mind.sortedJobs(peer)
      val nextPeerJobsList = peerJobsList.dropWhile(_ != job).filterNot(_ == job)
      val distance = nextPeerJobsList.foldLeft(0.0)((sum, j) =>
        sum + mind.beliefCompletionTime(j) - mind.beliefCompletionTime(j, peer)
      )
      if (distance ~= maxDistance) {
        maxNodes = Some(maxNodes.get + peer)
      }
      if (distance ~> maxDistance) {
        maxDistance = distance
        maxNodes = Some(Set(peer))
      }
    }
    if (maxNodes.isDefined) Some(RandomUtils.random(maxNodes.get))
    else None
  }

  /**
    * Selects a remote task whose delegation will reduce its cost since it will be executed.
    * Selects the task of the job or the previous jobs in the donor bundle with the maximum payoff.
    * In case of a tie, the priority task of the batch is chosen.
    */
  override def select(mind:MindWithPeerModelling, tasks: Set[Task], recipient: ComputingNode, job: Job): Option[Task] = {
    var maxPayoff = Double.MinValue
    var maxTask: Option[Task] = None
    val previousJobs: List[Job] = mind.previousJobs(job) ::: List(job)
    val previousTasks = tasks.intersect(previousJobs.foldLeft(Set[Task]())((acc, j) => acc.union(j.tasks)))
    mind.tasks.intersect(previousTasks).foreach { task =>
      val payoff = mind.stap.cost(task, mind.me) - mind.stap.cost(task, recipient)
      if (payoff ~> maxPayoff) {
        maxPayoff = payoff
        maxTask = Some(task)
      }
    }
    maxTask
  }
}
