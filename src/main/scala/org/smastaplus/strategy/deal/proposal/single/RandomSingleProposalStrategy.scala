// Copyright (C) Maxime MORGE 2020, 2022, 2023
package org.smastaplus.strategy.deal.proposal.single

import org.smastaplus.core._
import org.smastaplus.process.{Delegation, SingleDelegation}
import org.smastaplus.strategy.deal.MindWithPeerModelling
import org.smastaplus.utils.RandomUtils

import scala.annotation.unused

/**
  * Concrete conservative strategy for proposing a single delegation which
  * select a potential delegation with probability p (if exists),
  * eventually none with probability 1-p
  * the potential delegation must be triggerable (or not)
  * wrt the makespan, the localFlowtime and the globalFlowtime
 *
 * @param p is the probability to select a potential delegation if exists
  * @param triggerableMakespan is true if the improvement of the makespan is required
  * @param triggerableLocalFlowtime is true if the improvement of the local flowtime is required
  * @param triggerableGlobalFlowtime is true if the improvement of the global flowtime is required
  * @param minimalImprovement is the minimal required reduction for acceptability, 0.0 by default
  */
@unused
class RandomSingleProposalStrategy(val p: Double,
                                   val triggerableMakespan: Boolean = false,
                                   val triggerableLocalFlowtime: Boolean = false,
                                   val triggerableGlobalFlowtime: Boolean,
                                   override val minimalImprovement: Double = 0.0)
  extends SingleProposalStrategy(minimalImprovement) {

  override def toString: String = "RandomSingleProposalStrategy"

  /**
    * Returns a potential delegation
    */
  override def selectOffer(mind: MindWithPeerModelling, rule: SocialRule): Option[Delegation] = {
    if (RandomUtils.randomBoolean(p)) {
      val activePeers = mind.peers.filter( peer => mind.usage(peer).isActive)
      if (activePeers.isEmpty) return None
      val delegation = new SingleDelegation(mind.stap, mind.me,
        RandomUtils.random[ComputingNode](activePeers),
        RandomUtils.random[Task](mind.tasks))
      if (triggerableLocalFlowtime && triggerableGlobalFlowtime) {
        if (triggerable(delegation, mind, LocalFlowtime) && triggerable(delegation, mind, GlobalFlowtime)) {
          previousOffer = Some(delegation)
          return Some(delegation)
        }
        else {
          previousOffer = None
          return None
        }
      }
      if (triggerableMakespan && triggerableGlobalFlowtime) {
        if (triggerable(delegation, mind, Makespan) && triggerable(delegation, mind, GlobalFlowtime)) {
          previousOffer = Some(delegation)
          return Some(delegation)
        }
        else {
          previousOffer = None
          return None
        }
      }
      if (triggerableMakespan && triggerableLocalFlowtime) {
        if (triggerable(delegation, mind, Makespan) && triggerable(delegation, mind, LocalFlowtime)) {
          previousOffer = Some(delegation)
          return Some(delegation)
        }
        else {
          previousOffer = None
          return None
        }
      }
      if (triggerableMakespan) {
        if (triggerable(delegation, mind, Makespan)) return {
          previousOffer = Some(delegation)
          Some(delegation)
        }
        else {
          previousOffer = None
          return None
        }
      }
      if (triggerableLocalFlowtime) {
        if (triggerable(delegation, mind, LocalFlowtime)){
          previousOffer = Some(delegation)
          return Some(delegation)
        }
        else {
          previousOffer = None
          return None
        }
      }
      if (triggerableGlobalFlowtime) {
        if (triggerable(delegation, mind, GlobalFlowtime)){
          previousOffer = Some(delegation)
          return Some(delegation)
        }
        else {
          previousOffer = None
          return None
        }
      }
      previousOffer = Some(delegation)
      return Some(delegation)
    }
    previousOffer = None
    None
  }
}

