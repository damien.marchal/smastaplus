// Copyright (C) Maxime MORGE 2021
package org.smastaplus.strategy.consumption

import org.smastaplus.core._
import org.smastaplus.utils.MathUtils._

import scala.collection.immutable.ListMap
import org.joda.time.LocalDateTime

/**
  * Insight which are communicated by an agent,
  * i.e. the costs of the jobs for it
  * @param costs of the jobs
  * @param timestamp of the insight, now by default
  */
class Insight(val costs: Map[Job, Double],
              val completionTimes: Map[Job, Double] = Map[Job, Double](),
              val timestamp: LocalDateTime = LocalDateTime.now()) extends Ordered[Insight]{
  private val internalMap : Map[Job, Double] = costs

  //val timestamp: LocalDateTime = LocalDateTime.now()

  override def toString: String = costs.toString

  def removed(key: Job): Insight =  new Insight(internalMap.removed(key), completionTimes.removed(key))

  def cost(key: Job): Double = internalMap.getOrElse(key, 0.0)

  def iterator: Iterator[(Job, Double)] = internalMap.iterator

  def update(key: Job, value: Double): Insight = new Insight(internalMap.updated(key,value), completionTimes)

  def update(key: Job, value: Double, completionTime: Double): Insight = new Insight(internalMap.updated(key,value), completionTimes.updated(key, completionTime))

  def values: Iterable[Double] = internalMap.values

  override def equals(that: Any): Boolean =
    that match {
      case that: Insight => that.canEqual(this) && this.costs.equals(that.costs)
      case _ => false
    }

  def canEqual(a: Any): Boolean = a.isInstanceOf[Job]

  override def compare(that: Insight): Int = {
    if (this.values.sum ~= that.values.sum ) return 0
    else if (this.values.sum ~> that.values.sum) return 1
    -1
  }

  /**
    * Returns the ordered list of jobs according to their cost
    * with breaking ties according to their name
    **/
  def orderedJob : List[Job] = ListMap(internalMap.toSeq.sortWith( (x,y) =>
    x._2 < y._2 || ((x._2 ~= y._2) && (x._1 < y._1))
  ):_*).keys.toList

}

/**
  * Companion object
  */
object Insight {
  def apply(ts: (Job, Double)*): Insight = new Insight(ts.toMap)

  /**
    * Generate insight for a mind
    */
  def apply(mind: Mind) : Insight = {
    mind.stap.jobs.map(job => job -> mind.cost(job)).toMap.asInstanceOf[Insight]
  }

  /**
    * Generate zero insight for some jobs
    */
  def apply(jobs : Set[Job]) : Insight = jobs.map(job => job -> 0.0).toMap.asInstanceOf[Insight]
}
