// Copyright (C) Maxime MORGE 2020, 2022, 2023
package org.smastaplus.balancer

import org.smastaplus.core._
import org.smastaplus.strategy.consumption.Mind

/**
  * Balancer the allocation using the Sequential Single-Item (SSI) auction
  * @param stap instance to be solved
  * @param rule is the social rule to be applied
  * @param name of the balancer
  * See Koenig, S., Tovey, C., Lagoudakis, M., Markakis, V., Kempe, D., Keskinocak, P., ... & Jain, S. (2006, July).
  * The power of sequential single-item auctions for agent coordination. In AAAI (Vol. 2006, pp. 1625-1629).
  */
class SSIBalancer(stap: STAP,
                  rule: SocialRule,
                  name : String = "SSIBalancer")
  extends Balancer(stap, rule, name) {
  debug = false

  private var outcome: ExecutedAllocation = _
  private var unallocatedTargets: List[Task] = _

  /**
   * Initialize the outcome according an initial allocation by
   *  - keeping tasks in progress and consumed task
   *  - removing bundles
   */
  private def initOutcome(allocation: ExecutedAllocation): Unit = {
    outcome = allocation.copy()
    for (node <- stap.ds.computingNodes) {
      outcome = outcome.update(node, List.empty[Task])

    }
  }


  /**
   * Returns an allocation
   */
  @throws(classOf[RuntimeException])
  protected override def balance(): Option[ExecutedAllocation] = Some(balance(ExecutedAllocation.randomAllocation(stap)))

  /**
   * Returns an allocation with all the tasks of the allocation with the sequential single-item auction
   */
  @throws(classOf[RuntimeException])
  def balance(allocation: ExecutedAllocation): ExecutedAllocation = {
    initOutcome(allocation)
    unallocatedTargets = allocation.pendingTasks()
    // Metrics
    nbDeals = unallocatedTargets.size
    nbDelegations = nbDeals
    nbDelegatedTasks = nbDelegations
    avgEndowmentSize = 1.0
    while (unallocatedTargets.nonEmpty) { // While some targeted tasks are not allocated
      if (debug)  println(s"$name considers $unallocatedTargets")
      var minBid = Double.MaxValue
      var minTask: Option[Task] = None
      var minNode: Option[ComputingNode] = None
      for (node <- stap.ds.computingNodes) { // For each node
        if (debug)  println(s"$name considers $node")
        for (target <- unallocatedTargets) { // For each task
          if (debug)  println(s"\t $name considers $target")
          var currentAllocation = outcome.copy()
          currentAllocation = currentAllocation.update(node, target :: currentAllocation.bundle(node))
          val nodeMind = Mind(stap, node, currentAllocation)
          currentAllocation = currentAllocation.update(node, nodeMind.sortedTasks)
          if (debug)  println(s"\t $name considers $currentAllocation")
          val bid = rule match {
            case GlobalFlowtime =>
              currentAllocation.ongoingGlobalFlowtime
            case _ =>
              throw new RuntimeException(s"$name cannot apply the social rule $rule")
          }
          if (debug)  println(s"\t $name: $node bids for $target $bid")
          if (bid < minBid) {
            minBid = bid
            minTask = Some(target)
            minNode = Some(node)
          }
        }
      }
      if (debug) println(s"$name allocates $minTask at $minNode")
      outcome = outcome.update(minNode.get, minTask.get :: outcome.bundle(minNode.get))
      val minNodeMind = Mind(stap, minNode.get, outcome)
      outcome = outcome.update(minNode.get, minNodeMind.sortedTasks)
      unallocatedTargets = unallocatedTargets.filter(_ != minTask.get)
    }
    outcome
  }
}

  /**
  * Companion object to test it
  */
object SSIBalancer extends App {
  val debug = false
  import org.smastaplus.example.stap.ex1.stap
  println(stap)
  val rule : SocialRule = GlobalFlowtime
  val balancer = new SSIBalancer(stap,rule)
  val allocation = balancer.run()
  println(allocation)
  rule match {
    case GlobalFlowtime => println(allocation.get.globalFlowtime)
    case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
  }
}