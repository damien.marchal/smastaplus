// Copyright (C) Maxime MORGE, Anne-Cécile CARON, 2022, 2023
package org.smastaplus.balancer.deal.mas.nodeAgent.manager

import com.typesafe.config.{Config, ConfigFactory}
import org.smastaplus.core.{ComputingNode, STAP, Task}
import org.smastaplus.provisioning.ProvisioningUsage
import org.smastaplus.strategy.consumption._
import org.smastaplus.strategy.consumption.Mind
import org.smastaplus.utils.serialization.core.BundleWriter

import scala.annotation.unused

/**
  *  Manager's state of mind
  * @param me is the node I manage
  * @param peers are nodes managed by the other agents
  * @param usage are the the provisioning usages of the computing nodes
  * @param stap instance
  * @param taskSet tasks assigned to the agent
  * @param isFirstStage  is true if the agent is in the first stage, false otherwise
  */
final class ManagerMind(me: ComputingNode,
                        peers: Set[ComputingNode],
                        usage: Map[ComputingNode, ProvisioningUsage],
                        stap: STAP, taskSet: Set[Task],
                        val isFirstStage: Boolean)
  extends Mind(me, peers, stap, taskSet) {

  debug = false
  val debugUpdate = false

  bundle = new Bundle(stap, me).setBundle(taskSet)
  val config: Config = ConfigFactory.load()
  var pathName: String = config.getString("path.smastaplus") + "/" +
    config.getString("path.monitoringDirectory") + "/trace/"+me.name+".json"

  /**
    * Auxiliary  constructor
    */
  def this(me: ComputingNode,
           peers: Set[ComputingNode],
           usage: Map[ComputingNode, ProvisioningUsage],
           stap: STAP,
           bundle: Bundle,
           isFirstStage: Boolean) = {
    this(me, peers, usage, stap, Set.empty[Task], isFirstStage)
    this.bundle = bundle
  }

  override def toString: String = super.toString + s" and the usage is $usage and  "

  def bundleWriter(mode : String) : BundleWriter = new BundleWriter(pathName, bundle, "monitoring")


  /**
    * Returns a copy
    */
  override def copy(): ManagerMind = {
    val copyOfBundle = this.bundle.copy()
    new ManagerMind(me, peers, usage, stap, copyOfBundle,  isFirstStage)
  }

  /**
   * Get the bundle of the mind
   */
  def getBundle : Bundle = this.bundle

  /**
    * Sets and sorts the bundle of the mind
    */
  override def setTaskSet(updatedBundle: Set[Task]) : ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.setBundle(updatedBundle), isFirstStage)

  /**
   * Sets  the bundle of the mind
   */
  override def setBundle(updatedBundle: Bundle) : ManagerMind =
    new ManagerMind(me, peers, usage, stap, updatedBundle, isFirstStage)

  /**
    * Adds a task to the bundle and sorts it
    */
  override def addTask(task: Task): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.addTask(task), isFirstStage)

  /**
    * Adds an endowment to the bundle and sorts it
    */
  override def addTasks(endowment: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.addTasks(endowment), isFirstStage)

  /**
    * Removes a task to the bundle and sorts it
    */
  override def removeTask(task: Task): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.removeTask(task), isFirstStage)

  /**
    * Remove an endowment to the bundle and sorts it
    */
  override def removeTasks(endowment: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.removeTasks(endowment), isFirstStage)

  /**
    * Replaces a task by a counterpart in the bundle and sorts it
    */
  override def replaceTask(task: Task, counterpart: Task):ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.replaceTask(task, counterpart), isFirstStage)

  /**
    * Replaces an endowment by a counterpart in the bundle and sorts it
    */
  override def replaceTasks(endowment: List[Task], counterpart: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, this.bundle.replaceTasks(endowment, counterpart), isFirstStage)

  /**
    * Initializes the negotiation mind with a new bundle
    */
  def initBundle(initialBundle: List[Task]): ManagerMind =
    new ManagerMind(me, peers, usage, stap, initialBundle.toSet, isFirstStage)

  /**
    * Go to the first stage
    */
  @unused
  def firstStage : ManagerMind =
    new ManagerMind(me, peers, usage, stap, bundle, true)

  /**
    * Go th the second stage
    */
  @unused
  def secondStage : ManagerMind =
    new ManagerMind(me, peers, usage, stap, bundle, false)

}