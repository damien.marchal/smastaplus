// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON, 2020, 2021, 2022, 2023
package org.smastaplus.balancer.deal.mas

import org.smastaplus.core._
import org.smastaplus.balancer.deal._
import org.smastaplus.strategy.deal.counterproposal._
import org.smastaplus.strategy.deal.proposal._
import org.smastaplus.utils.serialization.core.AllocationWriter
import org.smastaplus.balancer.deal.mas.supervisor.Supervisor

import com.typesafe.config.{Config, ConfigFactory}
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.actor.SupervisorStrategy.Stop
import akka.pattern.ask
import akka.util.Timeout

/**
  * Decentralized agent-based balancer
  * @param stap instance
  * @param rule to adopt for reducing either the localFlowtime or the makespan
  * @param firstProposalStrategy for making proposals in the first stage
  * @param secondProposalStrategy for eventually making proposals in the second stage
  * @param counterProposalStrategy for making counter-proposals in the second stage
  * @param system of actors
  * @param name of the balancer
  * @param dispatcherId is the name of the dispatcher "akka.actor.default-dispatcher" or "single-thread-dispatcher"
  * @param monitor is true if the completion times are monitored during the solving
  */
class AgentBasedBalancer(stap: STAP,
                         rule: SocialRule,
                         firstProposalStrategy: ProposalStrategy,
                         secondProposalStrategy: Option[ProposalStrategy],
                         counterProposalStrategy: CounterProposalStrategy,
                         val system: ActorSystem,
                         name: String = "DecentralizedBalancer",
                         val dispatcherId: String,
                         val monitor: Boolean = false)
  extends DealBalancer(stap, rule, firstProposalStrategy, secondProposalStrategy, counterProposalStrategy, name){

  val TIMEOUT_VALUE : FiniteDuration = 6000 minutes // Default timeout of a run
  implicit val timeout : Timeout = Timeout(TIMEOUT_VALUE)

  // Launch a new supervisor
  AgentBasedBalancer.id += 1
  val supervisorName: String = "supervisor"+AgentBasedBalancer.id
  val supervisor : ActorRef = system.actorOf(Props(classOf[Supervisor], stap, rule,
    firstProposalStrategy, secondProposalStrategy, counterProposalStrategy, dispatcherId, monitor)
    withDispatcher dispatcherId,
      name = supervisorName)

  /**
    * Initiate the balancer with an allocation, eventually a random one
    */
  override def init(allocation: Allocation = Allocation.randomAllocation(stap)): Allocation = {
    if (debug) println(s"DecentralizedBalancer: initial (eventually random) allocation\n$allocation")
    allocation
  }

  /**
    * Modify the current allocation
    */
  def reallocate(allocation: Allocation) : Allocation = {
    initialLocalityRate = allocation.localAvailabilityRatio
    if (trace) {
      supervisor ! Trace
    }
    if (monitor) {
      val config: Config = ConfigFactory.load()
      val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
        config.getString("path.monitoringDirectory")
      val writer = new AllocationWriter(monitoringDirectoryName + "/allocation.json", allocation, "monitoring")
      writer.write()
    }
    init(allocation)
    if (debug) println("DecentralizedBalancer: reallocate")
    val future = supervisor ? Init(allocation)
    val result = Await.result(future, timeout.duration).asInstanceOf[Outcome]
    supervisor ! Stop
    if (debug) println(s"DecentralizedBalancer: reallocate ends")
    if (debug) println(s"DecentralizedBalancer: metrics ${result.metrics}")
    nbDeals= result.metrics.sumOfDelegationConfirmations + result.metrics.sumOfSwapConfirmations
    nbDelegations = result.metrics.sumOfDelegationConfirmations
    nbSwaps= result.metrics.sumOfSwapConfirmations
    if (debug) println(s"Negotiation successful rate = $nbDeals")
    nbTimeouts =  result.metrics.sumOfTimeouts
    if (debug) println(s"Nb timeout = ${result.metrics.sumOfTimeouts}")
    if (debug) println(s"Nb proposals = ${result.metrics.sumOfProposals}")
    if (debug) println(s"Nb delegated tasks = ${result.metrics.sumOfDelegatedTasks}")
    if (debug) println(s"Nb delegation confirmations = ${result.metrics.sumOfDelegationConfirmations}")
    if (debug) println(s"Nb swap confirmations = ${result.metrics.sumOfSwapConfirmations}")
    if (debug) println(s"Nb delegated tasks = ${result.metrics.sumOfDelegatedTasks}")

    avgEndowmentSize =  result.metrics.sumOfDelegatedTasks.toDouble / (result.metrics.sumOfDelegationConfirmations + result.metrics.sumOfSwapConfirmations).toDouble
    if (avgEndowmentSize.isNaN) avgEndowmentSize = 0.0
    if (debug) println(s"Endowment size = $avgEndowmentSize")
    localRatio = result.allocation.localAvailabilityRatio
    result.allocation
  }

  /**
    * Closes the balancer
    */
  override def close() : Unit = {
      system.terminate()
  }
}

/**
 * Companion object for testing DecentralizedBalancer
 */
object AgentBasedBalancer extends App {
  var id: Int = 1
}
