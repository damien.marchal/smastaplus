// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.balancer.deal.mas

import org.smastaplus.core._

import akka.actor.ActorRef
import scala.annotation.unused

/**
  *Directory is an index of the names and addresses of peers
  */
class Directory {
  var addressOf: Map[ComputingNode, ActorRef] = Map[ComputingNode, ActorRef]()//Agents' references
  var nodes: Map[ActorRef, ComputingNode] = Map[ActorRef, ComputingNode]()// Actors' node

  override def toString: String = allAgents().mkString("[",", ","]")

  /**
   * Returns the computing node corresponding to the actorref
   */
  @throws[RuntimeException]
  def nodeOf(actor : ActorRef): ComputingNode = {
    //Remove the name prefix of the agent
    val nodeName = actor.path.name.replace("manager@","").replace("negotiator@","")
    addressOf.keys.find(node => node.name == nodeName) match {
      case None =>
        throw new RuntimeException(s"ERROR Directory does not contains $actor with name ${actor.path.name}")
      case Some(node) =>
        node
    }
  }

  /**
    * Add to the directory
    * @param node corresponding to the agent
    * @param ref ActorRef for the agent
    */
  def add(node: ComputingNode, ref: ActorRef) : Unit = {
    if ( ! addressOf.keySet.contains(node) &&  ! nodes.keySet.contains(ref)) {
      addressOf += (node -> ref)
      nodes += (ref -> node)
    }
    else throw new RuntimeException(s"$node and/or $ref already in the directory")
  }

  def allActors() : Iterable[ActorRef]  = addressOf.values
  def allAgents() : Iterable[ComputingNode]  = nodes.values
  def peers(node : ComputingNode) : Set[ComputingNode] = allAgents().filterNot(_ == node).toSet
  @unused
  def peersActor(node : ComputingNode) :  Iterable[ActorRef] = peers(node: ComputingNode).map(w => addressOf(w))
}
