// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2020, 2021, 2022
package org.smastaplus.balancer.deal.mas.supervisor

import org.smastaplus.balancer.deal.mas.State

/**
  * Trait representing as state in a FSM
  * for the supervisor behaviour
  */
trait SupervisorState extends State
case object InitialState extends SupervisorState
case object RunningFirstStage extends SupervisorState
case object RunningSecondStage extends SupervisorState
case object EndFirstStage extends SupervisorState
case object EndSecondStage extends SupervisorState
case object FirstToSecond extends SupervisorState
case object SecondToFirst extends SupervisorState
case object TransitionalEnd extends SupervisorState
case object Terminal extends SupervisorState