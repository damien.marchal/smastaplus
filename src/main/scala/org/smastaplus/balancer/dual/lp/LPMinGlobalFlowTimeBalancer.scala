// Copyright (C) Maxime MORGE 2020
package org.smastaplus.balancer.dual.lp

import org.smastaplus.core._
import org.smastaplus.balancer.dual.DualBalancer
import org.smastaplus.utils.lp.TransportationWriter

import com.typesafe.config.{Config, ConfigFactory}
import java.io.File
import scala.sys.process._

/**
  * Algorithm from "Scheduling independent tasks to reduce mean finishing time" by
  * Bruno, James and Coffman Jr, Edward G and Sethi, Ravi using linear programming
  * @param pb   to be balancer
  * @param rule to be optimized
  * @param name of the balancer
  */
class LPMinGlobalFlowTimeBalancer(pb: STAP, rule: SocialRule, name : String = "LPMinGlobalFlowTimeBalancer")
  extends DualBalancer(pb, rule, name) {
  debug = false

  val config: Config = ConfigFactory.load()
  val inputPath: String =config.getString("path.smastaplus")+"/"+config.getString("path.input")
  val outputPath: String = config.getString("path.smastaplus")+"/"+config.getString("path.output")
  var lpPath: String = rule match {
    case GlobalFlowtime =>
      config.getString("path.smastaplus")+"/"+config.getString("path.bruno")
    case _ =>
      throw new RuntimeException(s"$name cannot tackle the social rule $rule")
  }

  /**
    * Balancer main method
    * @return the allocation
    */
  protected override def balance(): Option[ExecutedAllocation] = {
    // 1 -- Reformulate the problem
    var startingTime: Long = System.nanoTime()
    val writer=new TransportationWriter(inputPath, pb)
    writer.write()
    preSolvingTime= System.nanoTime() - startingTime

    //2 -- Solve the flow problem
    if (debug) println("Solve the flow problem")
    val command : String= config.getString("path.opl")+" "+
      lpPath+" "+
      inputPath
    if (debug) println(command)
    val success : Int = (command #> new File("/dev/null")).!
    if (success != 0) throw new RuntimeException(s"$name failed")

    //3 -- Translate the outcome into an allocation
    if (debug) println("Translate flow into an allocation")
    startingTime = System.nanoTime()
    val allocation : ExecutedAllocation = ExecutedAllocation(outputPath, pb)
    postSolvingTime = System.nanoTime() - startingTime
    if (debug) println("A\n"+allocation)
    Some(allocation)
  }
}

/**
* Companion object to test it
*/
object LPMinGlobalFlowTimeBalancer extends App {
  val debug = false

  import org.smastaplus.example.stap.ex1.stap

  if (debug) println(stap)
  val lpBalancer = new LPMinGlobalFlowTimeBalancer(stap, GlobalFlowtime)
  lpBalancer.debug = false
  val outcome = lpBalancer.run()
  println(outcome.toString)
  println(s"GlobalFlowtime ${outcome.get.meanGlobalFlowtime}")
}
