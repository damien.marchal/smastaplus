// Copyright (C) Maxime MORGE 2020
package org.smastaplus.balancer.dual.lp

import org.smastaplus.core._
import org.smastaplus.balancer.dual.DualBalancer
import org.smastaplus.utils._

/**
  * Algorithm from "Scheduling independent tasks to reduce mean finishing time" by
  * Bruno, James and Coffman Jr, Edward G and Sethi, Ravi using Ford-Ferguson algorithm
  * @param pb   to be balancer
  * @param rule to be optimized
  * @param name of the balancer
  */
class MinGlobalFlowTimeBalancer(pb: STAP,
                                rule: SocialRule,
                                name : String = "MinGlobalFlowTimeBalancer")
  extends DualBalancer(pb, rule, name) {
  debug = false

  if (rule != GlobalFlowtime) throw new RuntimeException(s"$name cannot tackle the social rule $rule")

  // 1 -- Reformulate the problem
  var startingTime: Long = System.nanoTime()
  var allocation = new ExecutedAllocation(pb)
  val N : Int = pb.n*pb.m+pb.n+2 // The implicit number of vertex in the network problem
  val source: Int = 0
  val sink: Int = 1
  var found : Array[Boolean] = new Array[Boolean](N)
  var flow : Array[Array[Int]] = Array.ofDim[Int](N, N)
  var dist = new Array[Double](N + 1)
  var dad = new Array[Int](N)
  var pi = new Array[Double](N)
  var totalFlow = 0
  var totalCost = 0.0

  /** The matrix
    *      | cost(i)(j)  |
    *  Q = | 2cost(i)(j) |
    *      |    ...      |
    *      | ncost(i)(j) |
    *      */
  def q(i: Integer, j: Integer): Double = {
    val k: Integer = i / pb.m +1
    val i2: Integer = i % pb.m
    val task: Task = pb.tasks.toVector(j)
    val node = pb.ds.computingNodes.toVector(i2)
    k * pb.cost(task, node)
  }

  /** The capacity of the edge between the nodeAgents
    * @param i the source node
    * @param j the target node
    */
  def capacity(i: Integer, j: Integer) : Integer = {
    // link the source to the factories and the warehouses to the sink
    if ( (i==0 && 2<=j && j < pb.m*pb.n+2) || (j==1 &&  pb.m*pb.n+2 <=i)) return  1
    // link the factories to the warehouses
    val i2= i-2
    val j2 = j-(pb.m*pb.n+2)
    if (0<=i2 && i2< pb.n*pb.m && 0 <= j2) return 1
    0
  }

  /** The cost of the edge between the nodeAgents
    * @param i the source node
    * @param j the target node
    */
  def cost(i: Integer, j: Integer): Double = {
    val i2= i-2
    val j2 = j-(pb.m*pb.n+2)
    if (0<=i2 && i2< pb.n*pb.m && 0 <= j2) return q(i2,j2)
    0.0
  }

  /**
    * Balancer main method
    * @return the allocation
    */
  protected override def balance(): Option[ExecutedAllocation] = {
    preSolvingTime = System.nanoTime() - startingTime

    if (debug){
      println("Q\n"+Matrix.show(q,pb.n*pb.m,pb.n))
      println("Cost\n"+Matrix.show(cost,N,N))
      println("Capacity\n"+Matrix.show(capacity,N,N))
    }

    //2 -- Solve the flow problem
    if (debug) println("Solve the flow problem")
    maxFlow()

    //3 -- Translate flow into an allocation
    if (debug) println("Translate flow into an allocation")
    startingTime = System.nanoTime()
    if (debug) {
      println("Flow\n" + Matrix.show(flow))
    }
    translateFlow()
    postSolvingTime = System.nanoTime() - startingTime
    if (debug) println("A\n"+allocation)
    Some(allocation)
  }

  /**
    * Returns true if there is a path from source 's' to sink 't' in
    * residual graph. Also fills ?[] to store the path
    */
  def search(source: Int, sink: Int): Boolean = {
    var src = source
    found = Array.fill(N)(false)
    dist = Array.fill(N+1)(Integer.MAX_VALUE)
    dist(src) = 0
    while (src != N) {
      var best = N
      found(src) = true
      for (k <- 0 until N) {
        //if (found[k]) continue
        if (!found(k)) {
          if (flow(k)(src) != 0) {
            val currentValue = dist(src) + pi(src) - pi(k) - cost(k,src)
            if (dist(k) > currentValue){
              dist(k) = currentValue
              dad(k) = src
            }
          }
          if (flow(src)(k) < capacity(src,k)){
            val currentValue = dist(src) + pi(src) - pi(k) + cost(src,k)
            if (dist(k) > currentValue){
              dist(k) = currentValue
              dad(k) = src
            }
          }
          if (dist(k) < dist(best)) best = k
        }
      }
      src = best
    }
    for (k <- 0 until N) {
      pi(k) = Math.min(pi(k) + dist(k), Double.MaxValue)
    }
    found(sink)
  }

  /**
    * Execute the minimal-cost maximal flow algorithm and returns
    * @return totalCost, totalFlow
    */
  def maxFlow() : (Int, Double) = {
    while (search(source,sink)){
      var residualCapacity =Integer.MAX_VALUE
      var x = sink
      while(x != source){
        residualCapacity = Math.min(residualCapacity, if (flow(x)(dad(x)) != 0) flow(x)(dad(x))
        else capacity(dad(x),x) - flow(dad(x))(x))
        x = dad(x)
      }
      x = sink
      while (x != source) {
        if (flow(x)(dad(x)) != 0) {
          flow(x)(dad(x)) -= residualCapacity
          totalCost -= residualCapacity * cost(x,dad(x))
        } else {
          flow(dad(x))(x) += residualCapacity
          totalCost += residualCapacity * cost(dad(x),x)
        }
        x = dad(x)
      }
      totalFlow += residualCapacity
    }
    (totalFlow, totalCost)
  }

  /**
    * Translate the flow in an allocation
    */
  def translateFlow(): Unit ={
    val (iStart,jStart) = (2, pb.m*pb.n+2)
    for (j<- 0 until pb.n) {
      for (i <- 0 until pb.m * pb.n) {
        if (debug) println(s"$i, $j")
        if (flow(i+iStart)(j+jStart) == 1) {
          val taskNumber = j
          val nodeNumber = i%pb.m
          val position = i/pb.m
          if (debug) println(s"$i, $j (found): $taskNumber, $nodeNumber, $position")
          val task: Task = pb.tasks.toVector(taskNumber)
          val node = pb.ds.computingNodes.toVector(nodeNumber)
          allocation.bundle += (node -> MyList.insert(allocation.bundle(node), position, task) )
        }
      }
    }
    allocation.bundle = allocation.bundle.transform((_,b) => b.reverse)
  }

}

