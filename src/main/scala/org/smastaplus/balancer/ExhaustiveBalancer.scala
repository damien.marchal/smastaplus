// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.balancer

import org.smastaplus.core._
import org.smastaplus.utils.MathUtils._

/**
  * Balancer testing all the allocation
  * @param stap instance to be solved
  * @param rule is the social rule to be applied
  * @param name of the balancer
  */
class ExhaustiveBalancer(stap: STAP, rule: SocialRule, name : String = "ExhaustiveBalancer")
  extends Balancer(stap, rule, name) {
  debug = false

  /**
    * Returns an allocation
    */
  @throws(classOf[RuntimeException])
  protected override def balance(): Option[ExecutedAllocation] = Some(
    stap.allAllocations().map(allocation => (allocation,  rule match {
      case Makespan => allocation.makespan
      case GlobalFlowtime => allocation.globalFlowtime
      case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
    })).reduceLeft((c1, c2) => if(c2._2 ~< c1._2) c2 else c1)._1)
}

/**
  * Companion object to test it
  */
object ExhaustiveBalancer extends App {
  val debug = false
  import org.smastaplus.example.stap.ex1.stap
  println(stap)
  val rule : SocialRule = GlobalFlowtime
  val balancer = new ExhaustiveBalancer(stap,rule)
  val allocation = balancer.run()
  println(allocation)
  rule match {
    case GlobalFlowtime => println(allocation.get.globalFlowtime)
    case Makespan => println(allocation.get.makespan)
    case rule => throw new RuntimeException(s"ERROR: Rule $rule does not match")
  }
}