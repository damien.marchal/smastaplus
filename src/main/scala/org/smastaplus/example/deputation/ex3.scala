// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.example.deputation

import org.smastaplus.core.ExecutedAllocation
import org.smastaplus.deputation.Deputation
import org.smastaplus.example.stap.ex2._

/**
  * Deputation example of the second stap
  *  where the task #1 is at the end of the bundle of the node #2
  */
object ex3 {

  val a = new ExecutedAllocation(stap)
  a.bundle += ( cn1 -> List(t4, t7) )
  a.bundle += ( cn2 -> List(t8, t5, t2, t1) )
  a.bundle += ( cn3 -> List(t9, t3, t6) )

  val deputation : Deputation = Deputation.stap2deputation(a)
}
