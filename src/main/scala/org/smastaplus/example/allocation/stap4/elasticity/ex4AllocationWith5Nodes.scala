package org.smastaplus.example.allocation.stap4.elasticity

import org.smastaplus.core.ExecutedAllocation

/**
  * Allocation example for elasticity with 5 active nodes
  */
object ex4AllocationWith5Nodes {

  import org.smastaplus.example.stap.ex4Elasticity._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t2, t8))
  a.bundle += (cn2 -> List(t7, t4))
  a.bundle += (cn3 -> List(t3, t5, t9))
  a.bundle += (cn4 -> List(t1))
  a.bundle += (cn5 -> List())
  a.bundle += (cn6 -> List(t6))


}
