// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2020, 2022
package org.smastaplus.example.allocation.stap4.jobRelease

import org.smastaplus.core.ExecutedAllocation

/**
  * Final allocation in the job release example
  * where all the tasks of the fourth job  are delegated to be executed
  */
object ex4Final {

  import org.smastaplus.example.stap.ex4with4Jobs._

  val a = new ExecutedAllocation(stap)
  a.bundle += (cn1 -> List(t3, t8, t9, t12))
  a.bundle += (cn2 -> List(t7, t2, t5, t4, t10))
  a.bundle += (cn3 -> List(t1, t11, t6))

}
