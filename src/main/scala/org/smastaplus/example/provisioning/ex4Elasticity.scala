// Copyright (C) Maxime MORGE, Ellie BEAUPREZ 2022
package org.smastaplus.example.provisioning

import org.smastaplus.provisioning.{ProvisioningProblem, QoSFlowtime}

/**
  *  Example for Elasticity paper
  */
object ex4Elasticity{
  import org.smastaplus.example.stap.ex4Elasticity._
  val problem: ProvisioningProblem = ProvisioningProblem(stap, maxThreshold = 16.5, QoSFlowtime )
}
