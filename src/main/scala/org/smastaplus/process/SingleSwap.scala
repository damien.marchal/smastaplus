// Copyright (C) Maxime MORGE, Luc BIGAND 2021
package org.smastaplus.process

import org.smastaplus.core._

/**
  * Single task swap
  * @param stap instance
  * @param initiator computing node which gives the task
  * @param responder computing node which takes the task
  * @param task which is delegated
  * @param counterpart which is the counterpart
  */
class SingleSwap(stap: STAP,
                 initiator: ComputingNode,
                 responder: ComputingNode,
                 val task: Task,
                 val counterpart: Task)
  extends Swap(stap, initiator, responder, List(task), List(counterpart)) {

  override def toString: String = s"σ($initiator,$responder,$task,$counterpart)"

  override def equals(that: Any): Boolean =
    that match {
      case that: SingleSwap => that.canEqual(this) &&
        this.initiator == that.initiator &&
        this.responder == that.responder &&
        this.task == that.task &&
        this.counterpart == that.counterpart
      case _ => false
    }

  override def canEqual(a: Any): Boolean = a.isInstanceOf[SingleSwap]

}