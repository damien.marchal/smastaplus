// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.process

import org.smastaplus.core.ComputingNode
import org.smastaplus.provisioning.{Configuration, ProvisioningProblem}

/**
  *  The deactivation of a computing node modifies
  *  the set of active computing nodes and so the time-extended allocation.
  *  A node must be associated with an empty bundle in order to be deactivated.
  *  @param problem is the provisioning problem
  */
class Deactivation(val problem: ProvisioningProblem) extends Operation {

  /**
    * Returns the configuration where the node has been deactivated
    */
  @throws(classOf[RuntimeException])
  def execute(configuration: Configuration, node: ComputingNode): Configuration = {
    if (!configuration.nodes.contains(node))
      throw new RuntimeException(s"Deactivation>$node is not active in $configuration")
    if (configuration.allocation.bundle(node).nonEmpty) {
      throw new RuntimeException(s"Deactivation>$node has not an empty bundle in $configuration")
    }
    new Configuration(problem, configuration.nodes.filter(_ != node), configuration.allocation)
  }
}
