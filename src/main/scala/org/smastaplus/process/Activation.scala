// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.process

import org.smastaplus.core.ComputingNode
import org.smastaplus.provisioning.{Configuration, ProvisioningProblem}

import scala.collection.SortedSet

/**
  *  The activation of a computing node modifies
  *  the set of active computing nodes and so,
  *  the time-extended allocation is also updated since
  *  the recently activated node is associated with an empty bundle
  *  @param problem is the provisioning problem
  */
class Activation(val problem: ProvisioningProblem) extends Operation {

  /**
    * Returns the configuration where the node has been activated
    */
  @throws(classOf[RuntimeException])
  def execute(configuration: Configuration, node: ComputingNode): Configuration = {
    if (configuration.nodes.contains(node))
      throw new RuntimeException(s"Activation>$node is active in $configuration")
    if (configuration.allocation.bundle(node).nonEmpty) {
      throw new RuntimeException(s"Activation>$node has already a no-empty bundle in $configuration")
    }
    new Configuration(problem, configuration.nodes.union(SortedSet(node))  , configuration.allocation)
  }
}
