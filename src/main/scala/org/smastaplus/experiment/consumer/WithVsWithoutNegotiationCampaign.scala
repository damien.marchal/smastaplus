// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2023
package org.smastaplus.experiment.consumer

import org.smastaplus.core.SimulatedCostE

/**
 * Object running a campaign which compares consumption process, eventually  with (re)allocation
 */
object WithVsWithoutNegotiationCampaign extends App {
  val fileName = "consumption4jobs8nodes5staps5allocations040-320tasks"
  val balancerTypes : List[String] = List("DecentralizedConsumerMultiDelegationGlobalFlowtime", "PSI", "SSI")
  private val campaign = new ConsumptionCampaign(simulatedCost = new SimulatedCostE, byNodes = false)
  campaign.run(balancerTypes, fileName)
}
