// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2020, 2022, 2023
package org.smastaplus.experiment.consumer

import org.smastaplus.core.{ExecutedAllocation, RandomGenerationRule}
import org.smastaplus.consumer.SynchronousConsumer
import org.smastaplus.experiment.{Configuration, Experiment, ExperimentOutcome, Setting}

/**
 * A configuration for a set of experiments
 */
class ConsumptionConfiguration(val m: Int,
                               val l : Int,
                               val n : Int,
                               val randomGenerationRule : RandomGenerationRule) extends Configuration{
  override def description : String = "m,l,n,randomGenerationRule"
  override def value: String = s"$m,$l,$n,$randomGenerationRule"
}

/**
 * Class representing the setting of a consumption experiment
 */
class ConsumptionSetting(val allocation : ExecutedAllocation, val consumer: SynchronousConsumer) extends Setting{
  def localityRate: Double = allocation.localAvailabilityRatio
  def globalFlowtime: Double = allocation.meanGlobalFlowtime
  def simulatedFlowtime: Double = allocation.simulatedMeanGlobalFlowtime
}

/**
 *  Class representing the outcome of an experiment for a deal-based consumer
 */
class DealConsumptionOutcome(metrics: Map[String,Double] = Map(
  "00MeanRealFlowtime" -> 0.0,
  "01MeanGlobalFlowtime"-> 0.0,
  "02SolvingTime" -> 0.0,
  "03LocalRatio" -> 0.0,
  "04NbDeals" -> 0.0,
  "05NbDelegations" -> 0.0,
  "06NbSwaps" -> 0.0,
  "07NbTimeouts"  -> 0.0,
  "08EndowmentSize" -> 0.0,
  "09NbDelegatedTasks" -> 0.0,
  "10NbFirstStages" -> 0.0,
  "11NbSecondStages" -> 0.0,
  "12MeanSimulatedFlowtime"-> 0.0)) extends ConsumptionOutcome(metrics)

/**
 * Class representing the outcome of an experiment for a standard consumer
 */
class ConsumptionOutcome(metrics: Map[String, Double]
                         = Map())  extends ExperimentOutcome(metrics)


/**
 * Class representing an experiment for consumption
 */
class ConsumptionExperiment(setting: ConsumptionSetting) extends Experiment[ConsumptionOutcome](setting) {
  val debug = false
  /**
   * Runs the experiment
   */
  def run(): DealConsumptionOutcome = {
    var result: ExecutedAllocation = setting.allocation
    var outcome = new DealConsumptionOutcome()
    val startingTime = System.nanoTime()
    result  = setting.consumer.consume(setting.allocation)
    val solvingTime = System.nanoTime() - startingTime
    if (debug) println(result.simulatedCost)
    outcome = new DealConsumptionOutcome(
      Map(
        "00MeanRealFlowtime" -> result.realGlobalMeanFlowtime,
        "01MeanGlobalFlowtime" -> result.consumptions2allocation.meanGlobalFlowtime,
        "02SolvingTime" -> solvingTime.toDouble,
        "03LocalRatio" -> result.consumptions2allocation.localAvailabilityRatio,
        "04NbDeals" -> setting.consumer.nbDeals,
        "05NbDelegations" -> setting.consumer.nbDelegations,
        "06NbSwaps" -> setting.consumer.nbSwaps,
        "07NbTimeouts" -> setting.consumer.nbTimeouts,
        "08EndowmentSize" -> setting.consumer.avgEndowmentSize,
        "09NbDelegatedTasks" -> setting.consumer.nbDelegatedTasks,
        "10NbFirstStages" -> setting.consumer.nbFirstStages,
        "11NbSecondStages" -> setting.consumer.nbSecondStages,
        "12MeanSimulatedFlowtime" -> result.consumptions2allocation.exPostMeanGlobalFlowtime//.realGlobalMeanFlowtime//result.simulatedMeanGlobalFlowtime
      ))
    if (result.isSound) outcome
    else throw new RuntimeException(s"Consumer: the outcome\n $result\nis not sound for\n ${setting.allocation.stap}")
  }
}
