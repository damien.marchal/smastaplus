// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, 2023
package org.smastaplus.experiment.consumer

import org.smastaplus.core.SimulatedCostH

/**
 * Object running a campaign which compares
 * consumption with and without negotiation
 * with half of the nodes slowing down
 */
object WithVsWithoutNegotiationSlowDownHalfNodeCampaign extends App {
  val fileName = "consumption4jobs8nodes5staps5allocationsSlowDownHalfNode040-320tasks"
  val balancerTypes : List[String] = List("DecentralizedConsumerMultiDelegationGlobalFlowtime", "PSI", "SSI")
  private val campaign = new ConsumptionCampaign(simulatedCost = new SimulatedCostH, byNodes = false)
  campaign.run(balancerTypes, fileName)
}
