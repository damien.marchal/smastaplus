// Copyright (C) Maxime MORGE 2022
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * local flowtime and makespan vs. global flowtime vs hill climbing
 */
object LocalVsGlobalCampaign extends App {
  val fileName = "localFlowtimeVsGlobalFlowtime.csv"
  val balancerTypes : List[String] = List(
    "DecentralizedBalancerDelegation",
    "DecentralizedBalancerDelegationGlobalFlowtime",
    "HillClimbingBalancerDelegationFlowtime"
  )
  val maxNodes4HillClimbing = 5
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4HillClimbing, fileName)
}