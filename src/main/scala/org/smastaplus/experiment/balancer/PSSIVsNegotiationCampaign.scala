// Copyright (C) Maxime MORGE 2023
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * local flowtime and makespan vs. global flowtime vs hill climbing
 */
object PSSIVsNegotiationCampaign extends App {
  val fileName = "pssiVsNegotiation.csv"
  val balancerTypes : List[String] = List(
    "DecentralizedBalancerDelegationGlobalFlowtime",
    "SSIGlobalFlowtime",
    "PSIGlobalFlowtime",
  )
  val maxNodes4Slow = 16
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4Slow, fileName)
}