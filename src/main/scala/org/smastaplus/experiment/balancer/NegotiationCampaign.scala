// Copyright (C) Maxime MORGE 2023
package org.smastaplus.experiment.balancer

/**
 * Object running a campaign which compares
 * local flowtime and makespan vs. global flowtime vs hill climbing
 */
object NegotiationCampaign extends App {
  val fileName = "negotiation.csv"
  val balancerTypes : List[String] = List(
    "DecentralizedBalancerDelegationGlobalFlowtime",
    "SSIGlobalFlowtime",
    "PSIGlobalFlowtime",
    "HillClimbingBalancerDelegationFlowtime"
  )
  val maxNodes4Slow = 8
  val campaign = new CampaignBalancer
  campaign.run(balancerTypes, maxNodes4Slow, fileName)
}