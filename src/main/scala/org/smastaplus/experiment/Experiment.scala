// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.experiment

/**
  * A configuration for a set of experiments
  */
abstract class Configuration {
  override def toString: String = value
  def description: String
  def value: String
}

/**
  * Class representing the setting of an experiment
  */
abstract class Setting

/**
  *  Class representing the outcome of an experiment,
  *  i.e. some metrics and thei values
  */
abstract class ExperimentOutcome(val metrics : Map[String,Any]){
  override def toString: String = metrics.values.mkString("",",","")
}

/**
  * Abstract class representing an experiment with its setting
  */
abstract class Experiment[T](setting: Setting) {
  /**
    * Runs and returns the outcome of an experiment
    */
  def run() : T

}
