// Copyright (C) Maxime MORGE 2020
package org.smastaplus.core

/**
  * Class representing a resource
  * @param name of the resource
  */
class Resource(val name : String, var size : Double) extends Ordered[Resource]{

  override def toString: String = name

  /**
    * Returns a full description of the resource
    */
  def describe: String = s"$name ($size)"

  override def equals(that: Any): Boolean =
    that match {
      case that: Resource => that.canEqual(this) && this.name == that.name
      case _ => false
    }
  def canEqual(a: Any): Boolean = a.isInstanceOf[Resource]

  /**
    * Returns 0 if this and that are the same, negative if this < that, and positive otherwise
    * Resources are sorted with their name
    */
  def compare(that: Resource) : Int = {
    if (this.name == that.name) return 0
    else if (this.name > that.name) return 1
    -1
  }
}

