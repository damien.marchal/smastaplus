// Copyright (C) Maxime MORGE 2022
package org.smastaplus.core

/**
  * Class representing resource node
  * @param name of the resource node
  */
final class ResourceNode(val name : String) extends Ordered[ResourceNode]{

  override def toString: String = name

  override def equals(that: Any): Boolean =
    that match {
      case that: ResourceNode => that.canEqual(this) && this.name == that.name
      case _ => false
    }
  def canEqual(a: Any) : Boolean = a.isInstanceOf[ResourceNode]

  /**
    * Returns 0 if this an that are the same, negative if this < that, and positive otherwise
    * Resource nodes are sorted with their name
    */
  def compare(that: ResourceNode) : Int = {
    if (this.name == that.name) return 0
    else if (this.name > that.name) return 1
    -1
  }
}
