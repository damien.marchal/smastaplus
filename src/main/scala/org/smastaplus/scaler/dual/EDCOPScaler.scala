// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler.dual

import org.smastaplus.core.{ComputingNode, STAP, Uncorrelated}
import org.smastaplus.provisioning._
import org.smastaplus.utils.serialization.provisioning.edcop.{ProvisionEDCOPParser, ProvisionEDCOPWriter}
import org.smastaplus.utils.RandomUtils.pick

import java.io.{File, IOException}
import com.typesafe.config.{Config, ConfigFactory}

import scala.annotation.unused
import scala.collection.SortedSet
import scala.concurrent.{Await, Future, TimeoutException, blocking}
import scala.sys.process._
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * An extended DCOP scaler based on PyDCOP using experiments/edcop/scaler/edcop.py
  * It is worth noticing that the hard constraints are
  * implemented as soft constraints with high cost
  * @param problem instance to be solved
  * @param name of the scaler
  * @param timeout of the scaler, 15 seconds by default
  */
class EDCOPScaler(problem: ProvisioningProblem, name: String = "EDCOPScaler",
                  val timeout: FiniteDuration = 15 second)
  extends DualScaler(problem, name) {
  debug = false

  var isFound = false// is an allocation found
  @unused val overhead = 0.1  // potential timeout overhead

  val config: Config = ConfigFactory.load()
  val inputPath: String = config.getString("path.smastaplus") + "/" + config.getString("path.edcopScalerInput")
  val outputPath: String = config.getString("path.smastaplus") + "/" + config.getString("path.edcopScalerOutput")
  val errorPath: String = config.getString("path.smastaplus") + "/" + config.getString("path.edcopScalerError")

  private var currentConfiguration = Configuration.randomConfiguration(problem)
  var initializationTime: Long = 0
  var initialLocalRatio = 0.0

  /**
    * Reformulates STAP as a Extended DCOP
    */
  def reformulate(): Unit = {
    new ProvisionEDCOPWriter(inputPath, problem, currentConfiguration).write()
  }

  /**
    * Translates the assignment as an allocation
    */
  def translate(): Unit = {
    currentConfiguration = new ProvisionEDCOPParser(outputPath, problem).parse()
  }

  /**
    * Run the balancer
    */
  def execute(): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val remainingTime: FiniteDuration = timeout - new FiniteDuration(preScalingTime, NANOSECONDS)
    //val command: String = s"pydcop --output $outputPath --timeout ${remainingTime.toSeconds} solve --algo mgm2 $inputPath"
    if (debug) println(s"pydcop --output $outputPath --timeout ${remainingTime.toSeconds} solve --algo mgm2 $inputPath")
    val command =Seq("sh", "-c", s"pydcop --output $outputPath --timeout ${remainingTime.toSeconds} solve --algo mgm2 $inputPath > /dev/null;" +
      """ sed -i'' -e "s/\"cost\": \([0-9]*\),/\"cost\": \1.0,/g" """+ outputPath)
    // Please note the cost is converted to a double not to raise a NumberFormat exception
    if (debug) println(s"$name runs '$command'")

    //val success = (command #> new File(errorPath)).! Rather than synchronous run
    val process = command.run(connectInput = false) // the command is asynchronously run
    // See https://github.com/sbt/sbt/pull/3970/commits/b0f52510e0c31bb324439903563ec202e2b849f2
    val future = Future(blocking(process.exitValue())) // adn wrap in a future
    val success = try {
      Await.result(future, 2*remainingTime)//
    } catch {
      case _ : TimeoutException =>
        if (debug) println(s"$name observes that pyDCOP outreaches timeout")
        process.destroy()
        if (debug) println(s"pyDCOP destroyed")
        try{
          //process.exitValue()
        }catch{
          case _  : IllegalThreadStateException  =>
            if (debug) println(s"pyDCOP exit illegal thread state exception")
        }
        if (debug) println(s"pyDCOP exited")
    }
    if (debug) println(s"$name : command result = $success")
    // Either the pyDCOP succeed or the sub-processes must be killed
    success match {
      case i: Int if i == 0 => isFound = true
      case _ => try {
        "ps -ef" #| """grep "dcop"""" #| """grep -v "grep"""" #| "awk '{print $2}'" #| "xargs  -r kill" !<
      } catch {
        case _: IOException =>
          if (debug) println(s"$name fails to kill pyDCOP")
      }
    }
    // ps -ef | grep "dcop" | grep -v "grep"| awk '{print $2}' | xargs -r kill
  }

  /**
    * Reallocate method
    * @return the allocation
    */
  override def rescale(configuration: Configuration): Option[Configuration] = perform(configuration)

  /**
    * Balancer main method
    * @return the allocation
    */
  @throws(classOf[RuntimeException])
  private def perform(configuration: Configuration = Configuration.randomConfiguration(problem)): Option[Configuration] = {

    // 0 - Init allocation
    currentConfiguration = configuration
    if (debug) println(s"$name: initial nodes ${configuration.nodes}")
    if (debug) println(s"$name: initial globalFlowtime ${configuration.allocation.globalFlowtime}")
    if (debug) println(s"$name: initial random allocation\n$configuration")

    // 1 -- Reformulate the problem
    val startingTime: Long = System.nanoTime()
    if (debug) println(s"$name reformulates as a DCOP")
    reformulate()
    preScalingTime = System.nanoTime() - startingTime

    //2 -- Solve the DCOP problem
    if (debug) println(s"$name solves the DCOP")
    execute()

    //3 -- Translate the assignment into an allocation
    if (debug) println(s"$name translates the assignment in an allocation")
    postScalingTime = 0 // The translation is asynchronously performed
    if (isFound) translate()
    //4 -- Remove files
    val fileInput= new File(inputPath)
    val fileOutput= new File(outputPath)
    val fileOutputSave= new File(outputPath+"-e")
    if (fileInput.exists()) fileInput.delete()
    if (fileOutput.exists()) fileOutput.delete()
    if (fileOutputSave.exists()) fileOutputSave.delete()
    if (isFound) {
      println(s"$name has found a configuration")
      Some(currentConfiguration)
    } else {
      println(s"$name has found no configuration")
      None
    }
  }

  /**
    * Returns any just-in-time configuration
    */
  @throws(classOf[RuntimeException])
  override def scale(): Option[Configuration] = perform()
}

/**
  * Companion object to test it
  */
object EDCOPScaler extends App {
  val debug = false
  val rule = QoSFlowtime
  /*
  import org.smastaplus.example.provisioning.ex4Elasticity._
  import org.smastaplus.example.configuration.ex4ConfigurationWith6Nodes._
  val configuration = ex4ConfigurationWith6Nodes
  */
  val l = 3 // with l jobs
  val m = 6 // with m nodes
  val n = l * m // with l * m tasks
  val o = 10 // with o resources per task
  val d = 3 // with d duplicated instances per resource
  val stap = STAP.randomProblem(l, m, n, o, d, Uncorrelated)
  val problem = new ProvisioningProblem(stap, 3000, 1000, rule )
  if (debug) println(s"Pb:\n$problem")
  val activeNodes : SortedSet[ComputingNode]=  collection.immutable.SortedSet[ComputingNode]() ++ pick(stap.ds.computingNodes, m/2)
  val configuration = Configuration.randomConfiguration(problem, activeNodes)
  println(s"${configuration.allocation}")
  println(s"Mean flowtime: ${configuration.allocation.meanGlobalFlowtime}")
  if (debug) println("Build scaler")
  val scaler = new EDCOPScaler(problem,"EDCOPBalancer",30 seconds)
  scaler.debug = false
  if (debug) println("Scale")
  val outcome = scaler.rescale(configuration).get
  if (outcome != configuration) {
    println(outcome.toString)
    println(s"Nb nodes: ${outcome.nodes.size}")
    if (rule == QoSFlowtime) println(s"GlobalFlowtime: ${outcome.allocation.meanGlobalFlowtime}")
    else println(s"Makespan ${outcome.allocation.makespan}")
  } else println("The configuration is the initial one")
}