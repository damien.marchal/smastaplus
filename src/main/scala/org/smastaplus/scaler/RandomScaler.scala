// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler

import org.smastaplus.provisioning._

/**
  * Random scaling
  * @param problem instance to be solved
  * @param name of the scaler
  */
class RandomScaler(problem: ProvisioningProblem,
                   name: String = "RandomScaler")
  extends Scaler(problem, name) {
  debug = false

  /**
    * Returns any just-in-time configuration
    */
  @throws(classOf[RuntimeException])
  override def scale(): Option[Configuration] = {
    var configuration = Configuration.randomConfiguration(problem)
    while (! configuration.isJustInNeed){
      configuration = Configuration.randomConfiguration(problem)
    }
    Some(configuration)
  }
}

