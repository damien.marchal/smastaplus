//Copyright (C) Maxime MORGE, 2022
package org.smastaplus.scaler.local

import org.smastaplus.process.{Activation, Deactivation, SingleDelegation}
import org.smastaplus.provisioning._

/**
  * Class representing an hill climbing for auto-scaling
  * @param problem is a provisioning problem instance to tackle
  * @param name of the scaler
  */
class HillClimbingScaler(problem: ProvisioningProblem, name: String = "HillClimbingScaler")
  extends LocalScaler(problem, name) {

  private val debugHillClimbing: Boolean = true

  /**
    * Returns the best state which is a neighbor with the lowest objective function value
    */
  def highValueSuccessor(current : Configuration) : Option[Configuration] ={
    var bestConfiguration = new Configuration(problem, current.nodes, current.allocation)
    var bestValue = bestConfiguration.nodes.size
    // Delegation
    current.nodes.foreach { initiator => // Foreach initiator
      current.allocation.bundle(initiator).foreach { task => // Foreach task in its bundle
        (current.nodes diff Set(initiator)).foreach { responder => // Foreach responder
          nbDelegations += 1
          val delegation = new SingleDelegation(problem.stap, initiator, responder, task)
          val nextAllocation = delegation.execute(current.allocation)
          val nextConfiguration = new Configuration(problem, nextAllocation.loadedNodes, nextAllocation)
          val nextValue = nextConfiguration.nodes.size
          if (nextValue <= bestValue && nextConfiguration.isJustInNeed) {
            nbSuccessfulDelegations += 1
            if (nextValue < current.nodes.size) nbSuccessfulDeactivations += 1
            bestConfiguration= nextConfiguration
            bestValue = nextValue
          }
        }
      }
    }
    // Deactivation
    current.nodes.foreach { node => // Foreach activated node
      if (current.allocation.bundle(node).isEmpty) {
        val deactivation = new Deactivation(problem)
        val nextConfiguration = deactivation.execute(current, node)
        val nextValue = nextConfiguration.nodes.size
        if (nextValue <= bestValue && nextConfiguration.isJustInNeed) {
          nbSuccessfulDeactivations += 1
          bestConfiguration= nextConfiguration
          bestValue = nextValue
        }
      }
    }
    // Activation
    (problem.stap.ds.computingNodes diff current.nodes).foreach { node => // Foreach deactivated node
        val activation = new Activation(problem)
        val nextConfiguration = activation.execute(current, node)
        val nextValue = nextConfiguration.nodes.size
        if (nextValue <= bestValue && nextConfiguration.isJustInNeed) {
          nbSuccessfulActivations += 1
          bestConfiguration= nextConfiguration
          bestValue = nextValue
        }
    }
    if (bestConfiguration.isJustInNeed) Some(bestConfiguration)
    else None
  }

  /**
    * Returns the closest local minima from a current state
    */
  @throws(classOf[RuntimeException])
  def rescale(configuration: Configuration): Option[Configuration] = {
    initialMeanFlowtime = configuration.allocation.meanGlobalFlowtime
    var current = new Configuration(configuration.problem, configuration.nodes, scheduler.schedule(configuration.allocation))
    if (debugHillClimbing){
      println(s"Just-in-need initial configuration: ${configuration.isJustInNeed}")
      println(s"Initial nb. of active nodes: ${configuration.nodes.size}")
    }
    var isTerminated = false
    while (!isTerminated) {
      val neighbor = highValueSuccessor(current)
      if (neighbor.isEmpty) {
        if (debugHillClimbing) println("No just-in-need neighbor found")
        return None
      }
      val (newValue, currentValue) = (neighbor.get.nodes.size, current.nodes.size)
      if (currentValue <= newValue){
        isTerminated = true
      }else {
        if (debugHillClimbing){
          println(s"Just-in-need current configuration: ${configuration.isJustInNeed}")
          println(s"Current nb. of active nodes: ${configuration.nodes.size}")
        }
        current = neighbor.get
      }
    }
    // Update metrics
    nbDelegations = nbSuccessfulDelegations
    nbActivations = nbSuccessfulActivations
    nbDeactivations = nbSuccessfulDeactivations
    if (current.isJustInNeed) {
      if (debugHillClimbing && current == configuration)
        println("The configuration is the initial one")
      Some(current)
    }
    else None
  }
}

