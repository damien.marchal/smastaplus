// Copyright (C) Maxime MORGE 2022
package org.smastaplus.scaler

import org.smastaplus.provisioning.{Configuration, ProvisioningProblem}
import org.smastaplus.utils.MyTime

/**
  * Abstract class representing a scaler
  * @param problem is a stap instance to tackle
  * @param name of the scaler
  */
abstract class Scaler(val problem: ProvisioningProblem,
                      val name: String) {
  var debug = false

  var scalingTime : Long = 0

  override def toString: String = s"$name with (${MyTime.show(scalingTime)})"

  /**
    * Returns eventually a configuration
    */
  protected def scale() : Option[Configuration]

  /**
    * Returns eventually a configuration and update scaling time
    */
  def run() : Option[Configuration] = {
    val startingTime = System.nanoTime()
    val configuration = scale()
    if (configuration.isDefined) {
      scalingTime = System.nanoTime() - startingTime
      if (configuration.get.isSound) return configuration
      else throw new RuntimeException(s"Scaler: the outcome\n $configuration\nis not sound for\n ${configuration.get.problem}")
    } else {
      if (debug) println(s"Scaler : balancer $name do not return any configuration for $problem")
    }
    configuration
  }
}