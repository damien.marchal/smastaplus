// Copyright (C) Maxime MORGE, Ellie BEAUPREZ, Anne-Cécile CARON 2023
package org.smastaplus.utils

import org.smastaplus.core._
import org.smastaplus.consumer.nodeal.SynchronousDecentralizedConsumer
import org.smastaplus.consumer.deal.mas.AgentBasedConsumer
import org.smastaplus.strategy.deal.proposal.single.ConcreteLiberalSingleProposalStrategy
import org.smastaplus.strategy.deal.proposal.multi.ConcreteMultiProposalStrategy
import org.smastaplus.strategy.deal.counterproposal.single.ConcreteSingleCounterProposalStrategy

import scala.annotation.tailrec
import akka.actor.ActorSystem
import akka.dispatch.MessageDispatcher
import com.typesafe.config.{Config, ConfigFactory}
import org.joda.time.LocalDateTime

/**
 * Consume and negotiate a random  allocation for a random  STAP instance using :
 *
 * - either  sbt "run org.smastaplus.utils.MASTAPlusConsumer -v -m"
 *
 * - or java -jar smastaplus-assembly-X.Y.jar org.smastaplus.utils.MASTAPlusConsumer -v -m
 */
object MASTAPlusConsumer extends App {
  private val debug = true
  private val decentralizedDispatcherId = "akka.actor.default-dispatcher"
  private val decentralizedSystem: ActorSystem = ActorSystem("DecentralizedSystem1")
  private val decentralizedSystem2: ActorSystem = ActorSystem("DecentralizedSystem2")
  private val decentralizedSystem3: ActorSystem = ActorSystem("DecentralizedSystem3")
  private val decentralizedSystem4: ActorSystem = ActorSystem("DecentralizedSystem4")

  implicit val decentralizedContext: MessageDispatcher = decentralizedSystem.dispatchers.lookup(decentralizedDispatcherId)

  // Load monitoring setup
  val config: Config = ConfigFactory.load()
  val monitoringDirectoryName: String = config.getString("path.smastaplus") + "/" +
    config.getString("path.monitoringDirectory")

  // Manage the command line argument
  var verbose: Boolean = false
  var monitor: Boolean = true
  var argList: List[String] = List[String]()
  if (!args.isEmpty) { // drop class name in the java command line
    argList = if (args.head == "org.smastaplus.utils.MASTAPlusConsumer") args.toList.drop(1) else args.toList
    parseCommandLine()
  }

  val usage =
    """
    Usage: java -jar smastasplus-assembly-X.Y.jar org.smastaplus.utils.MASTAPlusConsumer [-v -m]
    """
  val l = 4 // with l jobs
  val m = 8 // with m nodes, eventually 8
  val n = 128 // with n tasks, eventually 128
  val o = 10 // with o resources per task, eventually 10
  val d = 3 // with d instances per resource, eventually 3

  println(s"STAP with $l jobs, $m nodes, $n tasks, $o resources per tasks replicated $d times")
  val pb = STAP.randomProblem(l = l, m = m, n = n, o, d = d, Uncorrelated)
  if (verbose)  println(pb)
  private val a0 = ExecutedAllocation.randomAllocation(pb)
  println("Random allocation")
  if (verbose)  println(a0)

  println("Consume ...")
  a0.setDelay(pb.ds.t0)
  private val consumptionSystem = new SynchronousDecentralizedConsumer(pb,
    decentralizedSystem,
    name = "DecentralizedConsumer",
    dispatcherId = decentralizedDispatcherId,
    monitor = true,
    ssi = false,
    psi = false)
  private val consumedA0 = consumptionSystem.consume(a0)
  private val cs0 = a0.simulatedMeanGlobalFlowtime / 1000 / 60
  println(s"C^S(A0):\t$cs0 mins")
  private val cr0 = consumedA0.realGlobalMeanFlowtime / 1000 / 60
  println(s"C^R(A0):\t$cr0 mins")
  decentralizedSystem.terminate()

  println("Consume with parallel single-item auction ...")
  pb.ds.t0 = LocalDateTime.now()
  a0.setDelay(pb.ds.t0)
  private val psiSystem = new SynchronousDecentralizedConsumer(pb,
    decentralizedSystem3,
    name = "PSIConsumption",
    dispatcherId = decentralizedDispatcherId,
    monitor = true,
    ssi = false,
    psi = true)
  private val consumedA2 = psiSystem.consume(a0)
  private val cs2 = consumedA2.simulatedMeanGlobalFlowtime / 1000 / 60
  println(s"C^S(Ae):\t$cs2 mins")
  private val cr2 = consumedA2.realGlobalMeanFlowtime / 1000 / 60
  println(s"C^R(Ae):\t$cr2 mins")
  decentralizedSystem3.terminate()
  private val gammaPSI = (cr0 - cr2) / cr0 * 100
  println(s"GammaPSI:\t$gammaPSI %")

  println("Consume with sequential single-item auction ...")
  pb.ds.t0 = LocalDateTime.now()
  a0.setDelay(pb.ds.t0)
  private val ssiSystem = new SynchronousDecentralizedConsumer(pb,
    decentralizedSystem2,
    name = "SSIConsumption",
    dispatcherId = decentralizedDispatcherId,
    monitor = true,
    ssi = true,
    psi = false)
  private val consumedA1 = ssiSystem.consume(a0)
  private val cs1 = consumedA1.simulatedMeanGlobalFlowtime / 1000 / 60
  println(s"C^S(Ae):\t$cs1 mins")
  private val cr1 = consumedA1.realGlobalMeanFlowtime / 1000 / 60
  println(s"C^R(Ae):\t$cr1 mins")
  decentralizedSystem2.terminate()
  private val gammaSSI = (cr0 - cr1) / cr0 * 100
  println(s"GammaSSI:\t$gammaSSI %")

  println("Consume with negotiation...")
  pb.ds.t0 = LocalDateTime.now()
  a0.setDelay(pb.ds.t0)
  val consumer = new AgentBasedConsumer(pb,
    GlobalFlowtime,
    ConcreteMultiProposalStrategy(),
    Some(ConcreteLiberalSingleProposalStrategy()),
    new ConcreteSingleCounterProposalStrategy,
    decentralizedSystem4,
    name = "AgentBasedConsumer",
    dispatcherId = decentralizedDispatcherId,
    monitor = true)
  consumer.trace = false
  private val consumedAe = consumer.consume(a0)
  private val csE = consumedAe.simulatedMeanGlobalFlowtime / 1000 / 60
  println(s"C^S(Ae):\t$csE mins")
  private val crE = consumedAe.realGlobalMeanFlowtime / 1000 / 60
  println(s"C^R(Ae):\t$crE mins ")
  decentralizedSystem4.terminate()
  private val gammaNeg = (cr0 - crE) / cr0 * 100
  println(s"GammaNeg:\t$gammaNeg %")

  sys.exit()

  /**
   * Parse the command line
   */
  private def parseCommandLine(): Unit = {
    if (!nextOption(argList)) {
      println(s"ERROR MASTAPlusConsumer: options cannot be parsed ")
      System.exit(1)
    }
  }

  /**
   * Parse options at second
   * @param tags is the list of options
   */
  @tailrec
  def nextOption(tags: List[String]): Boolean = {
    if (tags.isEmpty) return true
    val tag: String = tags.head.substring(1) // remove '-'
    tag match {
      case "v" =>
        if (debug) println("Verbose mode")
        verbose = true
      case "m" =>
        if (debug) println("Monitoring mode")
        monitor = false
      case _ => return false
    }
    nextOption(tags.tail)
  }
}