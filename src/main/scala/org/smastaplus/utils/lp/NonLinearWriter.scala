// Copyright (C) Maxime MORGE 2020, 2022
package org.smastaplus.utils.lp

import org.smastaplus.core._

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Write aSTAP as a non-linear program
  * in a text file
  *
  * @param pathName of the text file
  * @param stap instance to be written
  */
class NonLinearWriter(pathName: String, stap: STAP) {
  val file = new File(pathName)
  def write() : Unit = {
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(stap.toNonLinear)
    bw.close()
  }
}

