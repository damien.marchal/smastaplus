// Copyright (C) Maxime MORGE 2021
package org.smastaplus.utils

import akka.actor.{ Actor, ReceiveTimeout }
import scala.concurrent.duration._

/** Companion object of the Timer class.
  *
  * Contain all the messages that a Timer agent could receive
  */
object Timer {

  /** Message Start
    *
    * Tell to the timer to begin the countdown.
    */
  object Start

  /** Message Cancel
    *
    * Tell to the timer to cancel the countdown.
    */
  object Cancel

  /** Message Timeout
    *
    * Tell to the timer creator that the countdown is over.
    */
  object Timeout

}

/** Timer actor which makes a countdown.
  *
  * @param duration duration of the timer in nanoseconds
  */
class Timer(duration: Long) extends Actor{

  import Timer._

  def receive: Receive = this.active orElse this.handleUnexpected

  /** Active state of the timer. */
  def active: Receive = {

    case Start if sender() == context.parent =>
      context.setReceiveTimeout(Duration(this.duration, NANOSECONDS))

    case ReceiveTimeout =>
      context.setReceiveTimeout(Duration.Undefined)
      context.parent ! Timeout
      context stop self

    case Cancel if sender() == context.parent => // turn off the timer
      context.setReceiveTimeout(Duration.Undefined)
      context stop self

  }

  /** Handle unexpected messages */
  def handleUnexpected: Receive = {
    case msg@_ =>
      throw new RuntimeException(s"ERROR Timer was not expected message $msg")
  }
}