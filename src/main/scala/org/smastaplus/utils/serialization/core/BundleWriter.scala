package org.smastaplus.utils.serialization.core

import org.smastaplus.strategy.consumption.Bundle
import java.io.{BufferedWriter, File, FileWriter}

class BundleWriter(pathName: String, bundle : Bundle, mode : String = "monitoring"){
  val file = new File(pathName)

  def write(): Unit = {
    val bw = new BufferedWriter(new FileWriter(file))
    if (mode == "monitoring") {
      bw.write(bundle.toJSON)
    } else
      bw.write(bundle.toString)
    bw.close()
  }
}
