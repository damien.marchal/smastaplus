// Copyright (C) Maxime MORGE 2022
package org.smastaplus.utils.serialization.provisioning.edcop

import org.smastaplus.core._
import org.smastaplus.provisioning.{Configuration, ProvisioningProblem, QoSFlowtime}
import org.smastaplus.utils.serialization.provisioning.ProvisionWrapper

import java.io.{BufferedWriter, File, FileWriter}

/**
  * Write a provisioning problem as an Extended DCOP problem in a yaml file
  * @param fileName of the text file
  * @param problem instance to be written
  * @param configuration for the provisioning problem
  */
class ProvisionEDCOPWriter(fileName: String, problem: ProvisioningProblem, configuration: Configuration)
  extends ProvisionWrapper(fileName, problem){
  val debug = false

  val file = new File(fileName)
  val bw = new BufferedWriter(new FileWriter(file))

  val weightHighConstraints: Int = Int.MaxValue

  /**
    * Writes the problem in a text file for python parsing
    */
  def writeProblem() : Unit = {
    //defining name of the problem file
    val pathArray = fileName.split("/")
    pathArray(pathArray.length-1) = "stap.txt"
    val problemFile = pathArray.mkString("/")

    val dimension = s"${problem.stap.n}, ${problem.stap.l}, ${problem.stap.m}\n" // dimension of the problem

    var jobsToString = ""
    for (j <- problem.stap.jobs){ // A line per job
      val tasksIDs : Set[String] = for {task <- j.tasks} yield task.toString.replaceAll("[^0-9]", "")
      // the numbers of the tasks in the job
      jobsToString = jobsToString + tasksIDs.mkString(", ")+"\n"
    }

    var costsToString = ""
    for (node <- problem.stap.ds.computingNodes){ // A line per node
      val listOfTasks = problem.stap.tasks.toList
      val costsForNode : List[Double] = for {task <- listOfTasks} yield problem.stap.cost(task, node)
      // the cost of the tasks for all the nodes
      costsToString = costsToString + costsForNode.mkString(", ")+"\n"
    }

    val bufferedWriter = new BufferedWriter(new FileWriter(problemFile))
    bufferedWriter.write(
      dimension +
        jobsToString +
        costsToString
    )
    bufferedWriter.close()
  }

  /**
    * Writes the head of the YAML file
    */
  def head() : Unit = bw.write(
    s"name: 'Provisioning problem instance with rule=${problem.rule}, Θmax=${problem.maxThreshold}, Θmin=${problem.minThreshold}, ${problem.stap.m} nodes, ${problem.stap.n} tasks and ${problem.stap.l} jobs'\n" +
      "description: |\n" +
      s"  nodes: ${problem.stap.ds.computingNodes}\n" +
      s"  tasks: ${problem.stap.tasks}\n" +
      s"  jobs: ${problem.stap.jobs}\n\n" +
      s"# Objective\n" +
      s"objective: min\n\n" +
      s"# Domain\n" +
      s"domains:\n" +
      s"  position:\n" +
      s"    values: [0..${problem.stap.n*problem.stap.m-1}]\n" +
      s"    type: non_semantic\n" +
      s"  activation:\n" +
      s"    values: [0..1]\n" +
      s"    type: non_semantic\n\n"
  )

  /**
    * Writes the variables
    */
  def variables() : Unit = {
    bw.write(s"# ${problem.stap.n} variables ti with i in [1, ${problem.stap.n}]\n")
    bw.write(s"# ${problem.stap.m} variables nj with j in [1, ${problem.stap.m}]\n")
    bw.write("variables:\n")
    for (i <- 1 to problem.stap.n) {
      bw.write(s"  t$i:\n")
      bw.write("      domain: position\n")
    }
    for (j <- 1 to problem.stap.m) {
      bw.write(s"  n$j:\n")
      bw.write("      domain: activation\n")
    }
  }

  /**
    * Returns the activation status of computing node
    */
  def status(node: ComputingNode) : Int = if (configuration.nodes.contains(node)) 1 else 0

  /**
    * Returns the position of a task to a value for the eDCOP model
    */
  def position(task: Task) : Int = {
    val nodeId = configuration.allocation.node(task).toString.replaceAll("[^0-9]", "").toInt
    val position = configuration.allocation.bundle(task).indexOf(task)
    (nodeId-1) * problem.stap.n + position
  }

  /**
    * Writes the variables with initial values in accordance with a configuration
    */
  def variablesWithInitialValues() : Unit = {
    bw.write(s"# ${problem.stap.n} variables ti with i in [1, ${problem.stap.n}]\n")
    bw.write(s"# ${problem.stap.m} variables nj with j in [1, ${problem.stap.m}]\n")
    bw.write("variables:\n")
    for (i <- 1 to problem.stap.n) {
      bw.write(s"  t$i:\n")
      if (debug) print(s"t$i : ")
      bw.write("      domain: position\n")
      //initial value to determine
      val task = problem.stap.tasks.toVector(i-1)
      val initialValue = position(task)
      bw.write(s"      initial_value: $initialValue\n")
    }
    for (j <- 1 to problem.stap.m) {
      bw.write(s"  n$j:\n")
      bw.write("      domain: activation\n")
      val node = problem.stap.ds.computingNodes.toVector(j-1)
      val initialValue : Int = status(node)
      bw.write(s"      initial_value: $initialValue\n")
    }
  }

  /**
    * Writes the constraints
    */
  def constraints() : Unit = {
    val tasksToStrings : List[String] = (for{i <- 1 to problem.stap.n} yield s"t$i").toList
    bw.write("\n# Constraints\n")
    bw.write("constraints:\n")
    bw.write(s"  #  Hard constraint to ensure there is at most one task per position\n")
    bw.write(s"  constraintOneTaskPerPosition :\n")
    bw.write("      type: intention\n")
    bw.write("      source: \"./edcop.py\"\n")
    bw.write(s"      function: source.all_different(${tasksToStrings.mkString(",")})\n")
    bw.write(s"\n  #  Hard constraint to ensure a node is active iff there is at least one task in its bundle\n")
    for (j <- 1 to problem.stap.m) {
      bw.write(s"  constraintActivation$j :\n")
      bw.write("      type: intention\n")
      bw.write("      source: \"./edcop.py\"\n")
      bw.write(s"      function: source.active(${j-1},n$j,${tasksToStrings.mkString(",")})\n")
    }
    val function = if (problem.rule == QoSFlowtime) "flowtime" else  "makespan"
    bw.write(s"\n  # Maximal threshold for the $function\n")
    bw.write(s"  max_$function:\n")
    bw.write("      type: intention\n")
    bw.write("      source: \"./edcop.py\"\n")
    bw.write(s"      function: source.max_$function(${problem.maxThreshold},${tasksToStrings.mkString(",")})\n")
    bw.write(s"\n  # Minimal threshold for the $function\n")
    bw.write(s"  min_$function:\n")
    bw.write("      type: intention\n")
    bw.write("      source: \"./edcop.py\"\n")
    bw.write(s"      function: source.min_$function(${problem.minThreshold},${tasksToStrings.mkString(",")})\n")
    val nodesToStrings : List[String] = (for{j <- 1 to problem.stap.m} yield s"n$j").toList
    bw.write(s"\n  # Objective function for the $function\n")
    bw.write(s"  nbActivatedNodes:\n")
    bw.write("      type: intention\n")
    bw.write("      source: \"./edcop.py\"\n")
    bw.write(s"      function: ${nodesToStrings.mkString(" + ")}\n")
  }

  /**
    * Write the agents
    */
  def agents() : Unit = {
    val nbAgents = problem.stap.n + problem.stap.m
    bw.write("\n# Agents\n")
    bw.write("agents: [")
    for (a <- 1 to nbAgents){
      bw.write(s"a$a")
      if (a != nbAgents) bw.write(", ")
    }
    bw.write("]\n")
  }

  /**
    * Write the whole YAML representation
    **/
  def write() : Unit = {
    writeProblem()
    head()
    variablesWithInitialValues()
    constraints()
    agents()
    bw.close()
  }
}

/**
  * Companion object for testing
  */
object ProvisionEDCOPWriter extends App {
  import org.smastaplus.example.provisioning.ex4Elasticity._
  import org.smastaplus.example.configuration.ex4ConfigurationWith6Nodes._
  // Run with the command line
  //pydcop --output examples/edcop/scaler/ex1.json --timeout 10 solve --algo mgm2 examples/edcop/scaler/ex1.yml
  new ProvisionEDCOPWriter("examples/edcop/scaler/ex1.yml", problem, ex4ConfigurationWith6Nodes).write()
}