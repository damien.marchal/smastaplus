// Copyright (C) Maxime MORGE, 2022
package org.smastaplus.utils.serialization.provisioning

import org.smastaplus.core._
import org.smastaplus.provisioning.ProvisioningProblem

/**
  * Wrapper to parse/write a provisioning problem/configuration as a file
  * @param fileName of the input/output
  * @param problem provisioning problem to be written/parsed
  */
class ProvisionWrapper(fileName: String,
                       problem: ProvisioningProblem) {

  val taskArray : Array[Task] = problem.stap.tasks.toArray // tasks as an array
  val nodeArray : Array[ComputingNode] = problem.stap.ds.computingNodes.toArray // nodes as array
  val jobArray : Array[Job] = problem.stap.jobs.toArray // jobs as an array
  var costArray : Array[Array[Double]] = // costs as an array
    Array.tabulate(problem.stap.n, problem.stap.m)((t, n) => problem.stap.cost(task(t), node(n)))
  var z: Array[Array[Int]] = // contains as an array
    Array.tabulate(problem.stap.n, problem.stap.l)((i, j) => if (job(j).tasks.contains(task(i))) 1 else 0)

  /**
    * Returns the ith task with 0 <= i < pb.n
    */
  def task(i : Int): Task = {
    if (i >= problem.stap.n) throw new RuntimeException(s"Cannot returns the $i th task with ${problem.stap.n} tasks")
    taskArray(i)
  }

  /**
    * Returns the ith node with 0 <= i < pb.m
    */
  def node(i : Int): ComputingNode = {
    if (i >=problem.stap.m) throw new RuntimeException(s"Cannot returns the $i th node with ${problem.stap.m} nodes")
    nodeArray(i)
  }

  /**
    * Returns the jth job with 0 <= j < pb.n
    */
  def job(j : Int): Job = {
    if (j >= problem.stap.l) throw new RuntimeException(s"Cannot returns the $j th job with ${problem.stap.l} jobs")
    jobArray(j)
  }
}

