// Copyright (C) Ellie BEAUPREZ, Maxime MORGE 2021, 2022
package org.smastaplus.deputation

import org.smastaplus.core.{Allocation, ComputingNode, ExecutedAllocation, STAP, Task}

import scala.collection.SortedSet
//import org.joda.time.LocalDateTime


/**
  * Class representing a Deputation Problem
  * @param nodeAgents the set of the node agents
  * @param jobAgents the set of the job agents
  */
class Deputation(val nodeAgents : SortedSet[NodeAgent], val jobAgents : SortedSet[JobAgent]){
  val debug = false

  /**
    * Returns a node for a particular node for the unit tests
    */
  @throws(classOf[RuntimeException])
  def nodeAgent(node: ComputingNode): NodeAgent = {
    nodeAgents.find(nodeAgent => nodeAgent.node == node) match {
      case Some(nodeAgent) => nodeAgent
      case None => throw new RuntimeException(s"ERROR: No node agent for the node $node has been found")
    }
  }

  /**
    * Returns the job agent which contains the task
    */
  @throws(classOf[RuntimeException])
  def jobAgent4Task(task: Task): JobAgent = {
    jobAgents.find(jobAgent => jobAgent.job.tasks contains task) match {
      case Some(jobAgent) => jobAgent
      case None => throw new RuntimeException(s"ERROR: The task $task does not belong to any job")
    }
  }

  /**
    * Returns the node agent which owns the task
    */
  @throws(classOf[RuntimeException])
  def nodeAgent4Task(task: Task): NodeAgent = {
    nodeAgents.find(nodeAgent => nodeAgent.sortedTasks() contains task) match {
      case Some(nodeAgent) => nodeAgent
      case None => throw new RuntimeException(s"ERROR: The task $task is not owned by any node")
    }
  }

  /**
    * Returns a string representation of the variableAssignments for all the job agents
    */
  def describeVariables() : String = jobAgents.foldLeft("")( (s, jobAgent) => s + jobAgent.describeVariables + "\n" )

  /**
    * Returns a string representation of the bundles for all the node agents
    */
  def describeBundles() : String = nodeAgents.foldLeft("")( (s, nodeAgent) => s + nodeAgent.describeBundle + "\n")

  /**
    * Returns a string representation of the costs for all the node agents
    */
  def describeCosts() : String = nodeAgents.foldLeft("")( (s, nodeAgent) => s + nodeAgent.describeCosts + "\n")

  /**
    * Returns a string representation of the completion times for all the node agents
    */
  def describeCompletionTimes() : String = nodeAgents.foldLeft("")( (s, nodeAgent) => s + nodeAgent.describeCompletionTimes + "\n")

  /**
    * Returns a string representation of the completion times for all the node agents
    */
  override def toString : String = s"Deputation:\n${describeVariables()}\n${describeBundles()}\n${describeCompletionTimes()}"

  /**
    * Returns the globalFlowtime
    */
  def globalFlowtime() : Double = jobAgents.foldLeft(0.0)((acc, jobAgent) => acc + jobAgent.completionTimeOfJob(nodeAgents))

  /**
    * Returns a copy
    */
  def copy() : Deputation = {
    var transformedNodes = Map[NodeAgent, NodeAgent]() // Map the current node agents with their copies
    //Copy nodes agents
    val copiedNodeAgents : SortedSet[NodeAgent] = nodeAgents.map{ nodeAgent =>
      val copiedNodeAgent = nodeAgent.copy()
      transformedNodes = transformedNodes + (nodeAgent -> copiedNodeAgent)
      copiedNodeAgent
    }
    //Copy job agents
    val copiedJobAgents : SortedSet[JobAgent]  = jobAgents.map { jobAgent =>
      val copiedJobAgent = jobAgent.copy()
      jobAgent.variableAssignments.foreach {  case (task, value) => // and the current assignment
          copiedJobAgent.assignVariable(task, new VariableAssignment( transformedNodes(value.nodeAgent), value.position ))
        }
      copiedJobAgent
    }
    new Deputation(copiedNodeAgents, copiedJobAgents)
  }

  /**
    * Returns the payoff of a new variableAssignment for a task within the updated problem
    */
  def payoff(task: Task, valuation: VariableAssignment) : Double = {
    if (debug) println(s"Current globalFlowtime: ${globalFlowtime()}")
    val copiedDCOP = copy()
    val copiedNodeAgent = copiedDCOP.nodeAgent(valuation.nodeAgent.node)
    val copiedVariableAssignment = new VariableAssignment(copiedNodeAgent, valuation.position)
    val copiedJobAgent = copiedDCOP.jobAgent4Task(task)
    copiedDCOP.updateAssignmentsAndBundles(copiedJobAgent, task, copiedVariableAssignment)
    if (debug) println(s"Current globalFlowtime: ${globalFlowtime()} - ${copiedDCOP.globalFlowtime()}")
    globalFlowtime() - copiedDCOP.globalFlowtime()
  }

  /**
    *  Clean the variableAssignments and the positions in the nodes bundles according to the sorted bundle of the node agents
    */
  def cleanPositions() : Unit = {
    for (nodeAgent <- nodeAgents) { // For each node agent
      val bundleArray = nodeAgent.sortedTasks()
      for (index <- bundleArray.indices) {
        val task = bundleArray(index)
        val integerPosition = index + 1
        val jobOfTask = jobAgent4Task(task)
        val variableAssignment = jobOfTask.variableAssignments(task)
        if (variableAssignment.position != integerPosition) {
          jobOfTask.assignVariable(task, new VariableAssignment(variableAssignment.nodeAgent, integerPosition))
        }
        nodeAgent.updatePositionOfTask(task, integerPosition)
      }
    }
  }

  /**
    * Update the problem with a new variableAssignment for a task of job agent
    */
  def updateAssignmentsAndBundles(job: JobAgent, task: Task, value: VariableAssignment) : Unit = {
    val currentAssignment = job.variableAssignments(task)
    val currentNode = currentAssignment.nodeAgent
    val newNode = value.nodeAgent
    // Update the variable assignments
    job.variableAssignments = job.variableAssignments + (task -> value)
    // Move the task
    currentNode.removeTask(task)
    newNode.addTask(task, value.position)
    //Update the positions on the node agents
    for (jobAgent <- jobAgents){
      for ((taskId, valuation) <- jobAgent.variableAssignments) {
        val nodeAgent = valuation.nodeAgent
        nodeAgent.updatePositionOfTask(taskId, valuation.position)
      }
    }
  }

  /**
    * Returns the set of the potential variable reassignments
    */
  def potentialReassignments(assignment: VariableAssignment) : SortedSet[VariableAssignment] = {
    var potentialAssignations : SortedSet[VariableAssignment] = SortedSet()
    for (node <- nodeAgents) { // For each node
      if (node.equals(assignment.nodeAgent)){ // Either the task stays on the same node
        // For all the other tasks assigned to the node
        for (otherTask <- node.sortedTasks() if node.bundle(otherTask) != assignment.position){
            val otherPosition = node.bundle(otherTask)
            val updatedPosition = node.justBeyond(assignment.position, otherPosition)
            potentialAssignations = potentialAssignations.union(SortedSet(new VariableAssignment(node, updatedPosition)))
        }
      }else{ // Or the task moves on another node
        val maxPosition = node.bundle.keys.size
        // For all the tasks assigned to the node
        for (otherTask <- node.sortedTasks()){
          val otherPosition = node.bundle(otherTask)
          val updatedPosition = node.immediatelyPrecedingPosition(otherPosition)
          potentialAssignations = potentialAssignations.union(SortedSet(new VariableAssignment(node, updatedPosition)))
        }
        potentialAssignations = potentialAssignations.union(SortedSet(new VariableAssignment(node, maxPosition+1.0)))
      }
    }
    potentialAssignations
  }

  /**
    * Returns the task which must be reassigned between payoff ties Rule for deciding the task to reassign in case of equality.
    * Either the heuristic chooses the task having the most task after it in the bundle
    * Or the the lexicographic order over the task ids breaks tie
    */
  def breakTieTasks(task1: Task, task2: Task, heuristic : Boolean) : Task = {
    if (heuristic) {
      if (nodeAgent4Task(task1).nbTasksAfter(task1) > nodeAgent4Task(task2).nbTasksAfter(task2)) return task1
      else return task2
    }
    if (task1 < task2) return task1
    task2
  }

  /**
    * Returns the allocation corresponding to the given STAP
    */
  def assignment2Allocation(stap : STAP) : ExecutedAllocation = {
    var allocation = new ExecutedAllocation(stap)
    stap.ds.computingNodes.foreach{ node =>
      allocation = allocation.update(node, nodeAgent(node).sortedTasks())
    }
    allocation
  }
}

  /**
  * Factory for building a deputation problem from an allocation
  */
object Deputation{
  /**
    * Returns the deputation problem corresponding to the given allocation
    */
  def stap2deputation(allocation: Allocation): Deputation = {
    // Build job agents
    val jobAgents : SortedSet[JobAgent] = allocation.stap.jobs.map(job  =>
      new JobAgent(job))

    // Build node agents
    val nodeAgents : SortedSet[NodeAgent] = allocation.stap.ds.computingNodes.map(node =>
      new  NodeAgent(node, allocation.stap.costs(node), allocation.bundle(node)))

    val deputation = new Deputation(nodeAgents, jobAgents)
    // Init the variable assignments according to the allocation
    nodeAgents.foreach{ nodeAgent =>
      nodeAgent.bundle.keys.foreach{ task =>
        val jobAgent = deputation.jobAgent4Task(task)
        jobAgent.assignVariable(task, new VariableAssignment(nodeAgent, nodeAgent.bundle(task)))
        }
    }
    deputation
  }
}
