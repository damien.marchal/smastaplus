# AUTHORS

* Ellie BEAUPREZ, Univ. Lille, CNRS, Centrale Lille, UMR 9189 - CRIStAL, F-59000 Lille, France

* [Anne-Cécile CARON](https://pro.univ-lille.fr/anne-cecile-caron), Univ. Lille, CNRS, Centrale Lille, UMR 9189 - CRIStAL, F-59000 Lille, France

* [Maxime MORGE](https://pro.univ-lille.fr/maxime-morge), Univ. Lille, CNRS, Centrale Lille, UMR 9189 - CRIStAL, F-59000 Lille, France

