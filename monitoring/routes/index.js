const express = require('express');
const router = express.Router();

// this router manage the root route "/"
const indexController = require('../controllers/indexController');

router.get('/', indexController.home );

module.exports = router;

