## What's new in SMASTA+ ?


### SMASTA+ 2.2

In order [compare](doc/experiments) our reallocation strategy executed concurrently with this consumption process, 
we consider  two baseline algorithms :
- an [allocation method](src/main/scala/org/smastaplus/balancer/SSIBalancer.scala)
  based on sequential single-item (SSI) auctions where bids correspond to the flowtime,
  which precedes the allocation process. Every agent bids on each unallocated task ;
- a [reallocation method](src/main/scala/org/smastaplus/balancer/PSIBalancer.scala) 
  based on parallel single-item (PSI) auctions executed concurrently
  with the consumption process where each node announces the pending jobs in its job pool.

[Lagoudakis et al., 2006] Lagoudakis, S. K. C. T. M., Markakis, V., Kempe, D., Keskinocak, P.,
Kleywegt, A., Meyerson, A., & Jain, S. (2006). The Power of Sequential Single-Item Auctions
for Agent Coordination. AAAI. 1625--1629.

### SMASTA+ 2.1

We provide a NodeJS monitoring interface for monitoring the task reallocation during consumption.
The instructions for monitoring the task allocation are [here](monitoring).

### SMASTA+ 2.0

Adaptive consumption by continuous negotiation !

In order to reduce the complexity of designing a node agent, we have adopted a modular
[architecture](doc/specification/README.md) that allows for concurrency of negotiations and consumption.

Our [experiments](doc/experiments) show that, when executed continuously during the consumption process, 
our reallocation strategy improves the flowtime by almost 20~%, even when the agents have imperfect 
knowledge of the computing environment (i.e. the slowing down of nodes).

### SMASTA+ 1.2

In order to simplify the agent behaviour, we introduce a composite node.

### SMASTA+ 1.1

A [distributed system](src/main/scala/org/smastaplus/core/DistributedSystem.scala) contains:
- a pool of computing nodes available within the infrastructure which can be activated in order to process the tasks;
- some resource nodes since each task requires resources which are distributed across different resource nodes.

We consider :
- [situated task allocation problems with concurrent jobs](src/main/scala/org/smastaplus/core/STAP.scala) which consist
  of finding an optimal allocation.
- [provisioning problems](src/main/scala/org/smastaplus/provisioning/ProvisioningProblem.scala) which consist of
  finding a minimal configuration.

We consider the following [operations](src/main/scala/org/smastaplus/process/Operation.scala):
- a [deal](src/main/scala/org/smastaplus/process/Deal.scala) between contractors (i.e. computing nodes involved)
which exchanges some lists of tasks within a stap instance;
- a [task consumption](src/main/scala/org/smastaplus/process/Consumption.scala);
- an [activation](src/main/scala/org/smastaplus/process/Activation.scala) of a computing node modifies the set of 
active computing nodes and so, the time-extended allocation is also updated since the recently activated node 
is associated with an empty bundle;
- a [deactivation](src/main/scala/org/smastaplus/process/Deactivation.scala) of a computing node modifies 
the set of active computing nodes and so the time-extended allocation. A node must be associated with an empty 
bundle in order to be deactivated.

A time-extended [allocation](src/main/scala/org/smastaplus/core/STAP.scala) is an assignment of the tasks 
to some computing nodes. Its delay is the time since the initial boot time of the distributed system.

A [configuration](src/main/scala/org/smastaplus/provisioning/Configuration.scala) (at time t) represents a specific 
task allocation for a non-empty set of computing nodes.

A [load-balancer](src/main/scala/org/smastaplus/balancer/Balancer.scala) returns an allocation with the aim of 
making it optimal with respect to a [social rule](src/main/scala/org/smastaplus/core/SocialRule.scala)

A [scaler](src/main/scala/org/smastaplus/scaler/Scaler.scala) returns a configuration with the aim of making it minimal 
according to the [QoS constraints](src/main/scala/org/smastaplus/provisioning/QoS.scala).

We propose a first [scaler](src/main/scala/org/smastaplus/scaler/dual/EDCOPScaler.scala) based on PyDCOP.

### SMASTA+ 1.0

#### Swap versus Delegation

1. The notion of delegation has been generalized in the form of 
[bilateral reallocation](src/main/scala/org/smastaplus/process/Deal.scala), 
which makes it possible to consider [swap](src/main/scala/org/smastaplus/process/Swap.scala);
2. the rationality criterion for reallocation has been redefined [here](src/main/scala/org/smastaplus/process/RationalDeal.scala);
3. the [counter-offer strategy](src/main/scala/org/smastaplus/strategy/deal/counterproposal/CounterProposalStrategy.scala) 
allows an agent to improve the proposal of task delegation with a task swap.

The agent behaviour is specified in by an [automaton](doc/specification/README.md) 
which now consists of 2 successive phases: 
1. the proposers adopt the 
[conservative offer strategy](src/main/scala/org/smastaplus/strategy/deal/proposal/single/ConcreteConservativeProposalStrategy.scala)
and the responders accept a delegation they consider the swap is acceptable. They reject it otherwise;
2. the agents adopt the 
[liberal offer strategy](src/main/scala/org/smastaplus/strategy/deal/proposal/single/ConcreteLiberalSingleProposalStrategy.scala) 
and the [counter-offer strategy](src/main/scala/org/smastaplus/strategy/deal/counterproposal/CounterProposalStrategy.scala). 
If at least one reallocation is performed, these two phases are repeated. Otherwise the process is closed.

The [Supervisor](src/main/scala/org/smastaplus/balancer/deal/mas/supervisor/Supervisor.scala) coordinates the negotiation process.

#### Reproducibility of experiments

For reproducibility, the empirical evaluation of SMASTA+ can be performed with
the following [instructions](doc/experiments).

### SMASTA+ 0.9

We are happy to announce the release of SMASTA+ v0.9. The prototype has been extensively refactored.

1. The only consumption strategy supported is "locally the cheapest job first - locally the cheapest task first". 
   [Bundle](src/main/scala/org/smastaplus/strategy/consumption/Bundle.scala) and
   [SubBundle](src/main/scala/org/smastaplus/strategy/consumption/SubBundle.scala) are data structure which are optimized
    for this strategy. the prototype is 100 times faster.

2. Since we provide several [social rules](src/main/scala/org/smastaplus/core/SocialRule.scala), 
the acceptability rule of the deal (single-delegation, multi-delegation, single-swap) strategies (for instance, 
the method `acceptabilityCriteria(...)` of the class 
[SingleDelegation](src/main/scala/org/smastaplus/strategy/deal/proposal/single/SingleProposalStrategy.scala)) can reduce:
  - either the makespan ;
  - or the local flowtime and the makespan  (as in the version 0.8);
  - or the global flowtime (this is new !);
  - etc.

#### The `GlobalFlowtime` acceptability rule

In order to optimize the deal strategies with the `GlobalFlowtime` acceptability rule,
[BeliefBase](src/main/scala/org/smastaplus/strategy/consumption/BeliefBase.scala) is a  data structure to preprocess
the ordered list of jobs for a peer and the completion times of jobs for a peer. The latter is not computed for each 
deal evaluation but when an [Insight](src/main/scala/org/smastaplus/strategy/consumption/Insight.scala) is received. 

Two main issues were raised by this release :

1. When a proposal is stashed for later evaluation, it may contain an obsolete insight. 
That is the reason why an [Insight](src/main/scala/org/smastaplus/strategy/consumption/Insight.scala) has a timestamp.

2. the acceptability rule `GlobalFlowtime`can lead to deadlocks  such that:
   1. a Propose b
   2. b Propose a
   3. a Accept b
   4. b Accept a
   5. Timeout reached for a
   6. Timeout reached for b
   7. a Withdraw  b
   8. b Withdraw  a
   9. a Propose b
   10. b Propose a         

That is the reason why an [Agent](src/main/scala/org/smastaplus/balancer/deal/mas/nodeAgent/negotiator/NegotiationBehaviour.scala):

1. randomly chooses the deadline for each proposal (see the method `deadline()`)

2. has a random stash policy (see the method `myStash()` method)

This trick allows around 20 successful negotiations per second in a symmetric continuous game.
The git repository [DeadlockEscapeGame](https://gitlab.univ-lille.fr/maxime.morge/deadlockescapegame)
allows testing and evaluating such a trick. If you have a better solution, please contact Maxime MORGE.

The experimental results comparing the acceptability rules  `LocalFlowtimeAndMakespan` (Local for  short) and `GlobalFlowtime` (Global) are available 
[here](experiments/balancer/figures/localFlowtimeVsGlobalFlowtime). With similar rescheduling times, 
the `GlobalFlowtime` acceptability rule significantly improves the mean flowtime.

#### `SingleProposalStrategy` vs. `MultiProposalStrategy`

We compare the 
[MultiProposalStrategy](src/main/scala/org/smastaplus/strategy/deal/proposal/multi/ConcreteMultiProposalStrategy.scala)
with `maxEndowmentSize = Int.MaxValue` and `maxEndowmentSize = 1`.

3When the initial allocation is almost balanced, the bundles negotiated by the
strategy contain on average 1.5 tasks and the two strategies have similar
strategies behaviours as illustrated [here](experiments/balancer/figures/singleVsMultiDelegationFlowtime)

When the bundles negotiated by the multi-proposal strategy 
(see [here](experiments/balancer/figures/singleVsMultiDelegationFlowtime)) contain on average 3 tasks 
and the single proposal strategy :
- is slightly faster since it allows for a more decentralised reallocation process
- achieves a slightly better completion time since the more refined search (with simple delegations) avoids local minima 

### SMASTA+ 0.8

- An multi-delegation strategy

### SMASTA+ 0.7

- An improved [swap strategy](doc/specification/README.md) for the decentralized balancer

### SMASTA+ 0.6

- A [swap strategy](doc/specification/README.md) for the decentralized balancer

### SMASTA+ 0.5

- A DCOP balancer based on the python library [pyDCOP](https://github.com/Orange-OpenSource/pyDcop)

### SMASTA+ 0.4

- A constraint programming balancer for comparison based on the [Java Constraint Programming Library (JaCoP)](https://www.lth.se/jacop/) 
- A DCOP-like balancer
