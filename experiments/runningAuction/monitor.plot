set terminal svg
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
set auto y
set auto y2
#set key autotitle columnhead
unset key
set key font ",14"
set key out vert
set key center top
set key maxrows 4
set key horiz
set ticslevel 0
set style data lines
set xlabel "Running time (ms)" font ",16"
set xtics (0,1,2,3,4,5,6)
set xtics nomirror
set xrange [0:5]
set yrange [0:2]
set y2range [0:128]
set ytics nomirror tc "black"
set y2tics nomirror tc "black"
set ylabel "Mean flowtime (mn)" font ",16" tc "black"
set y2label "Number of tasks" font ",16" tc "black"

set table 'data/q1NbDelegatedTasks.dat'
plot "data/quartilesNegotiationLog.csv" using ($1):($2) smooth bezier
unset table

set table 'data/q2NbDelegatedTasks.dat'
plot "data/quartilesNegotiationLog.csv" using ($1):($4) smooth bezier
unset table

set table 'data/q1PsiNbDelegatedTasks.dat'
plot "data/quartilesPsiLog.csv" using ($1):($2) smooth bezier
unset table

set table 'data/q2PsiNbDelegatedTasks.dat'
plot "data/quartilesPsiLog.csv" using ($1):($4) smooth bezier
unset table

set table 'data/q1NbConsumedTasks.dat'
plot "data/quartilesNegotiationLog.csv" using ($1):($5) smooth bezier
unset table

set table 'data/q2NbConsumedTasks.dat'
plot "data/quartilesNegotiationLog.csv" using ($1):($7) smooth bezier
unset table

set table 'data/q1NegFlowtime.dat'
plot "data/quartilesNegotiationLog.csv" using ($1):($8) smooth bezier
unset table

set table 'data/q2NegFlowtime.dat'
plot "data/quartilesNegotiationLog.csv" using ($1):($10) smooth bezier
unset table

set table 'data/q1NoNegFlowtime.dat'
plot "data/quartilesNoNegotiationLog.csv" using ($1):($8) smooth bezier
unset table

set table 'data/q2NoNegFlowtime.dat'
plot "data/quartilesNoNegotiationLog.csv" using ($1):($10) smooth bezier
unset table

set table 'data/q1SsiFlowtime.dat'
plot "data/quartilesSsiLog.csv" using ($1):($8) smooth bezier
unset table

set table 'data/q2SsiFlowtime.dat'
plot "data/quartilesSsiLog.csv" using ($1):($10) smooth bezier
unset table

set table 'data/q1PsiFlowtime.dat'
plot "data/quartilesPsiLog.csv" using ($1):($8) smooth bezier
unset table

set table 'data/q2PsiFlowtime.dat'
plot "data/quartilesPsiLog.csv" using ($1):($10) smooth bezier
unset table

set table 'data/q1NoNegNbConsumedTasks.dat'
plot "data/quartilesNoNegotiationLog.csv" using ($1):($5) smooth bezier
unset table

set table 'data/q2NoNegNbConsumedTasks.dat'
plot "data/quartilesNoNegotiationLog.csv" using ($1):($7) smooth bezier
unset table

set table 'data/q1PsiNbConsumedTasks.dat'
plot "data/quartilesPsiLog.csv" using ($1):($5) smooth bezier
unset table

set table 'data/q2PsiNbConsumedTasks.dat'
plot "data/quartilesPsiLog.csv" using ($1):($7) smooth bezier
unset table

set table 'data/q1SsiNbConsumedTasks.dat'
plot "data/quartilesSsiLog.csv" using ($1):($5) smooth bezier
unset table

set table 'data/q2SsiNbConsumedTasks.dat'
plot "data/quartilesSsiLog.csv" using ($1):($7) smooth bezier
unset table

set format x "10^%.0f"

set output 'figures/monitor.svg'
plot "data/quartilesNegotiationLog.csv" using ($1):($3) smooth bezier dt 7 lc "#32a68c" title 'MASTA+ delegated tasks' axes x1y2,\
    "< tail -r data/q1NbDelegatedTasks.dat | cat data/q2NbDelegatedTasks.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle axes x1y2,\
    "data/quartilesPsiLog.csv" using ($1):($3) smooth bezier dt 7 lc "#5862ed" title 'PSI delegated tasks' axes x1y2,\
    "< tail -r data/q1PsiNbDelegatedTasks.dat | cat data/q2PsiNbDelegatedTasks.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle axes x1y2,\
    "data/quartilesNegotiationLog.csv" using ($1):($6) smooth bezier dt 8 lc "#32a68c" title 'MASTA+ consumed tasks' axes x1y2,\
    "< tail -r data/q1NbConsumedTasks.dat | cat data/q2NbConsumedTasks.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle axes x1y2,\
    "data/quartilesNoNegotiationLog.csv" using ($1):($6) smooth bezier dt 8 lc "#fc535c" title 'Consumed tasks' axes x1y2,\
    "< tail -r data/q1NoNegNbConsumedTasks.dat | cat data/q2NoNegNbConsumedTasks.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#ff6941" fs transparent solid 0.3 border lc "#fc535c" notitle axes x1y2,\
    "data/quartilesPsiLog.csv" using ($1):($6) smooth bezier dt 8 lc "#5862ed" title 'PSI consumed tasks' axes x1y2,\
    "< tail -r data/q1PsiNbConsumedTasks.dat | cat data/q2PsiNbConsumedTasks.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle axes x1y2,\
    "data/quartilesSsiLog.csv" using ($1):($6) smooth bezier dt 8 lc "#000000" title 'SSI consumed tasks' axes x1y2,\
    "< tail -r data/q1SsiNbConsumedTasks.dat | cat data/q2SsiNbConsumedTasks.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle axes x1y2,\
    "data/quartilesNoNegotiationLog.csv" using ($1):($9) smooth bezier  dt 1 lc "#fc535c" title 'Consumption' axes x1y1,\
    "< tail -r data/q1NoNegFlowtime.dat | cat data/q2NoNegFlowtime.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#ff6941" fs transparent solid 0.3 border lc "#fc535c" notitle axes x1y1,\
    "data/quartilesNegotiationLog.csv" using ($1):($9) smooth bezier axes x1y1 dt 1 lc "#32a68c" title 'MASTA+',\
    "< tail -r data/q1NegFlowtime.dat | cat data/q2NegFlowtime.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#89e0b1" fs transparent solid 0.3 border lc "#32a68c" notitle axes x1y1,\
    "data/quartilesPsiLog.csv" using ($1):($9) smooth bezier axes x1y1 dt 1 lc "#5862ed" title 'PSI',\
    "< tail -r data/q1PsiFlowtime.dat | cat data/q2PsiFlowtime.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#5862ed" fs transparent solid 0.3 border lc "#5862ed" notitle axes x1y1,\
    "data/quartilesSsiLog.csv" using ($1):($9) smooth bezier axes x1y1 dt 1 lc "#000000" title 'SSI',\
    "< tail -r data/q1SsiFlowtime.dat | cat data/q2SsiFlowtime.dat - | awk 'BEGIN{OFS=\",\"}{print $1,$2}' " using 1:2 with filledcurves fc "#2d0505" fs transparent solid 0.3 border lc "#000000" notitle axes x1y1



