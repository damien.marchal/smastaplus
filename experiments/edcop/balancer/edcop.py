# Copyright (C) Ellie BEAUPREZ 2021
# Python program describing the "hard" constraints and the flowtime for a eDCOP problem
import sys


def all_different(*variables):
    """
    Returns
    0 if all the variable have a different assignment
    infinity otherwise
    """
    if len(set(variables)) == len(variables):
        return 0
    else:
        return sys.maxsize


class Job:
    """
    Class representing a job
    """

    def __init__(self, the_id, the_tasks):
        self.id = the_id
        self.tasks = the_tasks

    def get_id(self):
        return self.id

    def describe(self):
        """
        Returns a string description of the job
        :return:
        """
        res = "J" + str(self.id) + " :"
        for t in self.tasks:
            res += "t" + str(t) + " "
        return res

    def last(self, node):
        """
        Returns the last task of the job on a particular node or 0
        if none task of the job is over the node
        """
        max_pos = -1
        last_task = 0
        bundle = node.get_bundle()
        for task, position in bundle.items():
            if task in self.tasks:
                if max_pos < position:
                    max_pos = position
                    last_task = task
        return last_task

    def completion_time(self, nodes):
        """
        Returns the completion time of the job on all the nodes
        """
        max_completion_time = 0
        for node in nodes:
            last_task = self.last(node)
            if last_task > 0:
                completion_time_last_task = node.completion_time(last_task)
                if completion_time_last_task > max_completion_time:
                    max_completion_time = completion_time_last_task
        return max_completion_time


class Node:
    """
    Class representing a node
    """

    def __init__(self, the_id, the_costs, the_bundle):
        self.id = the_id
        self.costs = the_costs
        self.bundle = the_bundle

    def describe(self):
        """
        Returns a string description of the node
        :return:
        """
        return str(self.id) + ": " + str(self.bundle)

    def get_bundle(self):
        return self.bundle

    def get_id(self):
        return self.id

    def workload(self):
        """
        Returns the workload of the node
        eventually 0 if the bundle is empty
        """
        load = 0
        for task in self.bundle.keys():
            load += self.costs[task - 1]
        return load

    def completion_time(self, task_id):
        """
        Returns the completion time of a task on a node
        eventually 0 if the task is not in this bundle
        """
        if task_id not in self.bundle:
            return 0
        time = 0
        task_position = self.bundle[task_id]
        for task, position in self.bundle.items():
            if task == task_id or position < task_position:
                time += self.costs[task - 1]
        return time


def parse_problem_file(file_name):
    """
    Returns the MASTA problem from a text file
    n, l, m, jobs, costs
    """
    fp = open(file_name, 'r')
    lines = [line.rstrip('\n') for line in fp]
    fp.close()
    parameters = lines[0].split(", ")
    the_n, the_l, the_m = int(parameters[0]), int(parameters[1]), int(parameters[2])
    the_jobs = []
    index = 1
    for i in range(0, the_l):
        tasks = list(map(int, lines[index + i].split(", ")))
        job = Job(i + 1, tasks)
        the_jobs.append(job)
    the_costs = dict()
    index = the_l + 1
    for i in range(0, the_m):
        costs_of_node = list(map(float, lines[index + i].split(", ")))
        the_costs[i + 1] = costs_of_node
    return the_n, the_l, the_m, the_jobs, the_costs


# Import the masta problem/allocation from a text file
n, l, m, jobs, costs = parse_problem_file("experiments/edcop/balancer/allocation.txt")


def get_task(the_id, *variables):
    """
    Returns the variable corresponding to a task ID
    """
    return variables[the_id - 1]


def assignment2position(variable):
    """
    Converts a value to the corresponding (node_id, position) pair
    """
    node_id = (variable // n) + 1
    position = variable % n + 1
    return node_id, position


def assignments2positions(variables):
    """
    Returns the pairs (node_id, position) corresponding to the variables values
    """
    assignments = dict()
    for i in range(n):
        node_id, position = assignment2position(variables[i])
        assignments[i + 1] = (node_id, position)
    return assignments


def build_bundles(assignments):
    """
    Returns the bundles (node_id, task_id, position)
    """
    bundles = dict()
    for task, assignment in assignments.items():
        node_id, position = assignment
        if node_id not in bundles.keys():
            bundles[node_id] = {task: position}
        else:
            bundles[node_id][task] = position
    return bundles


def build_nodes(bundles):
    """
    Returns the nodes with their bundles
    """
    nodes = []
    for node_id, bundle in bundles.items():
        node = Node(node_id, costs[node_id], bundle)
        nodes.append(node)
    return nodes


def build_jobs(tasks_per_jobs):
    the_jobs = []
    for jobId, tasks in tasks_per_jobs.items():
        job = Job(jobId, tasks)
        jobs.append(job)
    return the_jobs


def flowtime(*variables):
    """
    Returns the flowtime of the current allocation
    """
    assignments = assignments2positions(variables)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    cost = 0
    for job in jobs:
        cost += job.completion_time(nodes)
    return cost


def makespan(*variables):
    """
    Returns the makespan of the current allocation
    """
    assignments = assignments2positions(variables)
    bundles = build_bundles(assignments)
    nodes = build_nodes(bundles)
    worst_workload = 0
    for node in nodes:
        workload = node.workload()
        if worst_workload < workload:
            worst_workload = workload
    return worst_workload
