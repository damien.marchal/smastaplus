set terminal svg #pdfcairo
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
#set xrange [2:12]
set auto y
set grid
set key autotitle columnhead
unset key
set key font ",20"
set key left above
set ticslevel 0
set style data lines
set xlabel "Number of nodes" font ",16"
set ylabel "Mean flowtime"  font ",16"
set output 'figures/delegationVsSwapFlowtime/meanFlowtime.svg'
plot  "data/delegationVsSwap.csv" using 1:16:18 with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/delegationVsSwap.csv" using 1:17 with lines dt 3 lc "dark-green" title 'Strategy without swap',\
      "data/delegationVsSwap.csv" using 1:46:48 with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/delegationVsSwap.csv" using 1:47 with lines dt 7 lc "dark-blue" title 'Strategy with swap',\
      "data/delegationVsSwap.csv" using 1:76:78 with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/delegationVsSwap.csv" using 1:77 with lines dt 5 lc "dark-red" title 'Hill climbing'
#      "data/delegationVsSwap.csv" using 1:($7):($9) with filledcurves fc "light-grey" fs transparent solid 0.3 border lc "grey" notitle,\
#      "data/delegationVsSwap.csv" using 1:($8) with lines dt 1 lc "dark-grey" title 'Initial allocation',\
unset ylabel
set ylabel "Rescheduling time (s)" font ",16"
set xlabel "Number of nodes" font ",16"
set yrange [*:*]
set output 'figures/delegationVsSwapFlowtime/schedulingTime.svg'
plot  "data/delegationVsSwap.csv" using 1:($19*1E-9):($21*1E-9) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/delegationVsSwap.csv" using 1:($20*1E-9) with lines dt 3 lc "dark-green" title 'Strategy without swap',\
      "data/delegationVsSwap.csv" using 1:($49*1E-9):($51*1E-9) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/delegationVsSwap.csv" using 1:($50*1E-9) with lines dt 7 lc "dark-blue" title 'Strategy with swap',\
      "data/delegationVsSwap.csv" using 1:($79*1E-9):($81*1E-9) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/delegationVsSwap.csv" using 1:($80*1E-9) with lines dt 5 lc "dark-red" title 'Hill climbing'
#unset ylabel
#set ylabel "Nb. of deals" font ",16"
#set xlabel "Number of nodes" font ",16"
#set output 'figures/delegationVsSwapFlowtime/nbDeals.svg'
#plot  "data/delegationVsSwap.csv" using 1:($28*100):($30*100) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
#      "data/delegationVsSwap.csv" using 1:($29*100) with lines dt 3 lc "dark-green" title 'Strategy without swap',\
#      "data/delegationVsSwap.csv" using 1:($58*100):($60*100) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
#      "data/delegationVsSwap.csv" using 1:($59*100) with lines dt 7 lc "dark-blue" title 'Strategy with swap',\
#       "data/delegationVsSwap.csv" using 1:($88*100):($90*100) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
#     "data/delegationVsSwap.csv" using 1:($89*100) with lines dt 5 lc "dark-red" title 'Hill climbing'
unset ylabel
set ylabel "Nb. of delegations" font ",16"
set xlabel "Number of nodes" font ",16"
set output 'figures/delegationVsSwapFlowtime/nbDelegations.svg'
plot  "data/delegationVsSwap.csv" using 1:($28*100):($30*100) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
      "data/delegationVsSwap.csv" using 1:($29*100) with lines dt 3 lc "dark-green" title 'Strategy without swap',\
      "data/delegationVsSwap.csv" using 1:($58*100):($60*100) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
      "data/delegationVsSwap.csv" using 1:($59*100) with lines dt 7 lc "dark-blue" title 'Strategy with swap',\
       "data/delegationVsSwap.csv" using 1:($88*100):($90*100) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
     "data/delegationVsSwap.csv" using 1:($89*100) with lines dt 5 lc "dark-red" title 'Hill climbing'
#unset ylabel
#set ylabel "Nb. of timeouts" font ",16"
#set xlabel "Number of nodes" font ",16"
#set output 'figures/delegationVsSwapFlowtime/nbTimeouts.svg'
#plot  "data/delegationVsSwap.csv" using 1:($34*100):($36*100) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
#      "data/delegationVsSwap.csv" using 1:($35*100) with lines dt 3 lc "dark-green" title 'Strategy without swap',\
#      "data/delegationVsSwap.csv" using 1:($64*100):($66*100) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
#      "data/delegationVsSwap.csv" using 1:($65*100) with lines dt 7 lc "dark-blue" title 'Strategy with swap'
#unset ylabel
#set ylabel "Nb. of delegated tasks" font ",16"
#set xlabel "Number of nodes" font ",16"
#set output 'figures/delegationVsSwapFlowtime/nbDelegatedTask.svg'
#plot  "data/delegationVsSwap.csv" using 1:($40*100):($42*100) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
#      "data/delegationVsSwap.csv" using 1:($41*100) with lines dt 3 lc "dark-green" title 'Strategy without swap',\
#      "data/delegationVsSwap.csv" using 1:($70*100):($72*100) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
#      "data/delegationVsSwap.csv" using 1:($71*100) with lines dt 7 lc "dark-blue" title 'Strategy with swap'
#      #"data/delegationVsSwap.csv" using 1:($100*100):($102*100) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
#      #"data/delegationVsSwap.csv" using 1:($101*100) with lines dt 5 lc "dark-red" title 'Hill climbing'
unset ylabel
set key font ",12"
set ylabel "Local availability ratio (%)"  font ",16"
set yrange [0:100]
set xlabel "Number of nodes" font ",16"
set output 'figures/delegationVsSwapFlowtime/localityRatio.svg'
plot  "data/delegationVsSwap.csv" using 1:($4*100):($6*100) with filledcurves fc "light-grey" fs transparent solid 0.3 border lc "grey" notitle,\
      "data/delegationVsSwap.csv" using 1:($5*100) with lines dt 1 lc "dark-grey" title 'Initial allocation',\
      "data/delegationVsSwap.csv" using 1:($22*100):($24*100) with filledcurves fc "light-green" fs transparent solid 0.3 border lc "green" notitle,\
     "data/delegationVsSwap.csv" using 1:($23*100) with lines dt 3 lc "dark-green" title 'Strategy without swap',\
     "data/delegationVsSwap.csv" using 1:($52*100):($54*100) with filledcurves fc "light-blue" fs transparent solid 0.3 border lc "blue" notitle,\
     "data/delegationVsSwap.csv" using 1:($53*100) with lines dt 7 lc "dark-blue" title 'Strategy with swap',\
      "data/delegationVsSwap.csv" using 1:($82*100):($84*100) with filledcurves fc "light-red" fs transparent solid 0.3 border lc "red" notitle,\
      "data/delegationVsSwap.csv" using 1:($83*100) with lines dt 5 lc "dark-red" title 'Hill climbing'
