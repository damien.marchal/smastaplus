set terminal svg #pdfcairo
set datafile separator ","
set style fill solid border rgb "black"
set style fill transparent solid 0.1 noborder
set key autotitle columnhead
unset key
set key font ",20"
set key left above
set ticslevel 0
set style data lines
#set xrange [0:4.5]
set xtics nomirror
#set x2tics ('Phase 2' .919, 'Phase 1' 3.211,'Phase 2' 3.973)
set xlabel "Time (s)" font ",16"
#set grid x2
set yrange [1500:3500]
set ytics nomirror tc "black"
set ylabel "Mean flowtime" font ",16" tc "black"
set output 'figures/delegationVsSwapFlowtime/meanFlowtimeByTime.svg'
plot "data/globalFlowtimeSingleProposalStrategySingleCounterProposalStrategylog.csv" using ($1*1E-9):($2):x2tic($1*1E-9) with lines lw 2 lc "dark-blue" notitle #"Swap strategy"
# TriggerSecondStage: Metrics(117,0,87,0,7,87,87)
# End first stage SingleProposalStrategy SingleCounterProposalStrategy: 919
# TriggerFirstStage: Metrics(542,39,89,38,46,165,40)
# TriggerSecondStage: Metrics(547,39,92,38,46,168,3)
# End first stage SingleProposalStrategy SingleCounterProposalStrategy: 3211
# TriggerFirstStage: Metrics(599,45,92,44,50,180,6)
# TriggerSecondStage: Metrics(608,45,100,44,50,188,8)
# End first stage SingleProposalStrategy SingleCounterProposalStrategy: 3973
# TriggerFirstStage: Metrics(617,47,100,46,50,192,2)
# TriggerSecondStage: Metrics(617,47,100,46,50,192,0)
# End first stage SingleProposalStrategy SingleCounterProposalStrategy: 4317
# DecentralizedSolver: metrics Metrics(617,47,100,46,50,192,0)
# nbDelegations nbSwaps nbTimeouts
# 100 46 50

